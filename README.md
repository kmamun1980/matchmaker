Prerequisites
=============

1. Install vagrant (vagrantup.com). Important! Use latest version from the vagrantup.com webpage. Vagrant in repos is usually way to old to work with this provisioning setup.
2. Install virtualbox (virtualbox.org)
3. Checkout project repository

Installing
==========

1. Go to project directory and run `vagrant up`
2. wait while until everything is installed
3. Call `vagrant ssh`  //log into the virtual machine
4. python manage.py syncdb
5. python manage.py migrate cities_light
6. python manage.py migrate company
7. python manage.py migrate events
8. python manage.py createsuperuser

Running
=======

1. Go to project directory and run `vagrant up`
2. wait while until everything is installed
3. Call `vagrant ssh`  //log into the virtual machine
4. While logged into virtual machine call `rs`  // alias to start django
5. go to localhost:6399 in the browser (at host machine)

Stopping
========

1. vagrant suspend - to suspend vagrant
2. vagrant halt - to stop vagrant
3. vagrant status - to check status
4. vagrant help - more commands

Development
===========

Project files are available at /vagrant at virtual machine. However it's a synced folder so just edit project files
from host machine using you favourite editor.
