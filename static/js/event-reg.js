

$(document).ready(function() {



    $(".event").click(function () {
         var value = $(this).attr("id");
        var div_id = 'div'+value
        $('.highlight').removeClass('active')
        $('#'+div_id).addClass('active')

        $.ajax({
        url: '/get-activities/'+value+'/',
        type: "POST",
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


         $('#event-details').html(response)


        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });


});

    $(".register").click(function () {
        var $link = $(this)
         var value = $(this).attr("id");
        var div_id = 'div'+value

        $.ajax({
        url: '/get-pricing/'+value+'/',
        type: "POST",
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


         $('#details').html(response)


        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });


});


     $(".nmregister").click(function () {
        var $link = $(this)
         var value = $(this).attr("id");
        var div_id = 'div'+value

        $.ajax({
        url: '/register-event/'+value+'/',
        type: "POST",
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


         data = $.parseJSON(response)
           if (data['success']=='true'){
               $link.text('Registered').removeClass('register')


           }
            else{
               window.location.href = '/pay-for-event/'+data['id']+'/';


           }




        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });


});





});  // end doc ready

$(document).on('click', '.activity', function() {
    var value = $(this).attr("id");

        $('.highlight-a').removeClass('active')
        $('#'+value).addClass('active')

        $.ajax({
        url: '/get-activity-type/'+value+'/',
        type: "POST",
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


         $('#details').html(response)


        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });


});


$(document).on('click', '.regActivity', function(e) {


            e.preventDefault();

            activityid = $(this).attr('id');

            $.ajax({

            url: "/events/register-activity/"+activityid+"/",
            dataType: 'json',
            context: document.body
            }).done(function(data) {

               $( ".class"+data.id ).html("<span class='btn btn-success'><i class='fa fa-check-circle' tooltip='Registered'></i> Registered </span><br/>" );
            })
          });
