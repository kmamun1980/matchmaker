function youtube_video_id_extractor(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/,
        match = url.match(regExp);
    if (match && match[2].length == 11) {
        return match[2];
    } else {
        // error
        return false;
    }
}

function youtube_previewer($input) {

    var $preview = $('#' + $input.attr('id') + '_preview'),
        $label;
    if (!$preview.length) {
        $preview = $('<div style="position: relative;" id="' + $input.attr('id') + '_preview"></div>');
        $label = $input.parents('label');
        if ($label.length) {
            $label.before($preview);
        } else {
            $input.before($preview);
        }
    }
    $preview.html('');

    if (!$input.val()) return;

    var video_id = youtube_video_id_extractor($input.val());

    if (video_id) {

        $preview.append('<img style="width: 100%; max-width: 100%;" src="http://i.ytimg.com/vi/' + video_id + '/hqdefault.jpg">');
        $preview.append('<div style="width: 0; height: 0; position: absolute; left: 50%; top: 50%;"><a title="Play video" style="position: absolute; left: -35px; top: -35px;" class="btn btn-primary btn-circle btn-xl" href="javascript:void(0);"><i class="glyphicon glyphicon-play"></i></a></div>');

        $preview.find('a').click(function() {
            $preview.find('img').replaceWith('<iframe style="border: 0; width: ' + $preview.width() + 'px; height: ' + $preview.height() + 'px;" src="https://www.youtube.com/embed/' + video_id + '?autoplay=1&autohide=1&border=0&wmode=opaque&enablejsapi=1" />');
            $(this).remove();
        });
    } else {
        $preview.append('<div class="alert alert-danger"><i class="fa-fw fa fa-times"></i><strong>Error:</strong> YouTube url seems to be incorrect.</div>');
    }
}

$(document).ready(function(){
    $(".toggler").click(function(e){
        e.preventDefault();
        $('.cat'+$(this).attr('data-prod-cat')).toggle();
     });
     $("input[name='show-send-email']").click(function () {
          $('#same-email-show-me').css('display', ($(this).val() === 'show-send-email1') ? 'block':'none');
          $('#different-email-show-me').css('display', ($(this).val() === 'show-send-email2') ? 'block':'none');
     });

});
