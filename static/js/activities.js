(function(){
    var scrollToForm = function(){
        $('body').animate({
            scrollTop: $('#activityFormWrapper').offset().top
        }, 2000);
    };

    var $body = $("body");
    $body.on("submit", "#addActivityForm", function(){
        var $this = $(this);
        var data = $this.serialize();
        $.post(
            djangoConstants.newActivityUrl,
            data,
            function(response){
                if(response.status == 'created'){
                    location.reload();
                }else{
                    $this.replaceWith(response.form_html);
                    scrollToForm();
                    $('#id_date').datepicker({});
                    $('#id_start_time, #id_end_time').timepicker({});
                }
            }
        );
        return false;
    });

    $("#activityToggleBtn").on('click', function(){
        clearModalWindow();
        $.get(
            djangoConstants.newActivityUrl,
            function(response){
                var $replace = $("#replace");
                $replace.removeClass("hide");
                $replace.html(_.template($("#FormWrapperTmpl").html(), {
                    title: 'New activity'
                }));

                $activityFormWrapper = $("#activityFormWrapper");
                $activityFormWrapper.html(response.form_html);
                scrollToForm();
                $('#id_date').datepicker({});
                $('#id_start_time, #id_end_time').timepicker({});
                $('.range-slider5').ionRangeSlider();
                //$('#id_facility').after('<input type="hidden" name="facility" id="id_facility_input" value="" />')
                $('#id_facility_input').val($('#id_facility').val())
            }
        );
        return false;
    });

    $body.on('click', ".editActivity", function(){
        clearModalWindow();
        var updateUrl = $(this).data('update-url');
        var activityId = $(this).data("id");

        var lookingDate = $(this).data("looking-date");
        var facilityPk = $(this).data("facility-pk");
        var facilityTheaterCapacity = $(this).data("facility-theater-capacity");
        var facilityClassroomCapacity = $(this).data("facility-classroom-capacity");
        var facilityBanquetCapacity = $(this).data("facility-banquet-capacity");
        var facilitySqft = $(this).data("facility-sqft");
        var facilityBuilding = $(this).data("facility-building");

        console.log("looking date: ", lookingDate);
        console.log("facility pk: ", facilityPk);
        console.log("facility theater capacity", facilityTheaterCapacity);
        console.log("facility classroom capacity", facilityClassroomCapacity);
        console.log("facility banquet capacity", facilityBanquetCapacity);
        console.log("facility sqft", facilitySqft);
        console.log("facility building", facilityBuilding);

        fillModal(lookingDate, facilityPk, facilityTheaterCapacity,
                  facilityClassroomCapacity, facilityBanquetCapacity,
                  facilitySqft, facilityBuilding);

        $("#availabilityList").fadeOut();
        $("#modal-activities").data("activity-id", activityId);
        $.get(updateUrl, function(response){
            var $replace = $("#replace");
            $replace.removeClass("hide");
            $replace.html(_.template($("#FormWrapperTmpl").html(), {
                title: 'Update activity'
            }));

            $("#activityFormWrapper").html(response.form_html);
            scrollToForm();
            $('#id_date').datepicker({});
            $('#id_start_time, #id_end_time').timepicker({});
        });
    });

    $('body').on('submit', '#updateActivityForm', function(e){
        e.preventDefault();
        var $this = $(this);
        var updateUrl = $(this).attr('action');
        var data = $this.serialize();
        $.post(updateUrl, data, function(response){
            if(response.status == 'updated'){
                location.reload();
            }else{
                $this.replaceWith(response.form_html);
                scrollToForm();
                $('#id_date').datepicker({});
                $('#id_start_time, #id_end_time').timepicker({});
            }
        });
        return false;
    });
})();