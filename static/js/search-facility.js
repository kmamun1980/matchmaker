var clearAvailabilityList = function(){
    var $availabilityList = $("#availabilityList");
    $availabilityList.fadeOut();
    $availabilityList.find(".timeSelected").removeClass("timeSelected");
    $("#facilitySearchResult").html('');
};

var clearModalWindow = function(){
    clearAvailabilityList();
    $("#modal-activities").data("activity-id", '');
    $("#id_building").val('1');
    $("#id_theater_capacity__gte").val('');
    $("#id_theater_capacity__lte").val('');
    $("#id_classroom_capacity__gte").val('');
    $("#id_classroom_capacity__lte").val('');
    $("#id_banquet_capacity__gte").val('');
    $("#id_banquet_capacity__lte").val('');
    $("#id_sqft__gte").val('');
    $("#id_sqft__lte").val('');
    $("#id_looking_date").val('');
};

var fillModal = function(lookingDate, facilityPk, theaterCapacity,
                         classRoomCapacity, banquetCapacity, sqft, building){
    $("#id_building").val(building);
    $("#id_theater_capacity__gte").val(theaterCapacity);
    $("#id_theater_capacity__lte").val(theaterCapacity);
    $("#id_classroom_capacity__gte").val(classRoomCapacity);
    $("#id_classroom_capacity__lte").val(classRoomCapacity);
    $("#id_banquet_capacity__gte").val(banquetCapacity);
    $("#id_banquet_capacity__lte").val(banquetCapacity);
    $("#id_sqft__gte").val(sqft);
    $("#id_sqft__lte").val(sqft);
    $("#id_looking_date").val(lookingDate);
    $("#searchFacilityButton").click();
    $(document).on("facilityResultCreated", function(){
        $("#room"+facilityPk).click();
        $(document).off("facilityResultCreated");
    });
};

(function(){
    var $body = $("body");
    $body.on('click', '#searchFacilityButton', function(){
        clearAvailabilityList();
        var $form = $("#searchFacilityForm");
        $.get(
            searchFacilityUrl,
            $form.serialize(),
            function(response){
                $form.replaceWith(response.form_html);
                if(response.status != 'error'){
                    $('#facilitySearchResult').html(
                        _.template($('#FacilityResultsTmpl').html(), response)
                    );
                    $(document).trigger("facilityResultCreated");
                }
            }
        );
        return false;
    });

    $body.on('change', '#id_looking_date', function(){
        clearAvailabilityList()
    });

    $body.on('click', '#facilityList li input', function(){
        var $input = $(this);
        var name = $input.text();
        var availableUrl = $input.data('url');
        var activityId = $("#modal-activities").data("activity-id");

        console.log(availableUrl);
        console.log(activityId);
        $("#facilityAlert").hide();
        $("#availabilityList").fadeOut();
        $.get(availableUrl, {'activity_pk': activityId || 0},function(response){
            $("#facilityAvailableTitle").text(name);

            var $timeNum = $(".time-num");
            $timeNum.removeClass("bg-color-greenLight");
            $timeNum.removeClass("bg-color-orange");
            $timeNum.removeClass("blocked");
            $timeNum.removeClass("timeSelected");
            $timeNum.find("i").removeClass("fa-check-square-o");
            $timeNum.find("i").addClass("fa-square-o");

            $('#timeAlert').text('');
            _.each(response.availability_list, function(item){
                $item = $(".num" + item.number);
                if(item.is_available){
                    $item.addClass('bg-color-greenLight');
                    if(item.is_selected){
                        var $icon = $(".num" + item.number + " i");
                        $item.addClass("timeSelected");
                        $icon.removeClass("fa-square-o");
                        $icon.addClass("fa-check-square-o");
                    }
                }else{
                    $item.addClass('bg-color-orange blocked');
                }
            });
            var $availabilityList = $("#availabilityList");
            $availabilityList.removeClass("hide");
            $availabilityList.fadeIn();
        });
    });

    $body.on('click', '.time-num', function(){
        var $this = $(this);
        var $icon = $($this.find("i").get(0));
        console.log($icon);
        if($this.hasClass("timeSelected")){
            $this.removeClass("timeSelected");
            $icon.removeClass("fa-check-square-o");
            $icon.addClass("fa-square-o")
        }else if(!$this.hasClass('blocked')){
            $this.addClass("timeSelected");
            $icon.addClass("fa-check-square-o");
            $icon.removeClass("fa-square-o");
        }
    });

    $body.on("click", '#selectTime', function(){
        var $lookingDate = $("#id_looking_date");
        var $facilityAlert = $("#facilityAlert");
        var $timeAlert = $("#timeAlert");
        var $dateAlert = $("#dateAlert");

        $facilityAlert.addClass("hide");
        $timeAlert.addClass("hide");
        $dateAlert.addClass("hide");

        var lookingDate = $lookingDate.val();
        var selectedRoomId = $("input[name=results]:checked").val();
        var selectedTimeArr = _.sortBy(_.map($(".time-num.timeSelected"), function(item){
            return parseInt($(item).data('num'));
        }), function(num){return num});
        console.log('Selected date: ' + lookingDate);
        console.log('Selected room id: ' + selectedRoomId);
        console.log('selectedTimeArr: ' + selectedTimeArr);

        var errorMessage = function($alert, message){
            $alert.text(message);
            $alert.removeClass("hide");
        };

        if(!lookingDate){
           errorMessage($dateAlert, "Please select date");
            return;
        }
        if(selectedRoomId === undefined){
            errorMessage($facilityAlert, "Please select room");
            return;
        }
        if(selectedTimeArr.length == 0){
            errorMessage($timeAlert, "Please select time");
            return;
        }
        if(selectedTimeArr.length > 1){
            var val1 = _.reduce(_.map(selectedTimeArr ,function(i){
                return i - selectedTimeArr[0];
            }), function(a, b){return a + b});

            var val2  = ((selectedTimeArr.length - 1)/2)*selectedTimeArr.length;

            if(val1 != val2){
               errorMessage($timeAlert,
                            "Selected time should follow one by one");
               return;
            }
        }
        var startTime = selectedTimeArr[0];
        var endTime = selectedTimeArr[selectedTimeArr.length - 1];

        var startHour = ~~(startTime/2);
        var startMinute = (startTime % 2)?"30":"00";
        var startTimeStr = "" + startHour + ":" + startMinute;

        var endHour = ~~(endTime/2);
        var endMinute = (endTime % 2)?"59":"29";

        var endTimeStr = "" + endHour + ":" + endMinute;

        console.log("Start time: ", startTimeStr);
        console.log("End time: ", endTimeStr);

        $("#id_date").val(lookingDate);
        $("#id_start_time").val(startTimeStr);
        $("#id_end_time").val(endTimeStr);
        $("#id_facility").val(selectedRoomId);
        $('#id_facility_input').val(selectedRoomId)

        $("#modal-activities").modal("hide");
    });
})();