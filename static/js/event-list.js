(function(){
    $("#createEvent").on("click", function(e){
        e.preventDefault();
        $.get($(this).attr("href"), function(response){
            console.log(response);
            $("#eventModalTitle").text("Create event");
            $("#eventModalBody").html(response.form_html);
            $("#eventModal").modal("show");
            var date = new Date();
            date.setDate(date.getDate());
            $('#id_start_time, #id_end_time').datepicker({});
            $('#id_reg_start, #id_reg_end').datetimepicker({});
        });
        return false;
    });

    var $body = $("body");
    $body.on("submit", "#createEventForm", function(e){
        e.preventDefault();
        var $this = $(this);
        $.post(
            $this.attr("action"),
            $this.serialize(),
            function(response){
                console.log(response);
                if(response.status == 'created'){
                    location.reload();
                }else{
                    $this.replaceWith(response.form_html);
                }
            }
        );
        return false;
    });

    $body.on("click", "a.editEvent", function(e){
        e.preventDefault();
        $.get($(this).attr("href"), function(response){
            console.log(response);
            $("#eventModalTitle").text("Edit event");
            $("#eventModalBody").html(response.form_html);
            $("#eventModal").modal("show");
            $('#id_start_time, #id_end_time').datepicker({});
            $('#id_reg_start, #id_reg_end').datetimepicker({});
        });
        return false;
    });

    $body.on("click", "a.deleteEvent", function(e){
        e.preventDefault();
        var $this = $(this);
        var name = $this.data("event-name");
        var deleteUrl = $this.attr("href");
        $.confirm({
            text: "Are you sure you want to delete event \"" + name + "\"?",
            confirm: function(){
                $.post(deleteUrl, {}, function(response){
                    if(response.status == 'deleted'){
                        location.reload();
                    }
                })
            }
        });
    });

    $body.on("submit", "#updateEventForm", function(e){
        e.preventDefault();
        var $this = $(this);
        $.post(
            $this.attr("action"),
            $this.serialize(),
            function(response){
                console.log(response);
                if(response.status == 'updated'){
                    location.reload();
                }else{
                    $this.replaceWith(response.form_html);
                }
            }
        );
    });

    $('#dt_basic').dataTable({
	    "sPaginationType" : "bootstrap_full"
	});

    /* Add the events etc before DataTables hides a column */
    var $datatable_fixed_colum_input = $("#datatable_fixed_column");
    $datatable_fixed_colum_input.keyup(function() {
        oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
    });

    $datatable_fixed_colum_input.each(function() {
        this.initVal = this.value;
    });
    $datatable_fixed_colum_input.focus(function() {
        if (this.className == "search_init") {
            this.className = "";
            this.value = "";
        }
    });
    $datatable_fixed_colum_input.blur(function() {
        if (this.value == "") {
            this.className = "search_init";
            this.value = this.initVal;
        }
    });

    var oTable = $('#datatable_fixed_column').dataTable({
        "sDom" : "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        //"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
        "oLanguage" : {
            "sSearch" : "Search all columns:"
        },
        "bSortCellsTop" : true
    });



    /*
     * COL ORDER
     */
    $('#datatable_col_reorder').dataTable({
        "sPaginationType" : "bootstrap",
        "sDom" : "R<'dt-top-row'Clf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "fnInitComplete" : function(oSettings) {
            console.log(oSettings);
            $('.ColVis_Button').addClass('btn btn-default btn-sm').html('Columns <i class="icon-arrow-down"></i>');
        }
    });

    /* END COL ORDER */

    /* TABLE TOOLS */
    $('#datatable_tabletools').dataTable({
        "sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "oTableTools" : {
            "aButtons" : ["copy", "print", {
                "sExtends" : "collection",
                "sButtonText" : 'Save <span class="caret" />',
                "aButtons" : ["csv", "xls", "pdf"]
            }],
            "sSwfPath" : "/static/assets/js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
        },
        "fnInitComplete" : function(oSettings) {
            console.log(oSettings);
            $(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        }
    });
})();