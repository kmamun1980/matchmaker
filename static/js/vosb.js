$(document).ready(function() {

        $('#add-vosb-info').submit(function(e){



    var postData = new FormData($(this)[0]);
    var formURL = '/add-vosb-info/'


     $('#show_submit').hide()

    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


            data = $.parseJSON(response)
           if (data['success']=='true'){
               $('#message-info').html('Information Saved Successfully')
                $('#message-info').show()


           }
            else{

               $('#message-info').html('Some required fields are missing')
                $('#message-info').show()



           }





        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    e.preventDefault();	//STOP default action
    e.stopImmediatePropagation();


});


    $('#doc-form').submit(function(e){



    var postData = new FormData($(this)[0]);
    var formURL = '/add-vosb-doc/'


     $('#doc-submit').hide()

    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


            data = $.parseJSON(response)
           if (data['success']=='true'){
               $('#doc-info').html('Document Saved Successfully')

               $('#document-list').prepend(data['message']).show();
                $('#doc-info').show()
               $('#doc-form')[0].reset();


           }
            else{

               $('#doc-info').html('Some required fields are missing')
                $('#doc-info').show()



           }





        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    e.preventDefault();	//STOP default action
    e.stopImmediatePropagation();


});

    $('#code-form').submit(function(e){



    var postData = new FormData($(this)[0]);
    var formURL = '/add-vosb-code/'


     $('#doc-submit').hide()

    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


            data = $.parseJSON(response)
           if (data['success']=='true'){
               $('#doc-info').html('Code Saved Successfully')

               $('#code-list').prepend(data['message']).show();
                $('#code-info').show()
               $('#code-form')[0].reset();


           }
            else{

               $('#code-info').html('Some required fields are missing')
                $('#code-info').show()



           }





        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    e.preventDefault();	//STOP default action
    e.stopImmediatePropagation();


});

    $('#cert-form').submit(function(e){



    var postData = new FormData($(this)[0]);
    var formURL = '/add-vosb-cert/'


     $('#cert-submit').hide()

    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


            data = $.parseJSON(response)
           if (data['success']=='true'){
               $('#cert-info').html('Certification Saved Successfully')

               $('#cert-list').prepend(data['message']).show();
                $('#cert-info').show()
                $('#cert-show').show()
               $('#cert-form')[0].reset();


           }
            else{

               $('#cert-info').html('Some required fields are missing')
                $('#cert-info').show()



           }





        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    e.preventDefault();	//STOP default action
    e.stopImmediatePropagation();


});


    $('#overview-form').submit(function(e){



    var postData = new FormData($(this)[0]);
    var formURL = '/add-vosb-overview/'


     $('#overview-submit').hide()

    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response, textStatus, jqXHR) {


            data = $.parseJSON(response)
           if (data['success']=='true'){

               $('#overview-show').prepend(data['message']).show();
                $('#overview-info').show()


           }
            else{

               $('#overview-show').html('Some required fields are missing')
                $('#overview-info').show()



           }





        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    e.preventDefault();	//STOP default action
    e.stopImmediatePropagation();


});

});  // end doc ready


