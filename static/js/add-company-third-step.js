(function(){
    
    $('tr:not(.hide) .dateinput.form-control').datepicker({});
    
    $("#addMoreDocument").on("click", function(e){
        var formTmpl = $("#documentEmptyForm").html();
        var $totalForms = $("#id_document-TOTAL_FORMS");
        var maxForms = parseInt($("#id_document-MAX_NUM_FORMS").val());

        var totalForms = parseInt($totalForms.val());
        if(totalForms < maxForms){
            var newForm = formTmpl.replace(/__prefix__/g, totalForms++);
            $("#document_formset_table > tbody").append(
                    '<tr>' + newForm + '</tr>'
            );
            $totalForms.val(totalForms);
        }
    });

    $("#addMorePerformance").on("click", function(e){
        var formTmpl = $("#performanceEmptyForm").html();
        var $totalForms = $("#id_performance-TOTAL_FORMS");
        var maxForms = parseInt($("#id_performance-MAX_NUM_FORMS").val());

        var totalForms = parseInt($totalForms.val());
        if(totalForms < maxForms){
            var newForm = formTmpl.replace(/__prefix__/g, totalForms++);
            $("#performance_formset_table > tbody").append(
                    '<tr>' + newForm + '</tr>'
            );
            $totalForms.val(totalForms);
        }
        //$('.dateinput.form-control').datepicker('destroy');
        $('tr:not(.hide) .dateinput.form-control').datepicker({});
    });
})();