#!/usr/bin/env bash

apt-get update -y

echo "Install system packages"
apt-get install -y build-essential python-pip python-dev mc git

# Dependencies for virtualenv
apt-get install -y libjpeg8 libjpeg8-dev libtiff-dev zlib1g-dev libfreetype6-dev liblcms2-dev libxml2-dev libxslt1-dev libssl-dev swig

echo "Setup mysql"
export DEBIAN_FRONTEND=noninteractive
apt-get -q -y install mysql-server mysql-client libmysqlclient-dev

# Call this to initialize mysqldb. It's dump from production made on 11-07-2014 but has broken migrations
#mysql -uroot -e "create database matchmaker;"
#mysql -uroot -e "GRANT ALL PRIVILEGES ON matchmaker.* TO vagrant@localhost IDENTIFIED BY 'vagrant'"
#mysql -uvagrant -pvagrant matchmaker < /vagrant/provisioning/dbdump.sql

echo "Setup pip and virtualenv"
pip install virtualenv
pip install virtualenvwrapper
pip install envdir
