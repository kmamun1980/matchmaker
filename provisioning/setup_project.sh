#!/usr/bin/env bash

echo "Create virtualenv"
mkdir /home/vagrant/envs
export WORKON_HOME=~/envs
source /usr/local/bin/virtualenvwrapper.sh
mkvirtualenv vagrant

echo 'Install app'
workon vagrant
pip install -r /vagrant/requirements/local.txt
cd /vagrant/
chmod +x manage.py

if [ ! -f = "/vagrant/MatchMaker/settings_local.py" ]
then
    cp -p /vagrant/MatchMaker/settings_local.py.example /vagrant/MatchMaker/settings_local.py
fi

if [ ! -f = "/vagrant/envdir/DJANGO_SETTINGS_MODULE" ]
then
    cp -p /vagrant/provisioning/conf/envdir/DJANGO_SETTINGS_MODULE /vagrant/envdir/DJANGO_SETTINGS_MODULE
fi

python manage.py syncdb --noinput
python manage.py migrate --noinput
python manage.py cities_light

cp -p /vagrant/provisioning/conf/.bashrc /home/vagrant/.bashrc

echo "Finished!"
echo "Please run 'vagrant ssh' to ssh to dev machine"
echo "Then run 'rs' to start django (in virtual machine, after vagrant ssh was called)"
echo "Then go to localhost:6399 in the browser (on the host, not in virtual machine)"
