export WORKON_HOME=~/envs
source /usr/local/bin/virtualenvwrapper.sh

workon vagrant
cd /vagrant/
alias rs="python /vagrant/manage.py runserver 0.0.0.0:6399"
