from tastypie.resources import ModelResource as _ModelResource
from django.db import models
from django.conf.urls import patterns, include, url
from django.db.models import Q, F

class ModelResource(_ModelResource):
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/search/$" % self._meta.resource_name, self.wrap_view('get_search'), name="api_get_search"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        query = request.GET.get('q', '')
        query_tokens = query.split(' ') # TODO: probably more sophisticated search query

        search_fields = self.search_fields if hasattr(self,'search_fields') else None
        if search_fields is None:
            search_fields = [
                (f.name + '__startswith') if isinstance(f,(models.CharField,models.TextField)) else f.name
                for f in self._meta.queryset.model._meta.fields if isinstance(f,(models.CharField,models.TextField,models.IntegerField))
            ]

        if not search_fields:
            raise Http404("Search is not possible here")

        qs = self.get_object_list(request)

        query_args_kw = []
        for word in query_tokens:
            for fname in search_fields:
                try:
                    #checking for compatibility
                    qs.filter(**{fname:word})
                except Exception,ex:
                    pass
                else:
                    query_args_kw.append({fname:word})

        #query_args_kw = [ {field:value} for field in search_fields for value in query_tokens ]

        query_args = [ models.Q(**kw) for kw in query_args_kw ]

        query_Q = models.Q()
        for q in query_args:
            query_Q = query_Q | q

        # Do the query (fixed copypast from tastypie!)
        base_bundle = self.build_bundle(request=request)
        objects = qs.filter(query_Q)
        objects = self.authorized_read_list(objects,base_bundle)
        #print "DEBUG HERE!!!!:",objects.count(),"!!",query_args_kw
        sorted_objects = self.apply_sorting(objects, options=request.GET)
        paginator = self._meta.paginator_class(request.GET, sorted_objects, resource_uri=self.get_resource_uri() + "search", limit=self._meta.limit, max_limit=self._meta.max_limit, collection_name=self._meta.collection_name)
        to_be_serialized = paginator.page()

        # Dehydrate the bundles in preparation for serialization.
        bundles = []

        for obj in to_be_serialized[self._meta.collection_name]:
            bundle = self.build_bundle(obj=obj, request=request)
            bundles.append(self.full_dehydrate(bundle, for_list=True))

        to_be_serialized[self._meta.collection_name] = bundles
        to_be_serialized = self.alter_list_data_to_serialize(request, to_be_serialized)
        self.log_throttled_access(request)
        return self.create_response(request, to_be_serialized)

    def available_resources(self):
        return {
            'list_endpoints': self.get_resource_uri(),
            'endpoint': self.get_resource_uri() + "<id>/",
            'schema': self.get_resource_uri()+'schema/',
            'search': self.get_resource_uri()+'search/',
        }

