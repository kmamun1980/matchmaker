from apps.company.models import Department,OrgCategory,Agency,Office,Industry,GVGPOrgProfile
def push(NSCode):
    import xlrd
    workbook = xlrd.open_workbook('csv/naics.xls')
    worksheet = workbook.sheet_by_name('2012NAICS')
    num_rows = worksheet.nrows - 1
    num_cells = worksheet.ncols - 1
    curr_row = -1
    while curr_row < num_rows:
        curr_row += 1
        row = worksheet.row(curr_row)

        curr_cell = -1


            # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank

        code = worksheet.cell_value(curr_row, 0)
        name = worksheet.cell_value(curr_row, 1)

        try:
            code = int(code)

            NSCode.objects.create(code=code,about=name)

            print '	', code
        except:

            print 'no code'+ str(code)



def depts():
    import xlrd
    workbook = xlrd.open_workbook('csv/feds.xls')
    worksheet = workbook.sheet_by_name('Sheet1')
    num_rows = worksheet.nrows - 1
    num_cells = worksheet.ncols - 1
    curr_row = -1
    while curr_row < num_rows:

        curr_row += 1
        row = worksheet.row(curr_row)

        curr_cell = -1


            # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank

        #code = worksheet.cell_value(curr_row, 0)

        dept = worksheet.cell_value(curr_row, 1)
        agency = worksheet.cell_value(curr_row, 2)
        office = worksheet.cell_value(curr_row, 4)
        industry = worksheet.cell_value(curr_row, 5)


        try:
            org = OrgCategory.objects.get(name='FED')
        except:
            org = OrgCategory.objects.create(name='FED')

        try:
            department = Department.objects.get(name=dept,org_category=org)
        except:
            department = Department.objects.create(name=dept,org_category=org)
        try:
            agency = Agency.objects.get(name=agency,department=department)
        except:
            agency = Agency.objects.create(name=agency,department=department)
        try:
            office = Office.objects.get(name=office,agency=agency)
        except:
            office = Office.objects.create(name=office,agency=agency)
        try:
            industry = Industry.objects.get(name=industry)
        except:
            industry = Industry.objects.create(name=industry)
        try:
            company = GVGPOrgProfile.objects.get(department=department,org_category=org,agency=agency,office=office,industry=industry)
        except:
            GVGPOrgProfile.objects.create(department=department,org_category=org,agency=agency,office=office,industry=industry)




def ind():
    import xlrd
    workbook = xlrd.open_workbook('csv/industries.xlsx')
    worksheet = workbook.sheet_by_name('Sheet1')
    num_rows = worksheet.nrows - 1
    num_cells = worksheet.ncols - 1
    curr_row = -1
    while curr_row < num_rows:

        curr_row += 1
        row = worksheet.row(curr_row)

        curr_cell = -1


            # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank

        #code = worksheet.cell_value(curr_row, 0)

        name = worksheet.cell_value(curr_row, 0)



        try:
            industry = Industry.objects.get(name=name)
        except:
            industry = Industry.objects.create(name=name)






