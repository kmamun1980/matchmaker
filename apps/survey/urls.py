from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^survey/(?P<s_id>\d+)/$', 'apps.survey.views.survey_detail', name='survey_detail'),
    url(r'^confirm/(?P<uuid>\w+)/$', 'apps.survey.views.confirm', name='confirmation'),
    url(r'^privacy/$', 'apps.survey.views.privacy', name='privacy_statement'),
    url(r'^add-question/(?P<s_id>\d+)/$', 'apps.survey.views.create_question', name='dynamic_question_add'),
    url(r'^edit-question/(?P<q_id>\d+)/$', 'apps.survey.views.edit_question', name='dynamic_question_edit'),
    url(r'^delete-survey/(?P<s_id>\d+)/$', 'apps.survey.views.delete_survey', name='delete_survey'),
    url(r'^delete-question/(?P<q_id>\d+)/$', 'apps.survey.views.delete_question', name='delete_question'),
    url(r'^add-survey/$', 'apps.survey.views.create_survey', name='dynamic_field_get'),
    url(r'^edit-survey/(?P<s_id>\d+)/$', 'apps.survey.views.edit_survey', name='survey_edit'),
    url(r'^my-surveys/$', 'apps.survey.views.my_surveys', name='all_surveys'),
    url(r'^view-survey/(?P<s_id>\d+)/$', 'apps.survey.views.view_survey', name='view_survey'),
    url(r'^send-survey/(?P<s_id>\d+)/$', 'apps.survey.views.send_survey', name='send_survey'),




)




