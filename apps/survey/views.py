from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core import urlresolvers
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from apps.events.models import Event,Activity,Attendee
from models import Question, Survey, Category,Response,AnswerBase
from forms import ResponseForm,SurveyForm
from django.conf import settings
from twilio.rest import TwilioRestClient
TWILIO_CLIENT = TwilioRestClient(settings.TWILIO_SID, settings.TWILIO_TOKEN)

@login_required
def survey_detail(request, s_id):
    survey = Survey.objects.get(id=s_id)
    category_items = Category.objects.filter(survey=survey)
    categories = [c.name for c in category_items]
    print 'categories for this survey:'
    print categories
    if request.method == 'POST':
        form = ResponseForm(request.POST, survey=survey)
        if form.is_valid():
            response = form.save()

            return HttpResponseRedirect("/confirm/%s" % response.interview_uuid)
    else:
        form = ResponseForm(survey=survey)

        # TODO sort by category
    return render_to_response('survey.html',
                              {'response_form': form, 'survey': survey, 'categories':

                                  categories},RequestContext(request))
@login_required
def confirm(request, uuid):
    email = 'survey@matchmakingsoftware.com'
    return render(request, 'confirm.html', {'uuid':uuid, "email": email})

@login_required
def privacy(request):
    return render(request, 'privacy.html')

@login_required
@csrf_exempt
def create_question(request, s_id):

    survey = Survey.objects.get(id=s_id)

    category = Category.objects.get(survey=survey.id)

    if request.method == 'POST':

        name = request.POST.get('name',None)
        required = request.POST.get('required',None)
        active = request.POST.get('active',False)
        question_type = request.POST.get('question_type',None)
        choices = request.POST.get('choices',None)

        if question_type == 'select':
            if not choices:
                error = 'For Radio,Checkbox and Select you need to enter choices'
                return HttpResponseRedirect('/view-survey/%s/'%str(s_id))
        if question_type == 'select-multiple':
            if not choices:
                error = 'For Radio,Checkbox and Select you need to enter choices'
                return HttpResponseRedirect('/view-survey/%s/'%str(s_id))
        if question_type == 'radio':
            if not choices:
                error = 'For Radio,Checkbox and Select you need to enter choices'
                return HttpResponseRedirect('/view-survey/%s/'%str(s_id))


        if active:
            active = True
        else:
            active = False
        if required:
            required = True
        else:
            required = False
        survey = Question.objects.create(text=name,question_type=question_type,survey=survey,
                                        required=required,choices=choices,category=category)
        return HttpResponseRedirect('/view-survey/%s/'%str(s_id))
    return  render_to_response('create_question.html',locals(),RequestContext(request))

@login_required
@csrf_exempt
def edit_question(request, q_id):
    q = Question.objects.get(id=q_id)
    if request.method == 'POST':
        name = request.POST.get('name',None)
        required = request.POST.get('required',None)
        active = request.POST.get('active',False)
        question_type = request.POST.get('question_type',None)
        choices = request.POST.get('choices',None)

        if question_type == 'select':
            if not choices:
                error = 'For Radio,Checkbox and Select you need to enter choices'
                return HttpResponseRedirect('/view-survey/%s/'%str(q_id))
        if question_type == 'select-multiple':
            if not choices:
                error = 'For Radio,Checkbox and Select you need to enter choices'
                return HttpResponseRedirect('/view-survey/%s/'%str(q_id))
        if question_type == 'radio':
            if not choices:
                error = 'For Radio,Checkbox and Select you need to enter choices'
                return HttpResponseRedirect('/view-survey/%s/'%str(q_id))



        if active:
            active = True
        else:
            active = False
        if required:
            required = True
        else:
            required = False
        q.name = name
        q.required = required
        q.active = active
        q.question_type = question_type
        q.choices = choices
        q.save()

        return HttpResponseRedirect('/view-survey/%s/'%str(q.survey_id))
    return  render_to_response('edit_question.html',locals(),RequestContext(request))


@login_required
@csrf_exempt
def delete_question(request, q_id):
    q = Question.objects.get(id=q_id)
    survey = q.survey.id
    q.delete()
    return HttpResponseRedirect('/view-survey/%s/'%str(survey))

@csrf_exempt
def delete_survey(request, s_id):
    s = Survey.objects.get(id=s_id)
    s.delete()
    return HttpResponseRedirect('/my-surveys/')


@login_required
@csrf_exempt
def create_survey(request):
    if request.method == 'POST':
        name = request.POST.get('name', None)
        description = request.POST.get('description', None)
        event = request.POST.get('event', None)
        activity = request.POST.get('activity', None)

        survey = Survey.objects.create(name=name,creator=request.user,description=description)
        category = Category.objects.create(survey=survey, name=survey.name)
        if activity:
            activity = Activity.objects.get(id=activity)
            event = activity.event
            survey.activity = activity
            survey.event = activity.event
        elif event:
            event = Event.objects.get(id=event)
            survey.event = event
            survey.save()
        else:
            pass

        survey.save()
        category.save()
        return HttpResponseRedirect('/view-survey/%s/'%str(survey.id))
    form = SurveyForm()
    return render_to_response('create_survey.html', locals(), RequestContext(request))


@csrf_exempt
def edit_survey(request,s_id):
    survey = Survey.objects.get(id=s_id)
    if request.method == 'POST':
        name = request.POST.get('name',None)
        description = request.POST.get('description', None)
        event = request.POST.get('event', None)
        activity = request.POST.get('activity', None)

        if description:
            survey.description = description
        if name:
            survey.name = name

        if activity:
            activity = Activity.objects.get(id=activity)
            event = activity.event
            survey.activity = activity
            survey.event = activity.event
        elif event:
            event = Event.objects.get(id=event)
            survey.event = event

        else:
            pass
        survey.save()


        return HttpResponseRedirect('/view-survey/%s/'%str(survey.id))
    form = SurveyForm(instance=survey)
    return render_to_response('edit_survey.html', locals(), RequestContext(request))


@login_required
@csrf_exempt
def my_surveys(request):
    surveys = Survey.objects.filter(creator=request.user)
    total =  surveys.count()
    sent = surveys.filter(active= True)
    return render_to_response('my_surveys.html', locals(),RequestContext(request))

@csrf_exempt
def view_survey(request, s_id):

    survey = Survey.objects.get(creator=request.user, id=s_id)
    questions = Question.objects.filter(survey=survey.id)
    return render_to_response('survey_detail.html', locals(),RequestContext(request))

@login_required
@csrf_exempt
def survey_response(request, s_id):

    survey = Survey.objects.get(creator = request.user,id=s_id)
    response = [r_id for r_id in Response.objects.filter(survey=survey.id).id]
    answers = AnswerBase.objects.filter(response__in = response)
    return render_to_response('survey_responses.html', locals(),RequestContext(request))

@login_required
@csrf_exempt
def send_survey(request, s_id):
    # get event for this survey
    survey = Survey.objects.get(id=s_id)
    event = survey.event

    # get attendees for the event
    if event:

        att =  Attendee.objects.filter(event = event.id)
        if att:
            for i in att:
                # email user
                try:
                    i.user.email_user('Subject','Please attempt the survey here','benfatola@cloudcustomsolutions.com')
                except:
                    pass  # TODO: logger.error() - fail silently for now

                # sms users
                try:
                    TWILIO_CLIENT.sms.messages.create(
                        body='Survey Link click here', to=i.user.phone, from_=settings.TWILIO_NUMBER)
                except:
                    pass  # TODO: logger.error() - fail silently for now


        survey.active = True
        survey.save()
    return HttpResponseRedirect('/view-survey/%s/'%survey.id)
