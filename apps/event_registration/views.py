from django.shortcuts import render_to_response,RequestContext
from apps.events.models import Event,Activity,Attendee,ActivityType,ActivityAttendee
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import HttpResponse,HttpResponseRedirect
from django.conf import settings
import stripe
import dateutil.parser
from models import EventTicket,TicketPurchase


def share_event(request,e_id):
    event = Event.objects.filter(id=e_id)
    if event:

        event = event[0]
    return render_to_response('user_events.html'
                              ,locals(),RequestContext(request))

def other_events(request):
    rsvp = Attendee.objects.filter(user = request.user)
    if rsvp:
        rsvp = [rsvp.event.id for rsvp in rsvp]

    events = Event.objects.exclude(id__in=rsvp).order_by('-created')
    return render_to_response('other_events.html'
                              ,locals(),RequestContext(request))

@login_required
@csrf_exempt
def get_activities(request,e_id):
    event = Event.objects.get(id=e_id)
    activities = Activity.objects.filter(event=e_id)
    act_list = set([act.type_id for act in activities])
    actlist = ActivityType.objects.filter(id__in=list(act_list))

    #return render_to_response('details.html',locals(),RequestContext(request))
    return render_to_response('activities.html',locals(),RequestContext(request))

@login_required
@csrf_exempt
def get_activity_type(request,e_id):

    event_id = e_id.split('_')[1]
    type_id = e_id.split('_')[0]

    event = Event.objects.get(id=event_id)
    act_type = ActivityType.objects.get(id=type_id)

    activities = Activity.objects.filter(event = event_id,type=type_id)
    activities_attendees = [act.id for act in ActivityAttendee.objects.filter(user=request.user)]


    return render_to_response('details.html',locals(),RequestContext(request))



@login_required
@csrf_exempt
def event_register(request,e_id):
    event = Event.objects.get(id=e_id)
    if event.is_paid:
        return HttpResponse(json.dumps({'success':'false','id':e_id}))

    else:
        if Attendee.objects.filter(user=request.user,event=event).exists():
            return HttpResponse(json.dumps({'success':'true','message':'already'}))
        else:
            rsvp = Attendee.objects.create(user=request.user,event=event)
            return HttpResponse(json.dumps({'success':'true','message':'already rsvp'}))




@login_required
@csrf_exempt
def get_pricing(request,e_id):
    plans = EventTicket.objects.filter(event=int(e_id))

    return render_to_response('pricing-table.html',locals(),RequestContext(request))



@login_required
@csrf_exempt
def pay_event(request,e_id):
    plan = EventTicket.objects.get(id=e_id)

    if request.method == 'POST':

            stripe.api_key = settings.STRIPE_SECRET_KEY

            # Get the credit card details submitted by the form
            token = request.POST['stripeToken']

            # Create the charge on Stripe's servers - this will charge the user's card
            try:
              charge = stripe.Charge.create(
                  amount=int(plan.price)*100, # amount in cents, again
                  currency="usd",
                  card=token,
                  description= plan.name
              )

              rsvp = Attendee.objects.create(user=request.user,event=plan.event)
              TicketPurchase.objects.create(amount=charge.amount/100,attendee=rsvp,trx_id=charge.id,event_id=e_id,
                                            email=request.user.email,name=request.user.username)
              return HttpResponseRedirect('/share-event/'+str(e_id)+'/')


            except stripe.CardError, e:
              return HttpResponseRedirect('/other-events/')


    else:
        event = Event.objects.filter(id=e_id)
        if event:
            event = event[0]
            activities = Activity.objects.filter(event=event)
        return render_to_response('event_reg.html',locals(),RequestContext(request))


def create_ticket(request,e_id):
    if request.method== 'POST':
        name = request.POST.get('name',None)
        price = request.POST.get('price',None)
        open_date = request.POST.get('open_date',None)
        close_date = request.POST.get('close_date',None)
        description  = request.POST.get('description',None)
        default_ticket = request.POST.get('popular',False)
        if default_ticket:
            default_ticket = True


        close_date = dateutil.parser.parse(close_date)
        open_date = dateutil.parser.parse(open_date)


        ticket = EventTicket.objects.create(name=name,price=price,open_date=open_date,
                                    default_ticket=default_ticket,creator=request.user,event_id=e_id,
                                    close_date=close_date,description=description)
    if Event.objects.filter(id=e_id).exists():
        event = Event.objects.get(id=e_id)
    else:
        return HttpResponseRedirect('/events/')




    tickets = EventTicket.objects.filter(event=e_id)

    return render_to_response('create_ticket.html',locals(),RequestContext(request))