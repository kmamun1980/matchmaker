from django.conf.urls import patterns, include, url

urlpatterns = patterns('',


    url(r'^share-event/(?P<e_id>\d+)/$', 'apps.event_registration.views.share_event', name='share-event'),
    url(r'^upcoming-events/$', 'apps.event_registration.views.other_events', name='upcoming-events'),
    url(r'^get-activities/(?P<e_id>\d+)/$', 'apps.event_registration.views.get_activities', name='get_activities'),
    url(r'^register-event/(?P<e_id>\d+)/$', 'apps.event_registration.views.event_register', name='event-register'),
    url(r'^pay-for-event/(?P<e_id>\d+)/$', 'apps.event_registration.views.pay_event', name='pay-event'),
    url(r'^get-pricing/(?P<e_id>\d+)/$', 'apps.event_registration.views.get_pricing', name='pricing'),
    url(r'^get-activity-type/(?P<e_id>\w+)/', 'apps.event_registration.views.get_activity_type', name='get_activity-type'),
    url(r'^create-ticket/(?P<e_id>\w+)/$', 'apps.event_registration.views.create_ticket', name='create-ticket'),

    )


