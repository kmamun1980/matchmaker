from django.db import models
from apps.events.models import Attendee,Event
from decimal import Decimal
from django.utils.translation import ugettext_lazy as _
from apps.users.models import User



class EventTicket(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    ticket_quantity = models.IntegerField(default=0)
    default_ticket = models.BooleanField(default=True,help_text="Is this the default Ticket")
    event = models.ForeignKey(Event,blank=True,null=True)
    available = models.BooleanField(_('available'), default=False, db_index=True,
                                    help_text=_('Is still available for purchase'))
    open_date = models.DateTimeField(blank=True,null=True)
    close_date = models.DateTimeField(blank=True,null=True)

    created = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(User,null=True, blank=True)
    price =  models.DecimalField(max_digits=20,decimal_places=1,default=Decimal('0.0'))

    def __str__(self):
        return self.name


    def __unicode__(self):
        return self.name

    def tickets_available(self):
        return max(self.ticket_quantity - self.tickets.count(),0)

    def get_absolute_url(self):
        return "/tickets/" + self.id

    @property
    def stripe_amount(self):
        return int(self.price*100)


class TicketPurchase(models.Model):
    amount = models.FloatField()
    email = models.EmailField(default='',verbose_name="email")
    name = models.CharField(max_length=100)
    status = models.CharField(default='Approved',max_length=20)
    attendee = models.ForeignKey(Attendee)
    price = models.DecimalField(max_digits=20,decimal_places=4,default=Decimal('0.00'))
    trx_id = models.CharField(max_length=255,blank=True,null=True)
    created = models.DateField(auto_now_add=True,editable=False)
    updated = models.DateTimeField(auto_now=True,editable=False)
    event = models.ForeignKey(Event,related_name="tickets")

    def __unicode__(self):
        return self.attendee or self.purchase.name

    def paid(self):
        return self.purchase.paid()



    def __unicode__(self):
        return "%s (%s)" % (self.name, self.status)

    def paid(self):
        return "completed" in self.status

    def invoice(self):
        return self.id



