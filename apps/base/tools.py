import re


def youtube_video_id_extractor(url):
    if url:
        pattern = r'^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*'

        match = re.match(pattern, url)
        if match:
            video_id = match.group(2)
            if len(video_id) == 11:
                return video_id

    return None

