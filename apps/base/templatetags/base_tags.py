import re

from django import template
from django.core.urlresolvers import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def is_active_url(context, name, by_path=False):
    """
    Return the string 'active' if current request.path is same as name
    Keyword arguments:
    request -- Django request object
    name -- name of the url or the actual path
    by_path -- True if name contains a url instead of url name
    """
    request = context['request']
    if by_path:
        if re.search(name, request.path):
            return u'active'
    else:
        path = reverse(name)
        if request.path == path:
            return u'active'
    return u''

