from django.db import models
from apps.users.models import User

# Create your models here.


class Category(models.Model):

    name = models.CharField(max_length=255, blank=True, null=True, verbose_name='Enter new category name')

    def __unicode__(self):
        return self.name


class Favorites(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    category = models.ForeignKey(Category, max_length=255, blank=True, null=True)
    favorite_results = models.ManyToManyField(User, blank=True, null=True, related_name='user_favorite_results')

    def __unicode__(self):
        return self.user