from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt

from apps.users.models import User, NSCode, VOSB, VOSBExperience
from apps.users.decorators import profile_complete_required
from forms import ProfileForm
from apps.search.forms import CategoryForm, FavoritesForm
from apps.search.models import Category, Favorites
from apps.company.models import NSCode
from django.http import HttpResponse
from django.utils.encoding import smart_str
import csv
import json


def basic_search_new(request):
    form = ProfileForm()
    params = {
      'form' : form
    }
    return render_to_response('search/search_new.html', params, RequestContext(request))    

@profile_complete_required
def basic_search(request):
    if request.method == 'POST':
        gov = request.POST.get('gov')
        code = request.POST.get('code')
        city = request.POST.get('city')
        country = request.POST.get('country')
        region = request.POST.get('region')
        years = request.POST.get('yrs')
        revenue = request.POST.get('revenue')

        users = User.objects.all()
        if int(years) is not 0:
            #yr1 = years.split(';')[0]
            #yr2 = years.split(';')[1]

            users = users.filter(years_of_exp__gte=years)
        if gov:
            if gov == '1':
                users = users.filter(vgp_type=1)
            elif gov == '2':
                users = users.filter(vgp_type=2)
            else:
                pass

        if code:
            users = users.filter(naics__id=code)

        if int(revenue) is not 0:
            #rev1 = revenue.split(';')[0]
            #rev2 = revenue.split(';')[1]

            users = users.filter(commmercial_org__avg_rev__gte=revenue)

        if country:
            users = users.filter(country=int(country))

        if region:
            users = users.filter(region=int(region))

        if city:
            users = users.filter(city=int(city))

    else:
        users = User.objects.exclude(id=request.user.id).none()

    data = []
    if request.method == 'POST':
        for user in users:
            user2 = [code.code for code in user.naics.all()]
            user1 = [code.code for code in request.user.naics.all()]
            x = set(user1).intersection(user2)

            if len(x) < len(user1):
                match = int(float(len(x))/float(len(user1))*float(100))
            else:
                match = 100
            data.append({'user': user, 'match': match})

    users = data

    form = ProfileForm()
#    codes = NSCode.objects.all()

    return render_to_response(
        'search/search.html', locals(), RequestContext(request))


@profile_complete_required
def advanced_search(request):

    data_list = []
    match_total = 0

    users = User.objects.exclude(id=request.user.id)

    favorites_form = FavoritesForm()
    category_form = CategoryForm()

    data = {'users': data_list, 'favorites': favorites_form, 'category': category_form}


    if request.is_ajax() and request.method == 'GET':

        favorite_results = request.POST.get('favorite_results')
        name = request.POST.get('name')
        category = request.POST.get('category')

        if favorite_results and (name or category):

            form = FavoritesForm(request.POST)

            if form.is_valid():
                fav_form = form.save(commit=False)

                if name:
                    obj, fav_category = Category.objects.get_or_create(name=name)
                    fav_form.category = obj  # Category.objects.get(name=name)

                fav_form.user = request.user
                fav_form.save()

        return HttpResponse(json.dumps({'success': 'true'}))

    if request.method == 'POST':

        request_format = request.POST.get('myFormat')

        years_in_business = request.POST.get('yib')
        years_in_business_weight = request.POST.get('yib-weight')

        verified = request.POST.get('verified', '')
        verified_weight = request.POST.get('verified-weight')

        contract_vehicle = request.POST.getlist('contract-vehicle')
        contract_vehicle_weight = request.POST.getlist('contract-vehicle-weight')
        for c_v, c_v_w in zip(contract_vehicle, contract_vehicle_weight):
            if c_v == '':
                contract_vehicle_weight.remove(c_v_w)
        contract_vehicle = filter(None, contract_vehicle)

        revenue = request.POST.get('revenue')
        revenue_weight = request.POST.get('revenue-weight')

        primary_naics = request.POST.getlist('primary-naics')
        primary_naics_weight = request.POST.get('primary-naics-weight')

        secondary_naics = request.POST.getlist('secondary-naics')
        secondary_naics_weight = request.POST.get('secondary-naics-weight')

        no_of_employees = request.POST.get('no-of-employees')
        no_of_employees_weight = request.POST.get('no-of-employees-weight')

        no_of_veteran_employees = request.POST.get('no-of-veteran-employees')
        no_of_veteran_employees_weight = request.POST.get('no-of-veteran-employees-weight')

        percent_of_veteran_employees = request.POST.get('percent-of-veteran-employees')
        percent_of_veteran_employees_weight = request.POST.get('percent-of-veteran-employees-weight')

        dcaa_compliant = request.POST.get('dcaa-compliant', '')
        dcaa_compliant_weight = request.POST.get('dcaa-compliant-weight')

        first_core_strength = request.POST.get('first-core-strength')
        first_core_strength_weight = request.POST.get('first-core-strength-weight')

        second_core_strength = request.POST.getlist('second-core-strength')
        second_core_strength_weight = request.POST.getlist('second-core-strength-weight')
        for c_s_s_w, c_s_s in zip(second_core_strength, second_core_strength_weight):
            if c_s_s == '':
                second_core_strength_weight.remove(c_s_s_w)
        second_core_strength = filter(None, second_core_strength)

        business_certification = request.POST.getlist('business-certification')
        business_certification_weight = request.POST.getlist('business-certification-weight')
        for b_c, b_c_w in zip(business_certification, business_certification_weight):
            if b_c == '':
                business_certification_weight.remove(b_c_w)
        business_certification = filter(None, business_certification)

        small_business_category = request.POST.getlist('small-business-category')
        small_business_category_weight = request.POST.getlist('small-business-category-weight')
        for s_b_c, s_b_c_w in zip(small_business_category, small_business_category_weight):
            if s_b_c == '':
                small_business_category_weight.remove(s_b_c_w)
        small_business_category = filter(None, small_business_category)


        naics = request.POST.getlist('pp-naics')
        naics_weight = request.POST.get('pp-naics-weight')

        prime = request.POST.getlist('pp-prime')
        prime_weight = request.POST.getlist('pp-prime-weight')
        for p, p_w in zip(prime, prime_weight):
            if p == '':
                prime_weight.remove(p_w)
        prime = filter(None, prime)

        past_p_estimated_value = request.POST.getlist('pp-estimated-value')
        past_p_estimated_value_weight = request.POST.getlist('pp-estimated-value-weight')
        for p_p_e_v, p_p_e_v_w in zip(past_p_estimated_value, past_p_estimated_value_weight):
            if p_p_e_v == '':
                past_p_estimated_value_weight.remove(p_p_e_v_w)
        past_p_estimated_value = filter(None, past_p_estimated_value)

        past_p_pop_start = request.POST.getlist('pp-pop-start')
        past_p_pop_end = request.POST.getlist('pp-pop-end')
        past_p_pop_time_weight = request.POST.getlist('pp-pop-time-weight')
        for p_p_p_s, p_p_p_e, p_p_p_t_w in zip(past_p_pop_start, past_p_pop_end, past_p_pop_time_weight):
            if p_p_p_s == p_p_p_e:
                past_p_pop_start.remove(p_p_p_s)
                past_p_pop_end.remove(p_p_p_e)
                past_p_pop_time_weight.remove(p_p_p_t_w)

        past_p_customer_dept = request.POST.getlist('pp-customer-dept')
        past_p_customer_dept_weight = request.POST.getlist('pp-customer-weight')
        for dept, dept_w in zip(past_p_customer_dept, past_p_customer_dept_weight):
            if dept == '':
                past_p_customer_dept_weight.remove(dept_w)
        past_p_customer_dept = filter(None, past_p_customer_dept)

        def append_results_in_data_list(user_var, weight):

            try:
                d = next((item for item in data_list if item['user'] == user_var))
                d['match'] += int(weight)
            except:
                data_list.append({'user': user_var, 'match': int(weight)})

        if int(years_in_business) is not 0:

            match_total += int(years_in_business_weight)

            users_yib = users.filter(years_of_exp__gte=int(years_in_business)).exclude(is_superuser=True)

            for user in users_yib:

                append_results_in_data_list(user, years_in_business_weight)

        if verified:

            match_total += int(verified_weight)

            if verified == 'true':
                users_verified = users.filter(verified=True)
            else:
                users_verified = users.filter(verified=False)

            for user in users_verified:

                append_results_in_data_list(user, verified_weight)

        if contract_vehicle:

            weight_iter = 0
            for c_vehicle in contract_vehicle:

                    match_total += int(contract_vehicle_weight[weight_iter])

                    users_vosb_exp_contract_vehicle = VOSBExperience.objects.filter(customer=c_vehicle)
                    users_vosb_contract_vehicle = VOSB.objects.filter(customer=c_vehicle)

                    for c_v in users_vosb_exp_contract_vehicle:

                        append_results_in_data_list(c_v.user, contract_vehicle_weight[weight_iter])

                    for c_v in users_vosb_contract_vehicle:

                        append_results_in_data_list(c_v.user, contract_vehicle_weight[weight_iter])
                    weight_iter += 1

        if revenue:

            match_total += int(revenue_weight)
            revenue = revenue.split('-')

            revenue1 = revenue[0]
            revenue2 = revenue[1]

            if revenue1 != 'gt':
                users_revenue = users.filter(commmercial_org__avg_rev__range=(int(revenue1), int(revenue2)))

            else:
                users_revenue = users.filter(commmercial_org__avg_rev__gte=int(revenue2))

            for user in users_revenue:

                append_results_in_data_list(user, revenue_weight)

            revenue = '-'.join(revenue)

        if primary_naics:

            match_total += int(primary_naics_weight)

            users_primary_naics = VOSB.objects.filter(primary_naics__code__iexact=primary_naics)

            for p_naics in users_primary_naics:

                append_results_in_data_list(p_naics.user, primary_naics_weight)

        if secondary_naics:

            weight_iter = 0
            for s_naic in secondary_naics:

                match_total += int(secondary_naics_weight)

                users_secondary_naics = VOSB.objects.filter(secondary_naics__code__iexact=s_naic)

                for s_naics in users_secondary_naics:

                    append_results_in_data_list(s_naics.user, secondary_naics_weight)

                weight_iter += 1

        if no_of_employees:

            match_total += int(no_of_employees_weight)
            no_of_employees = no_of_employees.split('-')
            no_of_employees1 = no_of_employees[0]
            no_of_employees2 = no_of_employees[1]

            if no_of_employees1 != 'gt':
                users_employees = users.filter(commmercial_org__total_employees__range=(int(no_of_employees1), int(no_of_employees2)))

            else:
                users_employees = users.filter(commmercial_org__total_employees__gte=no_of_employees2)

            for user in users_employees:

                append_results_in_data_list(user, no_of_employees_weight)

        if no_of_veteran_employees:

            match_total += int(no_of_veteran_employees_weight)
            no_of_veteran_employees = no_of_veteran_employees.split('-')
            no_of_veteran_employees1 = no_of_veteran_employees[0]
            no_of_veteran_employees2 = no_of_veteran_employees[1]

            if no_of_veteran_employees1 != 'gt':
                users_vet_employees = users.filter(commmercial_org__vet_employees__range=
                                                   (no_of_veteran_employees1, no_of_veteran_employees2))

            else:
                users_vet_employees = users.filter(commmercial_org__vet_employees__gte=int(no_of_veteran_employees2))

            for user in users_vet_employees:

                append_results_in_data_list(user, no_of_veteran_employees_weight)

        if percent_of_veteran_employees:

            match_total += int(percent_of_veteran_employees_weight)
            percent_of_veteran_employees = percent_of_veteran_employees.split('-')
            percent_of_veteran_employees1 = percent_of_veteran_employees[0]
            percent_of_veteran_employees2 = percent_of_veteran_employees[1]

            for user in users:
                if user.commmercial_org is not None:

                    percentage = user.commmercial_org.percentage_vet_employees()

                    if percent_of_veteran_employees1 != 'gt':

                        if int(percent_of_veteran_employees1) <= percentage <= int(percent_of_veteran_employees2):

                            append_results_in_data_list(user, percent_of_veteran_employees_weight)

                    else:

                        if percentage >= int(percent_of_veteran_employees2):

                            append_results_in_data_list(user, percent_of_veteran_employees_weight)

        if dcaa_compliant:

            match_total += int(dcaa_compliant_weight)

            if dcaa_compliant == 'true':
                users_dcaa_compliant = users.exclude(commmercial_org__dcaa_compliant_acc=None)
            else:
                users_dcaa_compliant = users.filter(commmercial_org__dcaa_compliant_acc=None)

            for user in users_dcaa_compliant:

                append_results_in_data_list(user, dcaa_compliant_weight)

        if first_core_strength:

            match_total += int(first_core_strength_weight)

            users_first_core_strength = VOSBExperience.objects.filter(first_core_strength__icontains=first_core_strength)

            for f_c_s in users_first_core_strength:

                append_results_in_data_list(f_c_s.user, first_core_strength_weight)

        if second_core_strength:

            weight_iter = 0
            for s_core_strength in second_core_strength:

                match_total += int(second_core_strength_weight[weight_iter])

                users_second_core_strength = VOSBExperience.objects.filter(second_core_strength__icontains=s_core_strength)

                for s_c_s in users_second_core_strength:

                    append_results_in_data_list(s_c_s.user, second_core_strength_weight[weight_iter])
                weight_iter += 1

        #TODO business certification

        if small_business_category:

            weight_iter = 0
            for small_business in small_business_category:

                match_total += int(small_business_category_weight[weight_iter])

                users_small_business_category = users.filter(
                    commmercial_org__small_business_category__name__iexact=small_business)

                for user in users_small_business_category:

                    append_results_in_data_list(user, small_business_category_weight[weight_iter])

                weight_iter += 1

        if naics:

            weight_iter = 0
            for naic in naics:

                match_total += int(naics_weight)

                users_naics = users.filter(naics__code__iexact=naic)

                for user in users_naics:

                    append_results_in_data_list(user, naics_weight)

                weight_iter += 1

        if prime:

            weight_iter = 0
            for p in prime:

                match_total += int(prime_weight[weight_iter])

                users_vosb_exp_prime = VOSBExperience.objects.filter(prime_sub=p)
                users_vosb_prime = VOSB.objects.filter(prime_sub=p)

                for prime in users_vosb_exp_prime:

                    append_results_in_data_list(prime.user, prime_weight[weight_iter])

                for prime in users_vosb_prime:

                    append_results_in_data_list(prime.user, prime_weight[weight_iter])

                weight_iter += 1

        #TODO estimated_value

        if past_p_pop_start and past_p_pop_end:

            weight_iter = 0
            for p_start in past_p_pop_start:

                if p_start != '' and past_p_pop_end[weight_iter] != '':


                    match_total += int(past_p_pop_time_weight[weight_iter])

                    users_vosb_exp_time = VOSBExperience.objects.filter(
                        pop_start=p_start, pop_finish=past_p_pop_end[weight_iter])

                    #VOSB Model needs the query too
                    #users_vosb_prime = VOSB.objects.filter(
                    # pop_start=past_p_pop_start, pop_finish=past_p_pop_end)

                    for time in users_vosb_exp_time:

                        append_results_in_data_list(time.user, past_p_pop_time_weight[weight_iter])

                    #VOSB model query result
                    #for prime in users_vosb_prime:
                    #    append_results_in_data_list(prime.user, past_p_pop_time_weight[weight_iter])
                weight_iter += 1

        if past_p_customer_dept:

            weight_iter = 0
            for customer_dept in past_p_customer_dept:

                match_total += int(past_p_customer_dept_weight[weight_iter])

                users_vosb_exp_customer_dept = VOSBExperience.objects.filter(customer=customer_dept)
                users_vosb_customer_dept = VOSB.objects.filter(customer=customer_dept)

                for dept in users_vosb_exp_customer_dept:

                    append_results_in_data_list(dept.user, past_p_customer_dept_weight[weight_iter])

                for dept in users_vosb_customer_dept:

                    append_results_in_data_list(dept.user, past_p_customer_dept_weight[weight_iter])

                weight_iter += 1

        for user in data_list:

            try:
                user['match'] = int((float(user['match'])/float(match_total))*100)

            except:
                pass

        search_parameters = {
            'years_in_business': years_in_business,
            'years_in_business_weight': years_in_business_weight,
            'verified': verified,
            'verified_weight': verified_weight,
            'contract_vehicle': contract_vehicle,  # list
            'contract_vehicle_weight': contract_vehicle_weight,  # list
            'revenue': revenue,
            'revenue_weight': revenue_weight,
            'primary_naics': primary_naics,
            'primary_naics_weight': primary_naics_weight,
            'secondary_naics': secondary_naics,  # list
            'secondary_naics_weight': secondary_naics_weight,  # list
            'no_of_employees': no_of_employees,
            'no_of_employees_weight': no_of_employees_weight,
            'no_of_veteran_employees': no_of_veteran_employees,
            'no_of_veteran_employees_weight': no_of_veteran_employees_weight,
            'percent_of_veteran_employees': percent_of_veteran_employees,
            'percent_of_veteran_employees_weight': percent_of_veteran_employees_weight,
            'dcaa_compliant': dcaa_compliant,
            'dcaa_compliant_weight': dcaa_compliant_weight,
            'first_core_strength': first_core_strength,
            'first_core_strength_weight': first_core_strength_weight,
            'second_core_strength': second_core_strength,  # list
            'second_core_strength_weight': second_core_strength_weight,  # list
            'business_certification': business_certification,
            'business_certification_weight': business_certification_weight,
            'small_business_category': small_business_category,  # list
            'small_business_category_weight': small_business_category_weight,  # list
            'naics': naics,  # list
            'naics_weight': naics_weight,  # list
            'prime': prime,  # list
            'prime_weight': prime_weight,  # list
            'past_p_estimated_value': past_p_estimated_value,  # list
            'past_p_estimated_value_weight': past_p_estimated_value_weight,  # list
            'past_p_pop_start': past_p_pop_start,   # list
            'past_p_pop_start_weight': None,   # dummy
            'past_p_pop_end': past_p_pop_end,  # list
            'past_p_pop_end_weight': past_p_pop_end,  # dummy
            'past_p_pop_time_weight': past_p_pop_time_weight,   # list
            'past_p_pop_time_weight_weight': past_p_pop_time_weight,   # dummy
            'past_p_customer_dept': past_p_customer_dept,   # list
            'past_p_customer_dept_weight': past_p_customer_dept_weight,  # list
        }

        data = {'users': data_list, 'favorites': favorites_form, 'category': category_form, }

        data.update(search_parameters)


    if request.method == 'POST' and request_format == 'MM_CUSTOM_FORMAT':  # Request from export form

        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=search_results.csv'
        writer = csv.writer(response, csv.excel)
        response.write(u'\ufeff'.encode('utf8'))
        writer.writerow([
            smart_str(u"SEARCH PARAMETER"),
            smart_str(u"VALUE"),
            smart_str(u"Parameter Weight"),
            smart_str(u"Value"),
        ])

        for key in search_parameters.keys():
            if search_parameters[key] and not search_parameters[key] == str(0):
                if key.split('_')[-1] != 'weight':
                    writer.writerow([
                        smart_str(key.replace('_', ' ')),
                        smart_str(search_parameters[key]),
                        smart_str(u"Weight"),
                        smart_str(search_parameters[key+'_weight']),
                    ])

        writer.writerow(u"")
        writer.writerow(u"")
        writer.writerow(u"")
        writer.writerow(u"")

        writer.writerow([
            smart_str(u"USER SEARCH RESULTS"),

        ])

        writer.writerow([
            smart_str(u"First Name"),
            smart_str(u"Last Name"),
            smart_str(u"Company Name"),
            smart_str(u"%age score"),
        ])

        writer.writerow(u"")

        for obj in data_list:

            writer.writerow([
                smart_str(obj['user'].first_name),
                smart_str(obj['user'].last_name),
                smart_str(obj['user'].company),
                smart_str(obj['match']),
            ])

            writer.writerow(u"")

        return response

    ns_codes = NSCode.objects.all()
    first_strength = VOSBExperience.objects.all()
    codes = {'codes': ns_codes, 'first_strength': first_strength}
    data.update(codes)

    return render_to_response('search/search.html', data , RequestContext(request))
