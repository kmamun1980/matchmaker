from django.conf.urls import patterns, include, url

urlpatterns = patterns('',

    url(r'^basic_search/$', 'apps.search.views.basic_search', name='basic_search'),
    url(r'^advanced_search/$', 'apps.search.views.advanced_search', name='advanced_search'),

    )


