from django import forms
from apps.users.models import User
from apps.search.models import Category, Favorites


class ProfileForm(forms.ModelForm):

    class Meta:
        model = User
        exclude = ['codes']


class CategoryForm(forms.ModelForm):

    class Meta:
        model = Category


class FavoritesForm(forms.ModelForm):

    class Meta:
        model = Favorites
        exclude = ['user', 'favorite_results']