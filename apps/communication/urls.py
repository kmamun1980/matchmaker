from django.conf.urls import patterns, include, url

urlpatterns = patterns('',

    url(r'^message-campaign/$', 'apps.communication.views.broadcast', name='message-campaign'),
    url(r'^message-email/$', 'apps.communication.views.broadcast', name='message-email'),
    url(r'^message-sms/$', 'apps.communication.views.message_sms', name='message-sms'),
    url(r'^message/call/(?P<message_id>\d+)$', 'apps.communication.views.message_call', name='message-call'),

    url(r'^message-old/$', 'apps.communication.views.broadcast', name='broad-cast'),
    url(r'^email-list/$', 'apps.communication.views.email_list', name='email-list'),
    url(r'^email-compose/$', 'apps.communication.views.email_compose', name='email_compose'),
    url(r'^email-open/(?P<m_id>\d+)/$', 'apps.communication.views.email_open', name='email_open'),



    )


