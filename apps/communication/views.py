from django.shortcuts import render_to_response,RequestContext
from django.views.decorators.csrf import csrf_exempt
from models import Message
from apps.events.models import Event,Activity,Attendee,ActivityAttendee,User
from django.http import HttpResponse
import json
from forms import MessageForm
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from twilio.rest import TwilioRestClient
# For escaping XML
from xml.sax.saxutils import escape

TWILIO_CLIENT = TwilioRestClient(settings.TWILIO_SID, settings.TWILIO_TOKEN)

def broadcast(request):
    return render_to_response('broadcast.html',locals(),RequestContext(request))

def message_sms(request):
    users = User.objects.all()
    events = Event.objects.all()
    activity = Activity.objects.all()
    # TODO: form = MessageSMSForm()

    return render_to_response('message-sms.html',locals(),RequestContext(request))

@csrf_exempt
def email_compose(request):
    if request.method=='POST':
        send_to = request.POST.get('recipients',None)
        voice_txt = request.POST.get('voice_txt',None)
        email_txt = request.POST.get('email_txt',None)
        sms_txt = request.POST.get('sms_txt',None)
        email_subject = request.POST.get('subject',None)
        user = request.user
        event = request.POST.get('events',None)
        activity = request.POST.get('activities',None)
        users = request.POST.getlist('users',None)
        send_at = request.POST.get('send_at',None)
        attachment = request.FILES.get('att',None)


        message = Message.objects.create(voice_txt=voice_txt,email_txt=email_txt,
                                         sms_txt=sms_txt,email_subject=email_subject,
                                         creator=user)
        if not send_to:
            return HttpResponse(json.dumps({'success':'false','message':'Please choose Recipients'}))


        elif event:
            event = Event.objects.filter(id=event)
            if event:
                message.event = event[0]

                user_list = [ att.user for att in Attendee.objects.filter(event=event[0].id)]

        elif activity:
            activity = Activity.objects.filter(id=activity)
            if activity :
                message.activity = activity[0]
                user_list = [ att.user for att in ActivityAttendee.objects.filter(activity=activity[0].id)]

        elif users:
            user_list  = User.objects.filter(id__in= request.POST.getlist('users'))
            if user_list :
                for user in user_list:
                    message.users.add(user)
        else:
            user_list  = User.objects.all()
            if user_list :
                for user in user_list:
                    message.users.add(user)


        if email_txt:
            message.use_email = True
            for user in user_list:
                msg = EmailMultiAlternatives(
                subject=email_subject,
                body="This is the text email body",
                from_email="Support Matchmaking Software <support@matchmakingsoftware.com.com>",
                to=[user.email],
                headers={'Reply-To': "Support Matchmaking Software <support@matchmakingsoftware.com.com>"}
                # optional extra headers
                )
                msg.attach_alternative(email_txt, "text/html")

                # Optional Mandrill-specific extensions:
                msg.tags = ["matchmaking", "event software"]
                msg.metadata = {'user_id': user.id}

                # Send it:
                msg.send()
        if voice_txt:
            for user in user_list:
                try:
                    TWILIO_CLIENT.calls.create(user.phone, from_=settings.TWILIO_NUMBER,
                        url=settings.SITE_URL + 'message/call/' + str(message.id))
                except:
                    pass # TODO:: Log error

            message.use_voice = True
        if sms_txt:
            for user in user_list:
                try:
                    TWILIO_CLIENT.sms.messages.create(
                    body=sms_txt, to=user.phone, from_=settings.TWILIO_NUMBER)
                except:
                    pass # TODO:: Log error
            message.use_sms = True
        message.save()




        return HttpResponse(json.dumps({'success':'true'}))
    else:
        users = User.objects.all()
        events = Event.objects.all()
        activity = Activity.objects.all()
        form = MessageForm()

        return render_to_response('email_compose.html',locals(),RequestContext(request))

def email_list(request):
    emails = Message.objects.all()

    return render_to_response('email_list.html',locals(),RequestContext(request))


def email_open(request,m_id):

    email = Message.objects.get(id=m_id)

    return render_to_response('email-opened.html',locals(),RequestContext(request))

@csrf_exempt
def message_call(request, message_id):
    """This is the callback for Twilio API to get the voice call contents."""
    message = Message.objects.get(id = message_id)
    response = ("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say voice=\"woman\" language=\"en\">" +
                escape(message.voice_txt) + "</Say></Response>")
    return HttpResponse(response, content_type='text/plain')
