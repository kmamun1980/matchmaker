from django.db import models
from apps.users.models import User
from apps.events.models import Event,Activity
from redactor.fields import RedactorField

class MessageBase(models.Model):
    """
    Base class for SMS, e-mail, and other messages.
    """
    campaign_name = models.CharField(max_length=255, blank=True,null=False)
    campaign_description = models.TextField(blank=True,null=False)
    event = models.ForeignKey(Event,blank=True,null=True)
    activity = models.ForeignKey(Activity,blank=True,null=True)
    users = models.ManyToManyField(User,blank=True,null=True,related_name='%(class)s_users')
    send_at = models.DateTimeField(blank=True,null=True)
    creator = models.ForeignKey(User,blank=True,null=True,related_name='%(class)s_message_creator')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    sent = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def __unicode__(self):
        if self.event:
            return str(self.event.name)
        elif self.activity:
            return str(self.activity.name)
        elif self.users.all():
             return self.users.all()[0].username + ' ..... '
        else:
            return str(self.created_at)

    @property
    def get_recipients(self):
        if self.event:
            return str(self.event.name)
        elif self.activity:
            return str(self.activity.name)
        elif self.users.all():
             return self.users.all()[0].username + ' ..... '
        else:
            return 'No recipients'

    @property
    def get_type(self):
        if self.event:
            return 'Event'
        elif self.activity:
            return 'Activity'
        elif self.users.all():
             return 'Users'
        else:
            return 'All Users'

class MessageSMS(MessageBase):
    """
    SMS message.
    """
    sms_txt = models.CharField(max_length=160, blank=True, null=False)


class EmailTemplate(models.Model):
    """
     Model to store email campaign template data
    """
    name = models.CharField(max_length=50,blank=True,null=True)
    template = models.FileField(upload_to='campaigns/email/templates')
    description = models.CharField(max_length=255,blank=True,null=True)
    user = models.ForeignKey(User)
    date_created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name



class Message(models.Model):
    email_template = models.ForeignKey(EmailTemplate,blank=True,null=True)
    voice_audio = models.FileField(upload_to='campaigns/voice',blank=True,null=True)
    voice_txt = models.TextField(blank=True,null=True)
    email_txt = RedactorField(blank=True,null=True)
    sms_txt = models.TextField(blank=True,null=True)
    email_subject = models.TextField(blank=True,null=True)
    attachment = models.FileField(upload_to='campaigns/attachments',blank=True,null=True)
    attachment2 = models.FileField(upload_to='campaigns/attachments',blank=True,null=True)
    attachment3 = models.FileField(upload_to='campaigns/attachments',blank=True,null=True)
    attachment4 = models.FileField(upload_to='campaigns/attachments',blank=True,null=True)
    attachment5 = models.FileField(upload_to='campaigns/attachments',blank=True,null=True)
    use_email = models.BooleanField(default=False)
    use_voice = models.BooleanField(default=False)
    use_sms = models.BooleanField(default=False)
    event = models.ForeignKey(Event,blank=True,null=True)
    activity = models.ForeignKey(Activity,blank=True,null=True)
    users = models.ManyToManyField(User,blank=True,null=True,related_name='users')
    send_at = models.DateTimeField(blank=True,null=True)
    creator = models.ForeignKey(User,blank=True,null=True,related_name='message_creator')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    sent = models.BooleanField(default=False)
    @property
    def attachments(self):
        if self.attachment:
            return self.attachment.url



    def __unicode__(self):
        if self.event:
            return str(self.event.name)
        elif self.activity:
            return str(self.activity.name)
        elif self.users.all():
             return self.users.all()[0].username + ' ..... '
        else:
            return str(self.created_at)
    @property
    def get_recipients(self):
        if self.event:
            return str(self.event.name)
        elif self.activity:
            return str(self.activity.name)
        elif self.users.all():
             return self.users.all()[0].username + ' ..... '
        else:
            return 'No recipients'

    @property
    def get_type(self):
        if self.event:
            return 'Event'
        elif self.activity:
            return 'Activity'
        elif self.users.all():
             return 'Users'
        else:
            return 'All Users'








