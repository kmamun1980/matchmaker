import json
import logging
import itertools
import dateutil.parser
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.views.generic import CreateView, UpdateView, View
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from cities_light.models import Region,City,Country
from django.views import generic as cbv
from django.forms.models import inlineformset_factory

from models import (GVGPOrgProfile,Industry,Department,Office,OrgCategory,
                    Organisation,Agency,NSCode, CompanyPerformance, CompanyDocument,BusinessCert,
                    PSCCode)
from . import forms
from apps.users.models import User

from braces.views import LoginRequiredMixin
from braces.views import AjaxResponseMixin
from braces.views import JSONResponseMixin
from crispy_forms.utils import render_crispy_form

logger = logging.getLogger('apps.company')

from apps.users.models import User
from apps.users.decorators import profile_complete_required

from models import (
    CVGPOrgProfile, GVGPOrgProfile, Industry, Department, Office, Agency,
    NSCode)
from forms import ( CVGPOrgProfileBasicForm, CVGPOrgProfileCodesForm,
    GVGPOrgProfileForm, IndustryProfileForm, CVGPOrgProfileOverviewForm,
    GVGPOrgProfileRegistrationForm, GVGPOrgProfileForm2, CVGPOrgProfile2Form,CVGPOrgProfileVOSB,
    CompanyPartnerInfoForm, SearchProductForm)

@login_required
def create_cvgp_org_profile(request):
    if request.method == 'POST':
        form = CVGPOrgProfileBasicForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = CVGPOrgProfileBasicForm()

    return render_to_response(
        'company/create_cvgp_org_profile.html',
        locals(), RequestContext(request))


@profile_complete_required
def edit_cvgp_org_profile(request, id):
    profile = None
    try:
        profile = CVGPOrgProfile.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = CVGPOrgProfileBasicForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = CVGPOrgProfileBasicForm(instance=profile)

    return render_to_response(
        'company/edit_cvgp_org_profile.html', locals(), RequestContext(request))


class CreateGovOrg(CreateView):
    form_class = GVGPOrgProfileRegistrationForm
    template_name = 'company/add_gov_org.html'
    context_object_name = 'gov_department'

    def get_initial(self):
        if self.request.session.get('register_type', False):
            type_dict = self.request.session['register_type']
            try:
                return {
                    'department': Department.objects.get(id=type_dict['department']),
                    'agency': Agency.objects.get(id=type_dict['agency']),
                    'industry': Industry.objects.get(id=type_dict['industry']),
                    'office': Office.objects.get(id=type_dict['office'])
                }
            except Exception:
                pass
        return {}

    def form_valid(self, form):
        self.object = form.save()
        self.request.user.gov_department = self.object
        self.request.user.save()
        return super(CreateGovOrg, self).form_valid(form)

    def get_success_url(self):
        return reverse('edit_personal_profile')


class EditGovOrg(UpdateView):
    form_class = GVGPOrgProfileForm
    template_name = 'company/add_gov_org.html'
    context_object_name = 'gov'

    def get_object(self):
        return GVGPOrgProfile.objects.get(id=self.request.user.gov_department.id)

    def get_success_url(self):
        messages.success(self.request, 'Government organisation profile details updated')
        return reverse('edit_gov_org')

    def get_context_data(self, **kwargs):
        context = super(EditGovOrg, self).get_context_data(**kwargs)
        context.update({'is_edit': True})
        return context


class BaseCompanyCompanyView(View):
    model = CVGPOrgProfile
    form_class = CVGPOrgProfileBasicForm
    codes_form = CVGPOrgProfileCodesForm
    overview_form = CVGPOrgProfileOverviewForm
    partner_info_form_class = CompanyPartnerInfoForm
    template_name = 'company/add_company.html'
            
    def get_document_formset_kwargs(self):
        return {'instance': self.object, 'prefix': 'document'}
 
    def get_document_formset_class(self):
        return inlineformset_factory(self.model, CompanyDocument, extra=1)

    def get_document_formset_helper(self):
        return forms.CompanyDocumentFormsetHelper()

    def get_performance_formset_kwargs(self):
        return {'instance': self.object, 'prefix': 'performance'}
    
    def get_performance_formset_class(self):
        return inlineformset_factory(self.model, CompanyPerformance, extra=1)

    def get_performance_formset_helper(self):
        return forms.CompanyPerformanceFormsetHelper()

    def get_company_partner_info_form_kwargs(self):
        return {'instance': self.object, 'prefix': 'partner'}

    def get_company_partner_info_form_class(self):
        return self.partner_info_form_class


class CreateCompany(BaseCompanyCompanyView, CreateView):
    #context_object_name = 'com_profile'
    
    def get_context_data(self, *args, **kwargs):
        context = super(CreateCompany, self).get_context_data(**kwargs)
        context['current_tab'] = 'base'
        context['codes_form'] = self.codes_form(instance=self.object)
        context['overview_form'] = self.overview_form(instance=self.object)
        context['document_formset'] = self.get_document_formset_class()(
            **self.get_document_formset_kwargs()
        )
        context['document_formset_helper'] = self.get_document_formset_helper()
        context['performance_formset'] = self.get_performance_formset_class()(
            **self.get_performance_formset_kwargs()
        )
        context['performance_formset_helper'] = self.get_performance_formset_helper()
        context['partner_info_form'] = self.get_company_partner_info_form_class()(
            **self.get_company_partner_info_form_kwargs()
        )
        
        return context

    def form_valid(self, form):
        self.object = form.save()
        self.request.user.commmercial_org = self.object
        self.request.user.save()
        return super(CreateCompany, self).form_valid(form)

    def get_success_url(self):
        return "%s?action=codes" % reverse('edit_company')


class EditCompany(BaseCompanyCompanyView, UpdateView):
    action = 'base'
    
    def get_object(self, queryset=None):
        try: 
            queryset = self.get_queryset()
            return queryset.get(id=self.request.user.commmercial_org.id)
        except:
            raise Http404
    
    def get_context_data(self, *args, **kwargs):
        context = super(EditCompany, self).get_context_data(**kwargs)
        context['profile'] = instance=self.object
        context['current_tab'] = self.action
        context['codes_form'] = self.codes_form(instance=self.object)
        context['overview_form'] = self.overview_form(instance=self.object)
        context['document_formset'] = self.get_document_formset_class()(
            **self.get_document_formset_kwargs()
        )
        context['document_formset_helper'] = self.get_document_formset_helper()
        context['performance_formset'] = self.get_performance_formset_class()(
            **self.get_performance_formset_kwargs()
        )
        context['performance_formset_helper'] = self.get_performance_formset_helper()
        context['partner_info_form'] = self.get_company_partner_info_form_class()(
            **self.get_company_partner_info_form_kwargs()
        )
        current_form = kwargs.get('current_form', None)
        if current_form:
            if self.action == 'codes':
                context['codes_form'] = current_form
            elif self.action == 'overview':
                context['overview_form'] = current_form
            elif self.action == 'performance':
                context['document_formset'] = self.document_formset
                context['performance_formset'] = self.performance_formset
                context['partner_info_form'] = current_form
        return context
    
    def get(self, request, *args, **kwargs):
        self.action = request.GET.get('action', 'base')
        return super(EditCompany, self).get(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(instance=self.object)
        codes_form = self.codes_form(request.POST, instance=self.object)
        overview_form = self.overview_form(request.POST, instance=self.object)
        self.action = request.POST.get('action', 'base')
        
        if self.action == 'codes':
            return self.validate_form(form, codes_form, 'overview')
        if self.action == 'overview':
            return self.validate_form(form, overview_form, 'performance')
        if self.action == 'performance':
            self.document_formset = self.get_document_formset_class()(
                self.request.POST,
                self.request.FILES,
                **self.get_document_formset_kwargs()
            )
            self.performance_formset = self.get_performance_formset_class()(
                self.request.POST,
                self.request.FILES,
                **self.get_performance_formset_kwargs()
            )    
            self.partner_info_form = self.get_company_partner_info_form_class()(
                self.request.POST,
                self.request.FILES,
                **self.get_company_partner_info_form_kwargs()
            )
    
            if (
                self.document_formset.is_valid() and
                self.performance_formset.is_valid() and
                self.partner_info_form.is_valid()
            ):
                self.document_formset.save()
                self.performance_formset.save()
                self.partner_info_form.save()
                
                return HttpResponseRedirect(reverse('edit_personal_profile'))
            else:
                return self.render_to_response(self.get_context_data(
                    form=form, current_form=self.partner_info_form))
        
        form = self.form_class(request.POST, request.FILES, instance=self.object)
        return self.validate_form(form, form, 'codes')
        
    def validate_form(self, form, current_form, action):
        if current_form.is_valid():
            self.object = current_form.save()
            return HttpResponseRedirect("%s?action=%s" % (reverse('edit_company'), action))
        else:
            return self.render_to_response(
                self.get_context_data(form=form, current_form=current_form))


@profile_complete_required
def GovOrg(request, id):

    users = User.objects.filter(gov_department = id)
    total_users = users.count()
    events = 100
    contracts = 200
    awarded = 300
    org = GVGPOrgProfile.objects.get(id=id)

    return render_to_response('company/org_profile.html',locals() ,RequestContext(request))


@profile_complete_required
def CompanyProfile(request, id):

    users = User.objects.filter(commmercial_org=id)
    total_users = users.count()
    events = 100
    contracts = 200
    awarded = 300
    org = CVGPOrgProfile.objects.get(id=id)

    return render_to_response(
        'company/company_profile.html', locals() ,RequestContext(request))


@profile_complete_required
def AddNaics(request):
    if request.method == 'POST':
        code = request.POST.get('code',None)
        if code:
            code = NSCode.objects.filter(id=code)
            if code:
                if request.user.vgp_type == '1':
                    company = GVGPOrgProfile.objects.get(id=request.user.gov_department)
                    company.naics.add(code[0])
                    company.save()
                else:
                    company = CVGPOrgProfile.objects.get(id=request.user.gov_department)
                    company.naics.add(code[0])
                    company.save()

                    return HttpResponse(json.dumps({'success':'true'}))
    else:
        return render_to_response()


@profile_complete_required
def edit_gvgp_org_profile(request, id):
    profile = None
    try:
       profile = GVGPOrgProfile.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = GVGPOrgProfileForm2(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = GVGPOrgProfileForm2(instance=profile)

    return render_to_response(
        'company/edit_gvgp_org_profile.html', locals(), RequestContext(request))


@profile_complete_required
def create_industry_profile(request):
    if request.method == 'POST':
        form = IndustryProfileForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = IndustryProfileForm()

    return render_to_response(
        'company/create_industry_profile.html', {"form": form},
        RequestContext(request))


@profile_complete_required
def edit_industry_profile(request, id):
    profile = None
    try:
       profile = Industry.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = IndustryProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = IndustryProfileForm(instance=profile)

    return render_to_response(
        'company/edit_industry_profile.html', locals(),
        RequestContext(request))


class SearchCodesView(LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin,
                      cbv.View):
    def get_ajax(self, request, *args, **kwargs):
        form = SearchProductForm(self.request.GET)
        if not form.is_valid():
            return self.render_json_response({
                'status': 'error',
                'form_html': render_crispy_form(form)
            })

        search_str = form.cleaned_data['search_str']
        psc_codes = PSCCode.objects.filter(
            Q(
                code__icontains=search_str
            ) | Q(
                about__icontains=search_str
            )
        )
        ns_codes = NSCode.objects.filter(
            Q(
                code__icontains=search_str
            ) | Q(
                about__icontains=search_str
            )
        )
        return self.render_json_response({
            'status': 'ok',
            'codes': list(itertools.chain(
                [{
                    'pk': x.pk,
                    'code': x.code,
                    'about': x.about,
                    'code_type': 'ps'
                } for x in psc_codes],
                [{
                    'pk': x.pk,
                    'code': x.code,
                    'about': x.about,
                    'code_type': 'ns'
                } for x in ns_codes]
            ))
        })


class GetNaicsByPksView(JSONResponseMixin, cbv.View):
    def get(self, request, *args, **kwargs):
        pk_list = self.request.GET.getlist('pks')
        logger.debug(pk_list)
        return self.render_json_response({
            'status': 'ok',
            'codes': [{
                'pk': x.pk,
                'code': x.code,
                'about': x.about,
                'code_type': 'ns'
            } for x in NSCode.objects.filter(pk__in=pk_list)]
        })


class GetPscByPksView(JSONResponseMixin, cbv.View):
    def get(self, request, *args, **kwargs):
        pk_list = self.request.GET.getlist('pks')
        logger.debug(pk_list)
        return self.render_json_response({
            'status': 'ok',
            'codes': [{
                'pk': x.pk,
                'code': x.code,
                'about': x.about,
                'code_type': 'ps'
            } for x in PSCCode.objects.filter(pk__in=pk_list)]
        })


def add_vosb(request):

    if request.method == 'POST':

        name = request.POST.get('name', None)
        website = request.POST.get('website', None)
        youtube = request.POST.get('youtube', None)
        country = request.POST.get('country', None)
        city = request.POST.get('city', None)
        region = request.POST.get('region', None)
        zipcode = request.POST.get('zip', None)
        phone = request.POST.get('phone',None)
        address = request.POST.get('address', None)
        logo = request.FILES.get('logo', None)

        if name and website and phone and zipcode and youtube and address:

            company = CVGPOrgProfile.objects.filter(name=name).exists()

            if not company:

                c = CVGPOrgProfile.objects.create(name=name, logo=logo, address=address, website=website,
                                          phone=phone, zip=zipcode, country_id=country, region_id=region,
                                          city_id=city, youtube_video=youtube)
                user = request.user
                user.commmercial_org_id = company.id
                user.save()
            else:
                company = CVGPOrgProfile.objects.get(name=name)
                company.logo = logo
                company.zip = zipcode
                company.phone = phone
                company.country_id = country
                company.city_id = city
                company.region_id = region
                company.address = address
                company.youtube_video = youtube
                company.website = website
                company.save()




            return HttpResponse(json.dumps({'success':'true'}))
        else:
            return HttpResponse(json.dumps({'success':'false'}))




    form = CVGPOrgProfileBasicForm()
    company = CVGPOrgProfile.objects.filter(id=request.user.commmercial_org_id)
    codes = NSCode.objects.all()
    if company:
        company = company[0]
        form  = CVGPOrgProfileVOSB(instance=company)
        my_codes = company.naics.all()
    documents = CompanyDocument.objects.filter(company=request.user.commmercial_org)

    certs = BusinessCert.objects.filter(user=request.user.commmercial_org)


    return render_to_response('company/add_vosb.html',locals(),RequestContext(request))


def add_doc(request):

    if request.method == 'POST':

        doc = request.FILES.get('doc', None)
        visibility =  request.POST.get('doc_visibility', None)
        if doc:

            company = request.user.commmercial_org
            c = CompanyDocument.objects.create(company=company, document=doc,visibility=visibility)
            html = "<li><a href='%s'>%s</a></li>" \
                   % (c.document.url, c.filename())
            return HttpResponse(json.dumps({'success': 'true' ,'message': html}))

        else:
            return HttpResponse(json.dumps({'success':'false'}))



    return render_to_response('company/add_vosb.html',locals(),RequestContext(request))

def add_vosb_code(request):

    if request.method == 'POST':

        code = request.POST.get('code', None)

        if code:

            company = request.user.commmercial_org
            code = NSCode.objects.get(id=code)
            company.naics.add(code)
            company.save()
            html = "<li>%s - %s</li>" % (code.code, code.about)

            return HttpResponse(json.dumps({'success': 'true', 'message': html}))

        else:
            return HttpResponse(json.dumps({'success': 'false'}))



    return render_to_response('company/add_vosb.html',locals(),RequestContext(request))

def add_vosb_cert(request):

    if request.method == 'POST':

        name = request.POST.get('cert_name', None)
        cert_date = request.POST.get('cert_date', None)

        if name:


            company = request.user.commmercial_org
            cert = BusinessCert.objects.create( certification_name=name,date_obtained=dateutil.parser.parse(cert_date),
                                               user=request.user.commmercial_org )

            html = "<li>%s - %s</li>" % (cert.certification_name, cert.date_obtained)

            return HttpResponse(json.dumps({'success': 'true', 'message': html}))

        else:
            return HttpResponse(json.dumps({'success': 'false'}))



    return render_to_response('company/add_vosb.html',locals(),RequestContext(request))

def add_overview(request):

    if request.method == 'POST':

        corporate_overview = request.POST.get('corporate_overview', None)
        states_you_do_business = request.POST.get('states_you_do_business', None)
        states_you_wish_to_do = request.POST.get('states_you_wish_to_do', None)
        total_employees = request.POST.get('total_employees', None)
        vet_employees = request.POST.get('vet_employees', None)
        relevant_links = request.POST.get('relevant_links', None)
        small_bus_cat = request.POST.get('small_bus_cat', None)
        poc = request.POST.get('poc', None)
        date_established = request.POST.get('date_established', None)
        cve_verified_on = request.POST.get('cve_verified_on', None)
        avg_rev = request.POST.get('avg_rev', None)
        small_business = request.POST.get('small_business', None)
        business_partner_ship = request.POST.get('business_partner_ship', None)
        mentor = request.POST.get('mentor', None)
        agency_program =  request.POST.get('agency_program', None)

        company = CVGPOrgProfile.objects.get(id=request.user.commmercial_org.id)
        company.avg_rev = avg_rev
        company.corporate_overview = corporate_overview
        company.business_partner_ship = business_partner_ship
        company.small_business_category_id = small_bus_cat
        company.primary_poc = poc
        company.relevant_links = relevant_links
        company.vet_employees = vet_employees
        company.total_employees = total_employees
        company.agency_program = agency_program
        if date_established and cve_verified_on and vet_employees and total_employees :
            company.date_established = dateutil.parser.parse(date_established)
            company.cve_verified_on = dateutil.parser.parse(cve_verified_on)

            company.vet_employees = vet_employees
            company.total_employees = total_employees
            company.save()





            return HttpResponse(json.dumps({'success': 'true', 'message':'Company Overview Saved Successfully'}))

        else:
            return HttpResponse(json.dumps({'success': 'false','message':'Some details are missing'}))



    return render_to_response('company/add_vosb.html',locals(),RequestContext(request))


def add_partner_need(request):

    if request.method == 'POST':

        name = request.POST.get('cert_name', None)
        cert_date = request.POST.get('cert_date', None)

        if name:


            company = request.user.commmercial_org
            cert = BusinessCert.objects.create( certification_name=name,date_obtained=dateutil.parser.parse(cert_date),
                                               user=request.user.commmercial_org )

            html = "<li>%s - %s</li>" % (cert.certification_name, cert.date_obtained)

            return HttpResponse(json.dumps({'success': 'true', 'message': html}))

        else:
            return HttpResponse(json.dumps({'success': 'false'}))



    return render_to_response('company/add_vosb.html',locals(),RequestContext(request))
