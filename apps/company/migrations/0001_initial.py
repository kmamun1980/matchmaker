# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'OrgCategory'
        db.create_table(u'company_orgcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['OrgCategory'])

        # Adding model 'Department'
        db.create_table(u'company_department', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('org_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.OrgCategory'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['Department'])

        # Adding model 'Organisation'
        db.create_table(u'company_organisation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['Organisation'])

        # Adding model 'Agency'
        db.create_table(u'company_agency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.Department'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['Agency'])

        # Adding model 'Office'
        db.create_table(u'company_office', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('agency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.Agency'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['Office'])

        # Adding model 'Industry'
        db.create_table(u'company_industry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('date_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['Industry'])

        # Adding model 'PSCCode'
        db.create_table(u'company_psccode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('about', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('nature', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('date_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['PSCCode'])

        # Adding model 'NSCode'
        db.create_table(u'company_nscode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('about', self.gf('django.db.models.fields.TextField')(default=u'no name', null=True, blank=True)),
            ('nature', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('date_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['NSCode'])

        # Adding model 'SmallBusinessCategory'
        db.create_table(u'company_smallbusinesscategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('industry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.Industry'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'company', ['SmallBusinessCategory'])

        # Adding model 'DCAACompliantAccount'
        db.create_table(u'company_dcaacompliantaccount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'company', ['DCAACompliantAccount'])

        # Adding model 'SecurityClearanceLevel'
        db.create_table(u'company_securityclearancelevel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'company', ['SecurityClearanceLevel'])

        # Adding model 'CVGPOrgProfile'
        db.create_table(u'company_cvgporgprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('logo', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('sec_clearance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.SecurityClearanceLevel'], null=True, blank=True)),
            ('dcaa_compliant_acc', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.DCAACompliantAccount'], null=True, blank=True)),
            ('cage_code', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('duns_no', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('avg_rev', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('cbla', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('cbla_per_contract', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('total_employees', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('vet_employees', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('industry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.Industry'], null=True, blank=True)),
            ('cve_verified_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('date_established', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('primary_poc', self.gf('django.db.models.fields.CharField')(max_length=55, null=True, blank=True)),
            ('phone', self.gf('phonenumber_field.modelfields.PhoneNumberField')(max_length=128, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=55, null=True, blank=True)),
            ('bonding_level', self.gf('django.db.models.fields.CharField')(max_length=55, null=True, blank=True)),
            ('bonding_number_aggregate', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('bonding_number_contract', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('zip', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('corporate_overview', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('relevant_links', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('youtube_video', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('location', self.gf('geoposition.fields.GeopositionField')(default='0,0', max_length=42, null=True, blank=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cities_light.Country'], null=True, blank=True)),
            ('region', self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['cities_light.Region'], null=True, blank=True)),
            ('city', self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['cities_light.City'], null=True, blank=True)),
            ('business_partner_ship', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('business_relation_ship', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('organization_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('agency_program', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('overview_visiblity', self.gf('django.db.models.fields.CharField')(default=u'Pub', max_length=4, null=True, blank=True)),
            ('average_visiblity', self.gf('django.db.models.fields.CharField')(default=u'Pub', max_length=4, null=True, blank=True)),
            ('documents_visiblity', self.gf('django.db.models.fields.CharField')(default=u'Pub', max_length=4, null=True, blank=True)),
            ('performance_visiblity', self.gf('django.db.models.fields.CharField')(default=u'Pub', max_length=4, null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['CVGPOrgProfile'])

        # Adding M2M table for field naics on 'CVGPOrgProfile'
        m2m_table_name = db.shorten_name(u'company_cvgporgprofile_naics')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cvgporgprofile', models.ForeignKey(orm[u'company.cvgporgprofile'], null=False)),
            ('nscode', models.ForeignKey(orm[u'company.nscode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cvgporgprofile_id', 'nscode_id'])

        # Adding M2M table for field psc_fsc on 'CVGPOrgProfile'
        m2m_table_name = db.shorten_name(u'company_cvgporgprofile_psc_fsc')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cvgporgprofile', models.ForeignKey(orm[u'company.cvgporgprofile'], null=False)),
            ('psccode', models.ForeignKey(orm[u'company.psccode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cvgporgprofile_id', 'psccode_id'])

        # Adding M2M table for field small_business_category on 'CVGPOrgProfile'
        m2m_table_name = db.shorten_name(u'company_cvgporgprofile_small_business_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cvgporgprofile', models.ForeignKey(orm[u'company.cvgporgprofile'], null=False)),
            ('smallbusinesscategory', models.ForeignKey(orm[u'company.smallbusinesscategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cvgporgprofile_id', 'smallbusinesscategory_id'])

        # Adding M2M table for field states_you_do_business on 'CVGPOrgProfile'
        m2m_table_name = db.shorten_name(u'company_cvgporgprofile_states_you_do_business')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cvgporgprofile', models.ForeignKey(orm[u'company.cvgporgprofile'], null=False)),
            ('region', models.ForeignKey(orm[u'cities_light.region'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cvgporgprofile_id', 'region_id'])

        # Adding M2M table for field states_you_wish_to_do on 'CVGPOrgProfile'
        m2m_table_name = db.shorten_name(u'company_cvgporgprofile_states_you_wish_to_do')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cvgporgprofile', models.ForeignKey(orm[u'company.cvgporgprofile'], null=False)),
            ('region', models.ForeignKey(orm[u'cities_light.region'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cvgporgprofile_id', 'region_id'])

        # Adding model 'GVGPOrgProfile'
        db.create_table(u'company_gvgporgprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('gov_office', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('org_category', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'org_category', null=True, to=orm['company.OrgCategory'])),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'org_department', null=True, to=orm['company.Department'])),
            ('agency', self.gf('smart_selects.db_fields.ChainedForeignKey')(blank=True, related_name=u'org_agency', null=True, to=orm['company.Agency'])),
            ('office', self.gf('smart_selects.db_fields.ChainedForeignKey')(blank=True, related_name=u'org_agency', null=True, to=orm['company.Office'])),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'org_visn', null=True, to=orm['company.Organisation'])),
            ('industry', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'industry', null=True, to=orm['company.Industry'])),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=255)),
            ('youtube_video', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('program_names', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('major_initiatives', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('mission', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('key_objectives', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('relevant_links', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('greatest_challenges', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('contractor_needs', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('c_needs_for', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['GVGPOrgProfile'])

        # Adding M2M table for field naics on 'GVGPOrgProfile'
        m2m_table_name = db.shorten_name(u'company_gvgporgprofile_naics')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('gvgporgprofile', models.ForeignKey(orm[u'company.gvgporgprofile'], null=False)),
            ('nscode', models.ForeignKey(orm[u'company.nscode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['gvgporgprofile_id', 'nscode_id'])

        # Adding M2M table for field psc_fsc on 'GVGPOrgProfile'
        m2m_table_name = db.shorten_name(u'company_gvgporgprofile_psc_fsc')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('gvgporgprofile', models.ForeignKey(orm[u'company.gvgporgprofile'], null=False)),
            ('psccode', models.ForeignKey(orm[u'company.psccode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['gvgporgprofile_id', 'psccode_id'])

        # Adding model 'BusinessCert'
        db.create_table(u'company_businesscert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('certification_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('date_obtained', self.gf('django.db.models.fields.DateField')()),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.CVGPOrgProfile'])),
            ('document', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['BusinessCert'])

        # Adding model 'CoreStrength'
        db.create_table(u'company_corestrength', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_core', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('capability', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('category', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('description', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.CVGPOrgProfile'])),
        ))
        db.send_create_signal(u'company', ['CoreStrength'])

        # Adding M2M table for field naics on 'CoreStrength'
        m2m_table_name = db.shorten_name(u'company_corestrength_naics')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('corestrength', models.ForeignKey(orm[u'company.corestrength'], null=False)),
            ('nscode', models.ForeignKey(orm[u'company.nscode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['corestrength_id', 'nscode_id'])

        # Adding model 'CPartneringNeed'
        db.create_table(u'company_cpartneringneed', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vgp_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.CVGPOrgProfile'])),
            ('visibility', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('description', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('market_segment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('client', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('poc', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('incubent_contractor', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('estimated_value', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('procurement_method', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('current_exp_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('estimated_start_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('security_requirement', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('documents', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('small_business_types', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('place_of_work', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('geographic_constraints', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('capabilities_constraints', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('relevant_links', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['CPartneringNeed'])

        # Adding model 'CompanyDocument'
        db.create_table(u'company_companydocument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'document_set', to=orm['company.CVGPOrgProfile'])),
            ('uploaded', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('visibility', self.gf('django.db.models.fields.CharField')(default=u'Pub', max_length=4, null=True, blank=True)),
            ('document', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'company', ['CompanyDocument'])

        # Adding model 'CompanyPerformance'
        db.create_table(u'company_companyperformance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'performance_set', to=orm['company.CVGPOrgProfile'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('primary_company', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('client_org', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('contract', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('contract_value', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('start_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('scope_desc', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'company', ['CompanyPerformance'])


    def backwards(self, orm):
        # Deleting model 'OrgCategory'
        db.delete_table(u'company_orgcategory')

        # Deleting model 'Department'
        db.delete_table(u'company_department')

        # Deleting model 'Organisation'
        db.delete_table(u'company_organisation')

        # Deleting model 'Agency'
        db.delete_table(u'company_agency')

        # Deleting model 'Office'
        db.delete_table(u'company_office')

        # Deleting model 'Industry'
        db.delete_table(u'company_industry')

        # Deleting model 'PSCCode'
        db.delete_table(u'company_psccode')

        # Deleting model 'NSCode'
        db.delete_table(u'company_nscode')

        # Deleting model 'SmallBusinessCategory'
        db.delete_table(u'company_smallbusinesscategory')

        # Deleting model 'DCAACompliantAccount'
        db.delete_table(u'company_dcaacompliantaccount')

        # Deleting model 'SecurityClearanceLevel'
        db.delete_table(u'company_securityclearancelevel')

        # Deleting model 'CVGPOrgProfile'
        db.delete_table(u'company_cvgporgprofile')

        # Removing M2M table for field naics on 'CVGPOrgProfile'
        db.delete_table(db.shorten_name(u'company_cvgporgprofile_naics'))

        # Removing M2M table for field psc_fsc on 'CVGPOrgProfile'
        db.delete_table(db.shorten_name(u'company_cvgporgprofile_psc_fsc'))

        # Removing M2M table for field small_business_category on 'CVGPOrgProfile'
        db.delete_table(db.shorten_name(u'company_cvgporgprofile_small_business_category'))

        # Removing M2M table for field states_you_do_business on 'CVGPOrgProfile'
        db.delete_table(db.shorten_name(u'company_cvgporgprofile_states_you_do_business'))

        # Removing M2M table for field states_you_wish_to_do on 'CVGPOrgProfile'
        db.delete_table(db.shorten_name(u'company_cvgporgprofile_states_you_wish_to_do'))

        # Deleting model 'GVGPOrgProfile'
        db.delete_table(u'company_gvgporgprofile')

        # Removing M2M table for field naics on 'GVGPOrgProfile'
        db.delete_table(db.shorten_name(u'company_gvgporgprofile_naics'))

        # Removing M2M table for field psc_fsc on 'GVGPOrgProfile'
        db.delete_table(db.shorten_name(u'company_gvgporgprofile_psc_fsc'))

        # Deleting model 'BusinessCert'
        db.delete_table(u'company_businesscert')

        # Deleting model 'CoreStrength'
        db.delete_table(u'company_corestrength')

        # Removing M2M table for field naics on 'CoreStrength'
        db.delete_table(db.shorten_name(u'company_corestrength_naics'))

        # Deleting model 'CPartneringNeed'
        db.delete_table(u'company_cpartneringneed')

        # Deleting model 'CompanyDocument'
        db.delete_table(u'company_companydocument')

        # Deleting model 'CompanyPerformance'
        db.delete_table(u'company_companyperformance')


    models = {
        u'cities_light.city': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('region', 'name'), ('region', 'slug'))", 'object_name': 'City'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'feature_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'population': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'search_names': ('cities_light.models.ToSearchTextField', [], {'default': "''", 'max_length': '4000', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'cities_light.country': {
            'Meta': {'ordering': "['name']", 'object_name': 'Country'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'code3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'continent': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"}),
            'tld': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '5', 'blank': 'True'})
        },
        u'cities_light.region': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('country', 'name'), ('country', 'slug'))", 'object_name': 'Region'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'geoname_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'company.agency': {
            'Meta': {'object_name': 'Agency'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Department']", 'null': 'True', 'blank': 'True'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.businesscert': {
            'Meta': {'object_name': 'BusinessCert'},
            'certification_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_obtained': ('django.db.models.fields.DateField', [], {}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.CVGPOrgProfile']"})
        },
        u'company.companydocument': {
            'Meta': {'object_name': 'CompanyDocument'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'document_set'", 'to': u"orm['company.CVGPOrgProfile']"}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uploaded': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'visibility': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'})
        },
        u'company.companyperformance': {
            'Meta': {'object_name': 'CompanyPerformance'},
            'client_org': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'performance_set'", 'to': u"orm['company.CVGPOrgProfile']"}),
            'contract': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'contract_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'primary_company': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'scope_desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'company.corestrength': {
            'Meta': {'object_name': 'CoreStrength'},
            'capability': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_core': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'naics': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'core_strength_naics'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['company.NSCode']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.CVGPOrgProfile']"})
        },
        u'company.cpartneringneed': {
            'Meta': {'object_name': 'CPartneringNeed'},
            'capabilities_constraints': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'client': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'current_exp_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'documents': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'estimated_start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'estimated_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'geographic_constraints': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incubent_contractor': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'market_segment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'place_of_work': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'poc': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'procurement_method': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'relevant_links': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'security_requirement': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'small_business_types': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'vgp_user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.CVGPOrgProfile']"}),
            'visibility': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.cvgporgprofile': {
            'Meta': {'object_name': 'CVGPOrgProfile'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'agency_program': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'average_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'avg_rev': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'bonding_level': ('django.db.models.fields.CharField', [], {'max_length': '55', 'null': 'True', 'blank': 'True'}),
            'bonding_number_aggregate': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'bonding_number_contract': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'business_partner_ship': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'business_relation_ship': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cage_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cbla': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cbla_per_contract': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'city': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['cities_light.City']", 'null': 'True', 'blank': 'True'}),
            'corporate_overview': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']", 'null': 'True', 'blank': 'True'}),
            'cve_verified_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_established': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'dcaa_compliant_acc': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.DCAACompliantAccount']", 'null': 'True', 'blank': 'True'}),
            'documents_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'duns_no': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Industry']", 'null': 'True', 'blank': 'True'}),
            'location': ('geoposition.fields.GeopositionField', [], {'default': "'0,0'", 'max_length': '42', 'null': 'True', 'blank': 'True'}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'naics': ('select2.fields.ManyToManyField', [], {'related_name': "u'cvgporgprofiles'", 'to': u"orm['company.NSCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'organization_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'overview_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'performance_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'phone': ('phonenumber_field.modelfields.PhoneNumberField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'primary_poc': ('django.db.models.fields.CharField', [], {'max_length': '55', 'null': 'True', 'blank': 'True'}),
            'psc_fsc': ('select2.fields.ManyToManyField', [], {'related_name': "u'cvgporgprofiles'", 'to': u"orm['company.PSCCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'region': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'relevant_links': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'sec_clearance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.SecurityClearanceLevel']", 'null': 'True', 'blank': 'True'}),
            'small_business_category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['company.SmallBusinessCategory']", 'null': 'True', 'blank': 'True'}),
            'states_you_do_business': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'states_u_do'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['cities_light.Region']"}),
            'states_you_wish_to_do': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'states_u_wish'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['cities_light.Region']"}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '55', 'null': 'True', 'blank': 'True'}),
            'total_employees': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vet_employees': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'youtube_video': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.dcaacompliantaccount': {
            'Meta': {'object_name': 'DCAACompliantAccount'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'company.department': {
            'Meta': {'object_name': 'Department'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'org_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.OrgCategory']", 'null': 'True', 'blank': 'True'})
        },
        u'company.gvgporgprofile': {
            'Meta': {'object_name': 'GVGPOrgProfile'},
            'agency': ('smart_selects.db_fields.ChainedForeignKey', [], {'blank': 'True', 'related_name': "u'org_agency'", 'null': 'True', 'to': u"orm['company.Agency']"}),
            'c_needs_for': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'contractor_needs': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'org_department'", 'null': 'True', 'to': u"orm['company.Department']"}),
            'gov_office': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'greatest_challenges': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'industry'", 'null': 'True', 'to': u"orm['company.Industry']"}),
            'key_objectives': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'major_initiatives': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'mission': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'naics': ('select2.fields.ManyToManyField', [], {'related_name': "u'gvgporgprofiles'", 'to': u"orm['company.NSCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'office': ('smart_selects.db_fields.ChainedForeignKey', [], {'blank': 'True', 'related_name': "u'org_agency'", 'null': 'True', 'to': u"orm['company.Office']"}),
            'org_category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'org_category'", 'null': 'True', 'to': u"orm['company.OrgCategory']"}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'org_visn'", 'null': 'True', 'to': u"orm['company.Organisation']"}),
            'program_names': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'psc_fsc': ('select2.fields.ManyToManyField', [], {'related_name': "u'gvgporgprofiles'", 'to': u"orm['company.PSCCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'relevant_links': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '255'}),
            'youtube_video': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'company.industry': {
            'Meta': {'object_name': 'Industry'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'company.nscode': {
            'Meta': {'object_name': 'NSCode'},
            'about': ('django.db.models.fields.TextField', [], {'default': "u'no name'", 'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nature': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'company.office': {
            'Meta': {'object_name': 'Office'},
            'agency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Agency']", 'null': 'True', 'blank': 'True'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.organisation': {
            'Meta': {'object_name': 'Organisation'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.orgcategory': {
            'Meta': {'object_name': 'OrgCategory'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.psccode': {
            'Meta': {'object_name': 'PSCCode'},
            'about': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nature': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'company.securityclearancelevel': {
            'Meta': {'object_name': 'SecurityClearanceLevel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'company.smallbusinesscategory': {
            'Meta': {'object_name': 'SmallBusinessCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Industry']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['company']