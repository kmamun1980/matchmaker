from __future__ import unicode_literals
import os

from django.db import models
from django.conf import settings

from djchoices import DjangoChoices, ChoiceItem
from geoposition.fields import GeopositionField
from phonenumber_field.modelfields import PhoneNumberField
from redactor.fields import RedactorField
from cities_light.models import City, Region, Country
from smart_selects.db_fields import ChainedForeignKey
from select2.fields import ManyToManyField as s2_ManyToManyField
from sorl.thumbnail import get_thumbnail, ImageField
from sorl.thumbnail.helpers import ThumbnailError

from apps.base.tools import youtube_video_id_extractor


CONTRACT_VEHICLE = ((1, "MOBIS"),
                    (2, "CEC"),
                    (3, "Schedule 70"),
                    (4, "T4"),
                    (5, "GWAC"))

PRIME_SUB = (("1", "Prime"),
             ("2", "Sub"),)


class VisibilityChoices(DjangoChoices):
    PUB = ChoiceItem('Pub', 'Public')
    GOV = ChoiceItem('Gov', 'Goverment')
    GBP = ChoiceItem('G-BP', 'Goverment or Business Partners')


class OrgCategory(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Department(models.Model):
    org_category = models.ForeignKey(OrgCategory, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Organisation(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Agency(models.Model):
    department = models.ForeignKey(Department, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Office(models.Model):
    agency = models.ForeignKey(Agency, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Industry(models.Model):
    name = models.CharField(max_length=255)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class PSCCode(models.Model):
    code = models.CharField(max_length=255, verbose_name='Code')
    about = RedactorField(
        verbose_name=u"About this code", blank=True, null=True)
    nature = models.CharField(max_length=1, verbose_name='NAICS or SIC',
                              choices=(('1', 'NAICS'), ('2', 'SIC')))
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{0} - {1}'.format(self.code, self.about)


class NSCode(models.Model):
    code = models.CharField(max_length=255, verbose_name='Code')
    about = models.TextField(verbose_name=u"About this code", blank=True,
                             null=True, default='no name')
    nature = models.CharField(max_length=1, verbose_name='NAICS or SIC',
                              choices=(('1', 'NAICS'), ('2', 'SIC')))
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{0} - {1}'.format(self.code, self.about)


class SmallBusinessCategory(models.Model):
    industry = models.ForeignKey(Industry, blank=True, null=True)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class DCAACompliantAccount(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class SecurityClearanceLevel(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class CVGPOrgProfile(models.Model):
    logo = ImageField(upload_to='company_photo',blank=True, null=True)
    name = models.CharField(max_length=255)

    naics = s2_ManyToManyField(
        NSCode, blank=True, null=True, related_name='cvgporgprofiles',
        # comment line below if you have problems with creating schema migration
        #ajax=True, search_field=lambda q: models.Q(code__icontains=q) | models.Q(about__icontains=q)
    )
    psc_fsc = s2_ManyToManyField(
        PSCCode, blank=True, null=True, related_name='cvgporgprofiles',
        # comment line below if you have problems with creating schema migration
        #ajax=True, search_field=lambda q: models.Q(code__icontains=q) | models.Q(about__icontains=q)
    )

    sec_clearance = models.ForeignKey(
        SecurityClearanceLevel,
        verbose_name='Facility Security clearance level',
        blank=True, null=True)
    dcaa_compliant_acc = models.ForeignKey(
        DCAACompliantAccount,
        verbose_name='DCAA Compliant Account',
        blank=True, null=True)

    cage_code = models.CharField(max_length=255, verbose_name='Cage Code',
                                 blank=True, null=True)
    duns_no = models.CharField(max_length=255, verbose_name='DUNS Number',
                               blank=True, null=True)
    avg_rev = models.IntegerField(verbose_name='Three year average revenue',
                                  blank=True, null=True)
    cbla = models.CharField(
        max_length=255, verbose_name='Contruction Bonding Level Aggregate',
        blank=True, null=True)
    cbla_per_contract = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='Contruction Bonding Level Aggregate,Per Contract')
    total_employees = models.IntegerField(verbose_name='Total Number of employees',
        blank=True, null=True)
    vet_employees = models.IntegerField(verbose_name='Number of veteran employees',
        blank=True, null=True)

    industry = models.ForeignKey(
        Industry, blank=True, null=True,
        verbose_name='Industry name')

    small_business_category = models.ManyToManyField(
        SmallBusinessCategory,null=True, blank=True)


    cve_verified_on = models.DateField(blank=True, null=True)
    date_established = models.DateField(blank=True, null=True)
    states_you_do_business = models.ManyToManyField(
        Region, blank=True, null=True, related_name='states_u_do',
        verbose_name='States in which you currently do business')
    states_you_wish_to_do = models.ManyToManyField(
        Region, blank=True, null=True, related_name='states_u_wish',
        verbose_name='States in which you wish to do business')
    primary_poc = models.CharField(
        max_length=55, verbose_name='primary POC', blank=True, null=True,)
    phone = PhoneNumberField(blank=True, null=True)
    street = models.CharField(
        max_length=55, verbose_name='Street', blank=True, null=True)
    bonding_level = models.CharField(
        max_length=55, verbose_name='Construction, Bonding Level, Aggregate',
        blank=True, null=True)
    bonding_number_aggregate = models.IntegerField(blank=True, null=True)
    bonding_number_contract = models.IntegerField(blank=True, null=True)

    zip = models.CharField(max_length=255, verbose_name='Zip Code',
                           null=True, blank=True)
    corporate_overview = models.TextField(blank=True, null=True)
    relevant_links = models.TextField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    website = models.URLField(max_length=200, blank=True, null=False, verbose_name='Website')
    youtube_video = models.URLField(max_length=200, blank=True, null=True, verbose_name='YouTube video URL')

    #Location
    location = GeopositionField(blank=True, null=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    region = ChainedForeignKey(
        Region,
        chained_field="country",
        chained_model_field="country",
        show_all=False,
        auto_choose=True, null=True, blank=True)

    city = ChainedForeignKey(
        City,
        chained_field="region",
        chained_model_field="region",
        show_all=False,
        auto_choose=True, null=True, blank=True)

    business_partner_ship = models.CharField(
        verbose_name="Business Partnership",
        max_length=255,
        blank=True,
        null=True
    )
    business_relation_ship = models.BooleanField(
        verbose_name="Mentor/Protege relationship",
        choices=(
            (False, "Mentor"),
            (True, "Protege")
        ),
        default=False
    )
    organization_name = models.CharField(
        verbose_name='Organization name',
        max_length=255,
        blank=True,
        null=True
    )
    agency_program = models.CharField(
        verbose_name='Agency program',
        max_length=255,
        blank=True,
        null=True
    )
    overview_visiblity = models.CharField(
        max_length=4,blank=True,null=True,
        default=VisibilityChoices.PUB,
        choices=VisibilityChoices.choices,
    )
    average_visiblity = models.CharField(
        max_length=4,blank=True,null=True,
        default=VisibilityChoices.PUB,
        choices=VisibilityChoices.choices,
    )
    documents_visiblity = models.CharField(
        max_length=4,blank=True,null=True,
        default=VisibilityChoices.PUB,
        choices=VisibilityChoices.choices,
    )
    performance_visiblity = models.CharField(
        max_length=4,blank=True,null=True,
        default=VisibilityChoices.PUB,
        choices=VisibilityChoices.choices,
    )

    def __unicode__(self):
            return self.name

    def percentage_vet_employees(self):
        percentage = None
        if self.vet_employees and self.total_employees is not None:
            percentage = (float(self.vet_employees)/float(self.total_employees))*100
        return percentage


    @property
    def youtube_video_id(self):
        return youtube_video_id_extractor(self.youtube_video)

    class Meta:
        verbose_name = 'VGP Organisation Profile'
    
    def get_logo_thumbnail(self):
        if self.logo:
            try:
                return get_thumbnail(self.logo, settings.PROFILE_PICTURE_THUMBNAIL_SIZE, crop='center')
            except (IOError, ThumbnailError):
                pass
        return None

    def get_logo_thumbnail_url(self):
        thumb = self.get_logo_thumbnail()
        if thumb:
            return thumb.url
        return ''


class GVGPOrgProfile(models.Model):

    naics = s2_ManyToManyField(
        NSCode, blank=True, null=True, related_name='gvgporgprofiles',
        # comment line below if you have problems with creating schema migration
        #ajax=True, search_field=lambda q: models.Q(code__icontains=q) | models.Q(about__icontains=q)
    )
    psc_fsc = s2_ManyToManyField(
        PSCCode, blank=True, null=True, related_name='gvgporgprofiles',
        # comment line below if you have problems with creating schema migration
        #ajax=True, search_field=lambda q: models.Q(code__icontains=q) | models.Q(about__icontains=q)
    )

    gov_office = models.CharField(
        max_length=255, verbose_name='Government Department Office',
        null=True, blank=True)
    org_category = models.ForeignKey(
        OrgCategory, blank=True, null=True, related_name='org_category')
    department = models.ForeignKey(
        Department, null=True, blank=True, related_name='org_department')
    agency = ChainedForeignKey(
        Agency,
        chained_field="department",
        chained_model_field="department",
        show_all=False,
        auto_choose=False, null=True, blank=True, related_name='org_agency')

    office = ChainedForeignKey(
        Office,
        chained_field="agency",
        chained_model_field="agency",
        show_all=False,
        auto_choose=False, null=True, blank=True, related_name='org_agency')
    organisation = models.ForeignKey(
        Organisation, blank=True, null=True, related_name='org_visn')
    industry = models.ForeignKey(
        Industry,blank=True, null=True, related_name='industry')
    website = models.URLField(max_length=255, verbose_name='Website')
    youtube_video = models.URLField(max_length=200, blank=True, null=True, verbose_name='YouTube video URL')
    program_names = RedactorField(blank=True, null=True)
    major_initiatives = RedactorField(blank=True, null=True)
    mission = RedactorField(blank=True, null=True)
    key_objectives = RedactorField(blank=True, null=True)
    relevant_links = RedactorField(null=True, blank=True)
    greatest_challenges = RedactorField(blank=True, null=True)
    contractor_needs = RedactorField(blank=True, null=True)
    c_needs_for = RedactorField(
        blank=True, null=True,
        verbose_name='Contractor Needs for small business')

    #company_video = models.CharField(max_length=512, null=True, blank=True)

    @property
    def youtube_video_id(self):
        return youtube_video_id_extractor(self.youtube_video)

    def __unicode__(self):
        try:
            return self.office.name
        except Exception:
            return 'office'

    class Meta:
        verbose_name = 'VGP Gov Organisation'


class BusinessCert(models.Model):
    certification_name = models.CharField(max_length=255, blank=True, null=True)
    date_obtained = models.DateField()
    user = models.ForeignKey(CVGPOrgProfile)
    document = models.FileField(upload_to='documents', blank=True, null=True)

    @property
    def file_name(self):
        if self.document:
            return os.path.basename(self.document.name)
        return ''
    

class CoreStrength(models.Model):
    is_core = models.CharField(
        max_length=1, choices=(('1', 'Core'), ('2', 'Additional')))
    capability = models.CharField(max_length=255, blank=True, null=True)
    category = models.CharField(max_length=255, blank=True, null=True)
    description = RedactorField(blank=True, null=True)
    naics = models.ManyToManyField(
        NSCode, blank=True, null=True, related_name='core_strength_naics')
    created_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(CVGPOrgProfile)


class CPartneringNeed(models.Model):
    vgp_user = models.ForeignKey(CVGPOrgProfile)
    visibility = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='Visibility of Business Requirement')
    name = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='Business Requirement name')
    description = RedactorField(
        blank=True, null=True,
        verbose_name='Visibility of Business Requirement')
    market_segment = models.CharField(max_length=255, blank=True, null=True)
    client = models.CharField(max_length=255, blank=True, null=True)
    poc = models.CharField(max_length=255, blank=True, null=True)
    incubent_contractor = models.CharField(
        max_length=255, blank=True, null=True)
    estimated_value = models.CharField(max_length=255, blank=True, null=True)
    procurement_method = models.CharField(max_length=255, blank=True, null=True)
    current_exp_date = models.DateField(
        blank=True, null=True, verbose_name='Current contract expiration date')
    estimated_start_date = models.DateField(blank=True, null=True)
    security_requirement = models.DateField(blank=True, null=True)
    documents = models.FileField(upload_to='documents', blank=True, null=True)
    small_business_types = models.CharField(
        max_length=255, blank=True, null=True)
    place_of_work = models.CharField(max_length=255, blank=True, null=True)
    geographic_constraints = RedactorField(blank=True, null=True)
    capabilities_constraints = RedactorField(blank=True, null=True)
    relevant_links = RedactorField(blank=True, null=True)


class CompanyDocument(models.Model):
    company = models.ForeignKey('CVGPOrgProfile', related_name='document_set')
    uploaded = models.DateTimeField(auto_now_add=True)
    visibility = models.CharField(
        max_length=4,blank=True,null=True,
        default=VisibilityChoices.PUB,
        choices=VisibilityChoices.choices,
    )

    document = models.FileField(
        verbose_name='Document',
        upload_to=lambda instance, filepath: os.path.join(
            'documents',
            'company_%d' % instance.company.pk,
            filepath
        )
    )

    def filename(self):
        return os.path.basename(self.document.name)


class CompanyPerformance(models.Model):
    company = models.ForeignKey('CVGPOrgProfile',
                                related_name='performance_set')
    name = models.CharField(
        verbose_name='name',
        max_length=255
    )
    role = models.CharField(
        verbose_name='role',
        max_length=255
    )
    primary_company = models.CharField(
        verbose_name='Primary company',
        max_length=255,
        blank=True,
        null=True
    )
    client_org = models.CharField(
        verbose_name='CLient org',
        max_length=255,
        blank=True,
        null=True
    )
    contract = models.CharField(
        verbose_name='Contract',
        max_length=255,
        blank=True,
        null=True
    )
    contract_value = models.CharField(
        verbose_name='TOT Contract value',
        max_length=255,
        blank=True,
        null=True
    )
    is_active = models.BooleanField(
        verbose_name='Active?',
        default=False
    )
    start_date = models.DateField(
        verbose_name='Start date',
        blank=True,
        null=True
    )
    end_date = models.DateField(
        verbose_name='End date',
        blank=True,
        null=True
    )
    scope_desc = models.TextField(
        verbose_name='Scope desc',
        blank=True,
        null=True
    )
