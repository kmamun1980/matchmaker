from django.core.management.base import BaseCommand

from apps.company.management.tools import populate_nscode_from_xls


class Command(BaseCommand):

    def handle(self, *args, **options):

        populate_nscode_from_xls()

