import xlrd

from apps.company.models import NSCode, PSCCode


def populate_nscode_from_xls():

    workbook = xlrd.open_workbook('csv/naics.xls')
    worksheet = workbook.sheet_by_name('2012NAICS')
    num_rows = worksheet.nrows - 1
    num_cells = worksheet.ncols - 1
    curr_row = 0

    while curr_row < num_rows:
        curr_row += 1
        row = worksheet.row(curr_row)

        curr_cell = -1

        # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank

        code = worksheet.cell_value(curr_row, 0)
        name = worksheet.cell_value(curr_row, 1)

        try:
            code = int(code)
            nscode, created = NSCode.objects.get_or_create(code=code, about=name)
        except:
            print 'No code ({}) in row {}'.format(code, curr_row + 1)
        else:
            if created:
                print 'NSCode created (row {})'.format(curr_row + 1)
            else:
                print 'NSCode already exists (row {})'.format(curr_row + 1)


def populate_psccode_from_xls():

    workbook = xlrd.open_workbook('csv/psc.xls')
    worksheet = workbook.sheet_by_name('Sheet1')
    num_rows = worksheet.nrows - 1
    num_cells = worksheet.ncols - 1
    curr_row = 0

    while curr_row < num_rows:
        curr_row += 1
        row = worksheet.row(curr_row)

        curr_cell = -1

        # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank

        code = worksheet.cell_value(curr_row, 0)
        name = worksheet.cell_value(curr_row, 1)

        if code:
            nscode, created = PSCCode.objects.get_or_create(code=code, about=name.lower().capitalize())
            if created:
                print 'PSCCode created (row {})'.format(curr_row + 1)
            else:
                print 'PSCCode already exists (row {})'.format(curr_row + 1)
        else:
            print 'No code ({}) in row {}'.format(code, curr_row + 1)

