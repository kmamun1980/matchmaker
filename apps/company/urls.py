from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from apps.company.views import CreateCompany, CreateGovOrg, EditGovOrg, EditCompany
from apps.users.decorators import profile_complete_required

from . import views


urlpatterns = patterns('',
    url(r'^edit-gov-org/$', profile_complete_required(EditGovOrg.as_view()),
        name='edit_gov_org'),
    url(r'^add-gov-org/$', login_required(CreateGovOrg.as_view()), 
        name='create_gov_org'),
    url(r'^company-profile/(?P<id>\d+)/$', 'apps.company.views.CompanyProfile', 
        name='company_profile'),
    url(r'^organisation-profile/(?P<id>\d+)/$', 'apps.company.views.GovOrg', 
        name='gov_org'),
    url(r'^add-company-profile/$', login_required(CreateCompany.as_view()), name='add_company'),
    url(r'^edit-company-profile/$', login_required(EditCompany.as_view()), 
        name='edit_company'),
    url(r'^edit-vosb-profile/$', 'apps.company.views.add_vosb', name='add_vosb'),
    url(r'^edit-cvgp/(?P<id>\d+)/$', 'apps.company.views.edit_cvgp_org_profile', 
        name='apps.edit_cvgp_org_profile'),

    #url(r'^create-gvgp/$', 'apps.company.views.create_gvgp_org_profile', name='apps.create_gvgp_org_profile'),

    url(r'^edit-gvgp/(?P<id>\d+)/$', 'apps.company.views.edit_gvgp_org_profile', 
        name='apps.edit_gvgp_org_profile'),

    url(r'^create-industry/$', 'apps.company.views.create_industry_profile', 
        name='apps.create_industry_profile'),

    url(r'^edit-industry/(?P<id>\d+)/$', 'apps.company.views.edit_industry_profile', 
        name='apps.edit_industry_profile'),

    url(r'^add-vosb-info/$', 'apps.company.views.add_vosb', name='add_vosb_info'),
    url(r'^add-vosb-doc/$', 'apps.company.views.add_doc', name='add_vosb_doc'),
    url(r'^add-vosb-code/$', 'apps.company.views.add_vosb_code', name='add_code'),
    url(r'^add-vosb-cert/$', 'apps.company.views.add_vosb_cert', name='add_cert'),
    url(r'^add-vosb-overview/$', 'apps.company.views.add_overview', name='add_overview'),
    url(r'^add-partnering-need/$', 'apps.company.views.add_partner_need', name='add_partnering_need'),




    url(
        r'^search/codes/$',
        views.SearchCodesView.as_view(),
        name='search_codes'
    ),
    url(
        r'^get/naics/$',
        views.GetNaicsByPksView.as_view(),
        name='get_naics'
    ),
    url(
        r'^get/psc/$',
        views.GetPscByPksView.as_view(),
        name='get_psc'
    )
)

