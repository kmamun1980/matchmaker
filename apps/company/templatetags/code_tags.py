# -*- coding: utf-8 -*-
from django.template import Library

from ..forms import SearchProductForm

register = Library()

@register.inclusion_tag("company/tags/code_modal.html")
def code_modal():
    return {'form': SearchProductForm()}