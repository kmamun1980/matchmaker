from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms import layout
from crispy_forms.bootstrap import StrictButton
from django.forms import ChoiceField
from django.core.urlresolvers import reverse

from models import CVGPOrgProfile, GVGPOrgProfile, Industry, OrgCategory
from smart_selects.form_fields import ChainedModelChoiceField
from django.core.exceptions import ValidationError
from models import (CVGPOrgProfile, GVGPOrgProfile, Industry, OrgCategory,SmallBusinessCategory,
    Department, CompanyDocument, CompanyPerformance, VisibilityChoices)


class CompanyForm(forms.ModelForm):

    class Meta:
        model = CVGPOrgProfile
        exclude = ['logo']


class CVGPOrgProfileBasicForm(forms.ModelForm):

    class Meta:
        model = CVGPOrgProfile
        fields = ['name','street', 'zip', 'phone', 'website', 'youtube_video',
                  'country','region','city', 'logo']

    def __init__(self, *args, **kwargs):
        super(CVGPOrgProfileBasicForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget.attrs['placeholder'] = 'Company Name'
        self.fields['street'].widget.attrs['placeholder'] = 'Street Address'
        self.fields['zip'].widget.attrs['placeholder'] = 'Zip Code'
        self.fields['phone'].widget.attrs['placeholder'] = 'Phone/Mobile'
        self.fields['website'].widget.attrs['placeholder'] = 'Web'
        self.fields['youtube_video'].widget.attrs['placeholder'] = 'YouTube video URL'


class CVGPOrgProfileVOSB(forms.ModelForm):

    class Meta:
        model = CVGPOrgProfile



class CVGPOrgProfileCodesForm(forms.ModelForm):

    class Meta:
        model = CVGPOrgProfile
        fields = ['cage_code','duns_no','naics', 'psc_fsc',
                  'dcaa_compliant_acc', 'sec_clearance', 
                  'bonding_number_aggregate', 'bonding_number_contract']

    def __init__(self, *args, **kwargs):
        super(CVGPOrgProfileCodesForm, self).__init__(*args, **kwargs)
        self.fields['cage_code'].required = True
        self.fields['duns_no'].required = True

        self.fields['naics'].widget = forms.MultipleHiddenInput()
        self.fields['psc_fsc'].widget = forms.MultipleHiddenInput()

        for f in self.fields:
            if f not in ['naics', 'psc_fsc']:
                self.fields[f].widget.attrs['class'] = 'form-control'


class CVGPOrgProfileOverviewForm(forms.ModelForm):

    class Meta:
        model = CVGPOrgProfile
        fields = ['corporate_overview', 'states_you_do_business', 
                  'states_you_wish_to_do', 'small_business_category',
                  'date_established', 'industry', 'total_employees', 
                  'relevant_links', 'date_established', 'cve_verified_on',
                  'vet_employees', 'primary_poc', 'avg_rev', 
                  'average_visiblity', 'overview_visiblity']

    def __init__(self, *args, **kwargs):
        super(CVGPOrgProfileOverviewForm, self).__init__(*args, **kwargs)
        self.fields['states_you_do_business'].required = True
        self.fields['states_you_wish_to_do'].required = True
        self.fields['date_established'].required = True
        self.fields['industry'].required = True

        for field_name in self.fields:
            self.fields[field_name].widget.attrs['class'] = 'form-control'


class CVGPOrgProfile2Form(forms.ModelForm):

    class Meta:
        model = CVGPOrgProfile
        exclude = ['logo', 'name']

    def __init__(self, *args, **kwargs):
        super(CVGPOrgProfile2Form, self).__init__(*args, **kwargs)
        self.fields['states_you_do_business'].required = True
        self.fields['states_you_wish_to_do'].required = True
        self.fields['date_established'].required = True
        self.fields['industry'].required = True

        for field_name in self.fields:
            self.fields[field_name].widget.attrs['class'] = 'form-control'


class GVGPOrgProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(GVGPOrgProfileForm, self).__init__(*args, **kwargs)
        self.fields['org_category'].required = True
        self.fields['department'].required = True
        self.fields['agency'].required = True
        self.fields['office'].required = True
        self.fields['organisation'].required = True
        self.fields['industry'].required = True
                
        self.fields['naics'].widget = forms.MultipleHiddenInput()
        self.fields['psc_fsc'].widget = forms.MultipleHiddenInput()

        for field_name in self.fields:
            if field_name not in ['naics', 'psc_fsc']:
                self.fields[field_name].widget.attrs['class'] = 'form-control'

        self.fields['website'].widget.attrs['placeholder'] = 'Organization Website'
        self.fields['youtube_video'].widget.attrs['placeholder'] = 'YouTube video URL'

    '''
    def clean(self):
        dt = self.cleaned_data
        if dt.get('department', False) and \
                dt.get('agency', False) and \
                dt.get('office', False) and \
                dt.get('organisation', False) and \
                dt.get('industry', False):
            if GVGPOrgProfile.objects.filter(
                agency=dt.get('agency'),
                office=dt.get('office'),
                organisation=dt.get('organisation'),
                industry=dt.get('industry')
            ).count() > 0:
                if not self._errors.has_key('industry'):
                    from django.forms.util import ErrorList
                    self._errors['industry'] = ErrorList()
                self._errors['industry'].append('GOVCompany already exists')
        return dt
    '''
    class Meta:
        model = GVGPOrgProfile


# form to edit company goverment
class GVGPOrgProfileForm2(GVGPOrgProfileForm):

    class Meta:
        model = GVGPOrgProfile
        exclude = ['organization']


class GVGPOrgProfileRegistrationForm(GVGPOrgProfileForm):
    def __init__(self, *args, **kwargs):
        super(GVGPOrgProfileRegistrationForm, self).__init__(*args, **kwargs)
        #self.fields['organisation'].choices = []


class GVGPOrgProfileSelectForm(forms.ModelForm):
    role = forms.CharField()

    class Meta:
        model = GVGPOrgProfile
        field = ['org_category', 'department', 'agency', 'office']
        exclude = ['website']

    def __init__(self, *args, **kwargs):
        super(GVGPOrgProfileSelectForm, self).__init__(*args, **kwargs)
        
        self.fields['org_category'].required = True
        self.fields['department'].required = True
        self.fields['agency'].required = True
        self.fields['office'].required = True


class RegisterTypeGovForm(forms.ModelForm):

    class Meta:
        model = GVGPOrgProfile
        field = ['department', 'agency', 'office', 'industry']
        exclude = ['website']

    def __init__(self, *args, **kwargs):
        super(RegisterTypeGovForm, self).__init__(*args, **kwargs)

        self.fields['industry'].required = True
        self.fields['department'].required = True
        #self.fields['department'].queryset = Department.objects.all()
        self.fields['agency'].required = True
        self.fields['office'].required = True


class RegisterTypeComForm(forms.ModelForm):

    company = forms.ChoiceField(
        choices=CVGPOrgProfile.objects.values_list('id', 'name')
    )

    class Meta:
        model = CVGPOrgProfile
        field = ['industry', 'small_business_category']
        exclude = ['website', 'logo']

    def __init__(self, *args, **kwargs):
        super(RegisterTypeComForm, self).__init__(*args, **kwargs)

        self.fields['industry'].required = True
        self.fields['company'].required = True


class IndustryProfileForm(forms.ModelForm):

    class Meta:
        model = Industry

    def __init__(self, *args, **kwargs):
        super(IndustryProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class CompanyDocumentFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(CompanyDocumentFormsetHelper, self).__init__(*args, **kwargs)
        self.template = 'company/helper/document_formset.html'
        self.form_tag=False
        self.disable_csrf=True


class CompanyDocumentForm(forms.ModelForm):

    class Meta:
        model = CompanyDocument
        exclude = ('company', )

    def __init__(self, *args, **kwargs):
        super(CompanyDocumentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.disable_csrf = True
        self.helper.form_tag = False


class CompanyPerformanceFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(CompanyPerformanceFormsetHelper,
              self).__init__(*args, **kwargs)
        self.template = 'company/helper/performance_formset.html'
        self.form_tag = False


class CompanyPerformanceForm(forms.ModelForm):

    class Meta:
        model = CompanyPerformance
        exclude =('company', )

    def __init__(self, *args, **kwargs):
        super(CompanyPerformanceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.disable_csrf = True
        self.helper.form_tag = False


class CompanyPartnerInfoForm(forms.ModelForm):
    class Meta:
        model = CVGPOrgProfile
        fields = (
            'business_partner_ship',
            'business_relation_ship',
            'organization_name',
            'agency_program',
            'documents_visiblity',
            'performance_visiblity'
        )

    def __init__(self, *args, **kwargs):
        super(CompanyPartnerInfoForm, self).__init__(*args, **kwargs)
#         self.fields['business_relation_ship'].widget=forms.RadioSelect()
#         self.fields['business_relation_ship'].choices=(
#             ('0', "Mentor"),
#             ('1', "Protege")
#         )
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.disable_csrf = True

        self.helper.layout = layout.Layout(
                layout.Div(
                    layout.Field(
                        'business_partner_ship',
                        template = 'company/helper/field_template.html'
                    ),
                ),
                layout.Div(
                    layout.Field(
                        'business_relation_ship',
                        template = 'company/helper/select_field_template.html'
                    ),
                ),
                layout.Div(
                    layout.Field(
                        'organization_name',
                        template = 'company/helper/field_template.html'
                    ),
                ),
                layout.Div(
                    layout.Field(
                        'agency_program',
                        template = 'company/helper/field_template.html'
                    ),
                ),
                layout.Div(
                    layout.Field(
                        'documents_visiblity',
                        template = 'company/helper/select_field_template.html'
                    ),
                ),
                layout.Div(
                    layout.Field(
                        'performance_visiblity',
                        template = 'company/helper/select_field_template.html'
                    ),
                ),
            )


class SearchProductForm(forms.Form):
    search_str = forms.CharField(required=True, min_length=3)

    def __init__(self, *args, **kwargs):
        super(SearchProductForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.disable_csrf = True
        self.helper.form_method = "get"
        self.helper.form_id="searchProductForm"
        self.helper.form_action = reverse('search_codes')
        self.helper.layout = layout.Layout(
            'search_str',
             layout.Submit('search', 'Search', css_class='btn-default'),
        )
