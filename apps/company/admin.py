from django.contrib import admin

from models import (
    PSCCode, CVGPOrgProfile, GVGPOrgProfile, Department, Agency,
    Office, Industry, Organisation, OrgCategory, NSCode,
    SecurityClearanceLevel, DCAACompliantAccount,
    SmallBusinessCategory
)


admin.site.register(Organisation)
admin.site.register(OrgCategory)
admin.site.register(Department)
admin.site.register(Agency)
admin.site.register(Office)
admin.site.register(Industry)
admin.site.register(GVGPOrgProfile)
admin.site.register(CVGPOrgProfile)
admin.site.register(PSCCode)
admin.site.register(NSCode)
admin.site.register(SecurityClearanceLevel)
admin.site.register(DCAACompliantAccount)
admin.site.register(SmallBusinessCategory)
