from django.core.management.base import BaseCommand, CommandError

from apps.users.models import User
from apps.events.models import Event, Activity, Attendee, Facility

import datetime

class Command(BaseCommand):
    help = 'Creates an Event, Activity for that Event and fills with Attendees'

    def handle(self, *args, **options):
        event = Event()
        event.name = "Test Event Created By Command"
        event.start_time = datetime.datetime.now()
        event.end_time = event.start_time + datetime.timedelta(1)
        event.save()

        users = User.objects.all().exclude(is_superuser=True)

        for u in users:
            att = Attendee()
            att.user = u
            att.event = event
            att.attend_as = u.vgp_type
            att.save()

        facs = Facility.objects.all()
        if len(facs)>0:
            fac = facs[0]
        else:
            fac = Facility()
            fac.name = "TEST FACILITY"
            fac.building = '1'
            fac.save()

        activity = Activity()
        activity.name = "Test Activity For Event ID: "+str(event.id)
        activity.event = event
        activity.facility = fac
        activity.date = datetime.datetime.today()
        activity.save()



