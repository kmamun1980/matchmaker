from django.conf.urls import patterns, url
from django.conf import settings
from django.views.generic import TemplateView

from . import views
from apps.users.decorators import profile_complete_required

urlpatterns = patterns(
    '',
    url(
        r'^$',
        profile_complete_required(views.EventListView.as_view()),
        name='events'
    ),
    url(
        r'^create/',
        profile_complete_required(views.CreateEventView.as_view()),
        name='create_event'
    ),
    url(
        r'^(?P<pk>\d+)/$',
        profile_complete_required(views.EventDetailsView.as_view()),
        name='event_details'
    ),
    url(
        r'^(?P<pk>\d+)/edit/$',
        profile_complete_required(views.EditEventView.as_view()),
        name='edit_event'
    ),
    url(
        r'^(?P<pk>\d+)/delete/$',
        profile_complete_required(views.DeleteEventView.as_view()),
        name='delete_event'
    ),
    url(
        r'^(?P<event_pk>\d+)/activities/$',
        profile_complete_required(views.ActivitiesListView.as_view()),
        name='activities'
    ),
    url(
        r'^(?P<event_pk>\d+)/create-activity/ajax/$',
        views.AjaxNewActivityView.as_view(),
        name='ajax_new_activity'
    ),
    url(
        r'^update-activity/ajax/(?P<pk>\d+)/$',
        views.AjaxUpdateActivityView.as_view(),
        name='ajax_update_activity'
    ),
    url(
        r'^event-matches/$', 
        TemplateView.as_view(template_name='events/one_one_event_matches.html'), 
        name='event_matches'
    ),
    url(
        r'^event-matches-many/$', 
        TemplateView.as_view(template_name='events/one_many_event_matches.html'), 
        name='event_matches_many'
    ),
    url(
        r'^buyer-match-schedule/$', 
        TemplateView.as_view(template_name='events/buyer_matchmaker_schedule.html'), 
        name='buyer_matchmaker_schedule'
    ),
    url(
        r'^seller-match-schedule/$', 
        TemplateView.as_view(template_name='events/seller_matchmaker_schedule.html'), 
        name='seller_matchmaker_schedule'
    ),
    url(
        r'^event-communication/$', 
        TemplateView.as_view(template_name='events/communication.html'), 
        name='event_communication'
    ),
    url(
        r'^create-activity/$',
        profile_complete_required(views.NewActivity.as_view()),
        name='new_activity'
    ),
    url(r'^activities/(?P<id>\d+)/$', 'apps.events.views.EditActivity', 
        name='activity_edit'),
    url(r'^activities-list/$', 'apps.events.views.ActivitiesList', 
        name='activities_list'),
    url(r'^register-activity/(?P<id>\d+)/$', 'apps.events.views.RegisterActivity', 
        name='register_activity'),
    url(r'^delete-activity/(?P<id>\d+)/$', 'apps.events.views.DeleteActivity', 
        name='delete_activity'),
    url(r'^edit-activity/(?P<id>\d+)/$', 'apps.events.views.EditActivity', 
        name='edit_activity'),
    url(r'^activities-reports/$', 'apps.events.views.ActivitiesReports', 
        name='activities-reports'),
    url(r'^search/$', 'apps.events.views.Search', name='apps.events_search'),
    #url(r'^dashboard/$', 'apps.events.views.dashboard', name='apps.dashboard'),
    url(r'^upcoming/$', 'apps.events.views.upcoming_events', 
        name='apps.events_upcoming_events'),
	url(r'^past/$', 'apps.events.views.past_events', 
        name='apps.events_past_events'),
	url(r'^review/$', 'apps.events.views.needs_review', 
        name='apps.events_needs_review'),
	url(r'^message/$', 'apps.events.views.event_message', 
        name='apps.events_event_message'),
	url(r'^newuser/$', 'apps.events.views.new_user_email_signup', 
        name='apps.events_new_user_email_signup'),
    url(r'^emailpreferences/(?P<username>[\w\d\-\.@+_]+)/$', 'apps.events.views.email_preferences', 
        name='apps.events_email_preferences'),
	# url(r'^(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/edit/$', 'apps.events.views.edit_event',
     #    name='apps.events_edit_event'),
	url(r'^(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/rsvp/yes/newuser/$', 'apps.events.views.rsvp_new_user', 
        name='apps.events_rsvp_new_user'),
	url(r'^(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/rsvp/no/$', 'apps.events.views.rsvp_cancel', 
        name='apps.events_rsvp_cancel'),
	url(r'^(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/rsvp/yes/$', 'apps.events.views.rsvp_event', 
        name='apps.events_rsvp_event'),
	url(r'^(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/endorse/$', 'apps.events.views.endorse', 
        name='apps.events_event_endorse'),
	url(r'^(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/publish/$', 'apps.events.views.event_publish', 
        name='apps.events_event_publish'),
	url(r'^(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/approve/$', 'apps.events.views.event_approve', 
        name='apps.events_event_approve'),
    # url(r'^event/(?P<event_id>\d+)/(?P<event_slug>[\w\d\-\.@+_]+)/$', 'apps.events.views.view_event',
    #     name='apps.events_view_event'),
    # url(r'^event/(?P<event_id>\d+)/$', 'apps.events.views.view_event',
    #     name='apps.events_view_event'),
    url(r'^attend/$', 'apps.events.views.rsvp_event', name='apps.events_rsvp'),
    url(r'^attending/(?P<id>\d+)/$', 'apps.events.views.Register', 
        name='apps.events_register'),
    url(
        regex=r'activitytype/create/$',
        view=views.ActivityTypeCreateView.as_view(),
        name='activity_type_create'
    ),
    url(
        regex=r'activitytype/list/$',
        view=views.ActivityTypeListView.as_view(),
        name='activity_type_list'
    ),
    url(
        regex=r'manage/role/$',
        view=views.ManageRoleView.as_view(),
        name='manage_role_list'
    ),
    url(
        r'^search/facility/',
        views.SearchFacilityView.as_view(),
        name='search-facility'
    ),
    url(
        r'^facility/(?P<pk>\d+)/(?P<looking_date>\d{4}-\d{2}-\d{2})/$',
        views.FacilityAvailableTimeView.as_view(),
        name='facility-available'
    )
)

# Rebuilt views requires to delete
urlpatterns += patterns(
    '',
    url(
        r'^create-activity-old/$',
        'apps.events.views.OldNewActivity',
        name='new_activity_old'
    ),
)


