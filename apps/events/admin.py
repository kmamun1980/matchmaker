from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User


from apps.events.models import Event,Attendee,PrimaryFunction,Host,Facility,Activity, ActivityType, ActivityAttendee, \
    AgendaBasedActivityType, ScheduledBasedActivityType

class EventAdmin(admin.ModelAdmin):
    list_display = ['creator','name','limit','reg_start','created']
    model = Event


class FacilityAdmin(admin.ModelAdmin):
    list_display = ['name', 'room_number', 'building', 'theater_capacity',
                    'classroom_capacity', 'banquet_capacity', 'sqft']


admin.site.register(PrimaryFunction)
admin.site.register(ScheduledBasedActivityType)
admin.site.register(AgendaBasedActivityType)
admin.site.register(Host)
admin.site.register(Facility, FacilityAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Attendee)
admin.site.register(Activity)
admin.site.register(ActivityType)
admin.site.register(ActivityAttendee)

