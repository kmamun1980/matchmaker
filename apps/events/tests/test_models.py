# -*- coding: utf-8 -*-
import datetime

from django import test
from django.contrib.auth import get_user_model
from django.utils import timezone

from .. import models

User = get_user_model()


class FacilityAvailableTestCase(test.TestCase):
    def setUp(self):
        now = datetime.datetime(2014, 7, 20, 14, 30, tzinfo=timezone.utc)
        self.date = now.date()
        self.start_time = now.time()
        self.end_time = (now + datetime.timedelta(hours=5)).time()
        self.user = User.objects.create_user(
            username='user',
            email='user@test.ru',
            password='test'
        )

        self.facility = models.Facility.objects.create(
            building='1',
            name='Test facility'
        )
        self.event = models.Event.objects.create(
            name='Event',
            start_time=self.date,
            end_time=self.date
        )
        self.activity = models.Activity.objects.create(
            name='Activity',
            date=self.date,
            start_time=self.start_time,
            end_time=self.end_time,
            facility=self.facility,
            event=self.event
        )

    def test_bad_start_time(self):
        bad_start_time = datetime.time(15, 30)
        end_time = datetime.time(22,30)
        self.assertFalse(
            self.facility.is_available(self.date, bad_start_time, end_time)
        )

    def test_bad_end_time(self):
        start_time = datetime.time(13,30)
        bad_end_time = datetime.time(15,30)
        self.assertFalse(
            self.facility.is_available(self.date, start_time, bad_end_time)
        )

    def test_inner_time(self):
        bad_start_time = datetime.time(15,30)
        bad_end_time = datetime.time(16, 30)
        self.assertFalse(
            self.facility.is_available(self.date, bad_start_time, bad_end_time)
        )

    def test_outer_time(self):
        bad_start_time = datetime.time(13, 30)
        bad_end_time = datetime.time(22, 30)
        self.assertFalse(
            self.facility.is_available(self.date, bad_start_time, bad_end_time)
        )

    def test_all_ok_left(self):
        start_time = datetime.time(11, 00)
        end_time = datetime.time(12, 30)
        self.assertTrue(
            self.facility.is_available(self.date, start_time, end_time)
        )

    def test_all_ok_right(self):
        start_time = datetime.time(20, 00)
        end_time = datetime.time(21, 30)
        self.assertTrue(
            self.facility.is_available(self.date, start_time, end_time)
        )

    def test_another_date(self):
        self.assertTrue(
            self.facility.is_available(
                self.date + datetime.timedelta(days=5),
                self.start_time,
                self.end_time
            )
        )

    def test_exclude_current_activity(self):
        self.assertTrue(
            self.facility.is_available(
                self.date,
                self.start_time,
                self.end_time,
                exclude_activity=self.activity
            )
        )
