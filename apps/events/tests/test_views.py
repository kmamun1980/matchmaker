# -*- coding: utf-8 -*-
import datetime
import json
import logging

from django import test
from django.test import client
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from .. import models

User = get_user_model()

logger = logging.getLogger('apps.events')


class FacilitySearchTestCase(test.TestCase):

    def setUp(self):
        self.client = client.Client()
        self.first_user = User.objects.create_user(
            username='first',
            email='first@test.ru',
            password='test'
        )

        self.facility_list = [
            models.Facility.objects.create(
                **{
                    'building': str(x),
                    'name': 'Facility %d' % x,
                    'room_number': '40%d' % x,
                    'theater_capacity': x,
                    'classroom_capacity': x,
                    'banquet_capacity': x,
                    'sqft': x**2
                }
            )
            for x in range(1, 10)
        ]

    def test_error_request(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse('search-facility'),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertEquals(data['status'], 'error')
        self.assertIn('form_html', data)

    def test_request_with_date(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse('search-facility'),
            data={'looking_date': '2014-05-15'},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertIn('facility_list', data)
        self.assertEquals(len(data['facility_list']), len(self.facility_list))
        self.assertIn('available_url', data['facility_list'][0])

    def test_request_with_theater_capacity(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse('search-facility'),
            data={
                'looking_date': '2014-05-15',
                'theater_capacity__gte': 2,
                'theater_capacity__lte': 5,
            },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertIn('facility_list', data)
        self.assertEquals(len(data['facility_list']), 4)

    def test_request_with_classroom_capacity(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse('search-facility'),
            data={
                'looking_date': '2014-05-15',
                'classroom_capacity__gte': 4,
                'classroom_capacity__lte': 6,
                },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertIn('facility_list', data)
        self.assertEquals(len(data['facility_list']), 3)

    def test_request_with_banquet_capacity(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse('search-facility'),
            data={
                'looking_date': '2014-05-15',
                'banquet_capacity__gte': 3,
                'banquet_capacity__lte': 7,
                },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertIn('facility_list', data)
        self.assertEquals(len(data['facility_list']), 5)

    def test_request_with_sqft(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse('search-facility'),
            data={
                'looking_date': '2014-05-15',
                'sqft__gte': 5,
                'sqft__lte': 20,
                },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertIn('facility_list', data)
        self.assertEquals(len(data['facility_list']), 2)

    def test_request_with_building(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse('search-facility'),
            data={
                'looking_date': '2014-05-15',
                'building': '1'
                },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertIn('facility_list', data)
        self.assertEquals(len(data['facility_list']), 1)


class FacilityAvailableTestCase(test.TestCase):

    def setUp(self):
        self.client = client.Client()
        self.today = datetime.datetime(
            *timezone.now().timetuple()[:3],
            tzinfo=timezone.utc
        )
        self.user = User.objects.create_user(
            username='first',
            email='first@test.ru',
            password='test'
        )

        self.facility = models.Facility.objects.create(
            name='First',
            building='1'
        )

        self.start_time = datetime.datetime(2000, 1, 1)
        self.end_time = datetime.datetime(2100, 1, 1)

        self.event = models.Event.objects.create(
            name='Event',
            start_time=self.start_time,
            end_time=self.end_time
        )

        self.activity = models.Activity.objects.create(
            name='Activity',
            facility=self.facility,
            date=self.today.date(),
            start_time=datetime.time(18, 00),
            end_time=datetime.time(20, 59),
            created_by=self.user,
            event=self.event
        )

    def test_wrong_date(self):
        self.client.login(username='first', password='test')
        response = self.client.get(reverse(
            'facility-available',
            kwargs={
                'pk': self.facility.pk,
                'looking_date': '2014-15-88'
            },

        ), HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('status', data)
        self.assertEquals(data['status'], 'error')

    def test_free_day(self):
        self.client.login(username='first', password='test')
        tomorrow = self.today + datetime.timedelta(days=1)
        response = self.client.get(reverse(
            'facility-available',
            kwargs={
                'pk': self.facility.pk,
                'looking_date': tomorrow.strftime("%Y-%m-%d")
            },

        ), HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('availability_list', data)
        self.assertEquals(
            len([x for x in data['availability_list'] if x['is_available']]),
            48
        )

    def test_day_with_not_available_evening(self):
        self.client.login(username='first', password='test')
        response = self.client.get(reverse(
            'facility-available',
            kwargs={
                'pk': self.facility.pk,
                'looking_date': self.today.strftime("%Y-%m-%d")
            },
        ), HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)

        self.assertIn('availability_list', data)

        date_format = "%Y-%m-%d %H:%M"
        logger.debug(data['availability_list'])
        logger.debug(
            'Start time: %s' % self.activity.start_time.strftime(date_format)
        )
        logger.debug(
            'End time: %s' % self.activity.end_time.strftime(date_format)
        )
        logger.debug(
            'Today: %s' % self.today.strftime(date_format)
        )

        self.assertEquals(
            len([x for x in data['availability_list'] if x['is_available']]),
            42
        )

    def test_available_but_selected_evening(self):
        self.client.login(username='first', password='test')
        response = self.client.get(
            reverse(
                'facility-available',
                kwargs={
                    'pk': self.facility.pk,
                    'looking_date': self.today.strftime("%Y-%m-%d")
                }
            ),
            {'activity_pk': self.activity.pk},
            HTTP_X_REQEUSTED_WTH='XMLHttpRequest'
        )

        self.assertEquals(response.status_code, 200)
        data = json.loads(response.content)

        self.assertEquals(
            len([x for x in data['availability_list'] if x['is_available']]),
            48
        )

        self.assertEquals(
            len([x for x in data['availability_list'] if x['is_selected']]),
            6
        )

