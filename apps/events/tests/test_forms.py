# -*- coding: utf-8 -*-
import os
import datetime

import mock

from django import test
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone

from .. import models
from .. import forms

User = get_user_model()


class ActivityFormTestCase(test.TestCase):
    def setUp(self):
        self.file_name = '/tmp/test-upload-presentation'
        fp = open(self.file_name, 'w')
        fp.write("Test data")
        fp.close()

        self.user = User.objects.create_user(
            username='user',
            email='user@test.ru',
            password='test'
        )

        self.facility = models.Facility.objects.create(
            building='1',
            name='Test facility'
        )

        self.start_time = datetime.datetime(2000, 1, 1)
        self.end_time = datetime.datetime(2100, 1, 1)

        self.event = models.Event.objects.create(
            name='Event',
            start_time=self.start_time.date(),
            end_time=self.end_time.date()
        )

    def tearDown(self):
        os.remove(self.file_name)

    def test_form_valid(self):
        form = forms.ActivityForm(
            {
                'name': 'Test',
                'date': '2014-07-12',
                'start_time': '18:00',
                'end_time': '21:00',
                'facility': self.facility.pk,
                'email_presentation': False,
                'visibility': True,
                'feedback': False,
            },
            event=self.event
        )
        print form.errors
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = forms.ActivityForm(event=self.event)
        self.assertFalse(form.is_valid())

    def test_form_without_presentation_but_with_email_presentation_on(self):
        form = forms.ActivityForm(
            {
                'name': 'Test',
                'date': '2014-07-12',
                'start_time': '18:00',
                'end_time': '21:00',
                'facility': self.facility.pk,
                'email_presentation': True,
                'visibility': True,
                'feedback': False
            },
            event=self.event
        )
        self.assertFalse(form.is_valid())

    def test_form_with_presentation_but_without_email_presentation(self):
        upload_file = open(self.file_name, 'rb')
        form = forms.ActivityForm(
            {
                'name': 'Test',
                'date': '2014-07-12',
                'start_time': '18:00',
                'end_time': '21:00',
                'facility': self.facility.pk,
                'email_presentation': False,
                'visibility': True,
                'feedback': False
            },
            {
                'presentation_file': SimpleUploadedFile(
                    upload_file.name,
                    upload_file.read()
                )
            },
            event=self.event
        )
        self.assertFalse(form.is_valid())

    def test_form_valid_with_presentation(self):
        upload_file = open(self.file_name, 'rb')
        form = forms.ActivityForm(
            {
                'name': 'Test',
                'date': '2014-07-12',
                'start_time': '18:00',
                'end_time': '21:00',
                'facility': self.facility.pk,
                'email_presentation': True,
                'visibility': True,
                'feedback': False
            },
            {
                'presentation_file': SimpleUploadedFile(
                    upload_file.name,
                    upload_file.read()
                )
            },
            event=self.event
        )
        self.assertTrue(form.is_valid())

    def test_not_available_facility_bad_start_date(self):
        with mock.patch('apps.events.models.Facility.is_available',
                        autospec=True) as patch_method:
            patch_method.return_value = False
            now = timezone.now()
            bad_start_time = now + datetime.timedelta(hours=2)
            good_end_time = now + datetime.timedelta(hours=10)
            form = forms.ActivityForm(
                {
                    'name': 'Test',
                    'date': bad_start_time.strftime('%Y-%m-%d'),
                    'start_time': bad_start_time.strftime('%H:%M'),
                    'end_time': good_end_time.strftime('%H:%M'),
                    'facility': self.facility.pk,
                    'email_presentation': False,
                    'visibility': True,
                    'feedback': False
                },
                event=self.event
            )
            self.assertFalse(form.is_valid())
