import uuid
import os
import requests
import datetime

from django.db import models
from django.contrib.auth.models import Group
from django.conf import settings
from django.db.models.signals import post_save
from django.db.models import Q
from django.template.defaultfilters import slugify
from django.utils import timezone

from djchoices import DjangoChoices, ChoiceItem


from model_utils.models import TimeStampedModel
from cities_light.models import City, Region, Country
from phonenumber_field.modelfields import PhoneNumberField
from redactor.fields import RedactorField

from apps.users.models import User, HostRegistration
from apps.users.models import User, Industry


BUILDING = (('1', 'Exhibit Hall'), ('2', 'Auditorium'),
            ('3', 'Boardroom'), ('4', 'Thomas Murphy Ballroom'),
            ('5', 'Georgia Ballroom'), ('6', 'Build-Out Meeting Rooms in Exhibit Halls A3 and B1'),
            ('7', 'Building B - Level 2 Meeting Rooms'), ('8', 'Building B - Level 3 Meeting Rooms'),
            ('9', 'Building B - Level 4 Meeting Rooms'), ('10', 'BuildingA - Level 3 Meeting Rooms'),
            ('11', 'BuildingA - Level 4 Meeting Rooms'), ('12', 'BuildingC - Level 1 Meeting Rooms'),
            ('13', 'BuildingC - Level 2 Meeting Rooms'), ('14', 'BuildingC - Level 3 Meeting Rooms'),
            ('15', 'Build-Out Meeting Rooms in Exhibit Hall C1'), ('16', 'Build-Out Meeting Rooms in Exhibit Hall C2'),
            ('17', 'Build-Out Meeting Rooms in Exhibit Hall C3'), ('18', 'Build-Out Meeting Rooms in Exhibit Hall C4'),
)

SEATING_TYPE = (('1', 'Banquet'), ('2', 'Theater'),)


#Model not needed
class ActivityType(TimeStampedModel):
    short_name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255)
    creator = models.ForeignKey(User, blank=True, null=True)
    AgendaBasedActivityType = models.BooleanField(
        verbose_name='Agenda based activity type',
        default=False,
    )
    ScheduledBasedActivityType = models.BooleanField(
        verbose_name='Scheduled based activity type',
        default=False,
    )
    # TODO :: Add group staff  below here, and use south to update db
    groups = models.ManyToManyField(Group, blank=True, null=True)

    def __unicode__(self):
        return self.short_name


class AgendaBasedActivityType(TimeStampedModel):
    short_name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255)
    creator = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.short_name


class ScheduledBasedActivityType(TimeStampedModel):
    short_name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255)
    creator = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.short_name



def event_img_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    # rename file to random string
    filename = "%s.%s" % (uuid.uuid4(), ext.lower())

    upload_path = "data/events/%s/" % instance.slug
    upload_abs_path = os.path.join(settings.MEDIA_ROOT, upload_path)
    if not os.path.exists(upload_abs_path):
        os.makedirs(upload_abs_path)
    return os.path.join(upload_path, filename)


class Facility(models.Model):
    building = models.CharField(
        max_length=30, choices=BUILDING, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    room_number = models.CharField(max_length=255, blank=True, null=True)
    theater_capacity = models.PositiveIntegerField(blank=True, null=True)
    classroom_capacity = models.PositiveIntegerField(blank=True, null=True)
    banquet_capacity = models.PositiveIntegerField(blank=True, null=True)
    sqft = models.PositiveIntegerField(blank=True, null=True)
    no_rounds = models.PositiveIntegerField(blank=True, null=True,
                                            verbose_name='Number of rounds',
                                            help_text='For N/Working roundtables etc')
    booth_capacity = models.PositiveIntegerField(blank=True, null=True,
                                                 verbose_name='Booth Capacity',
                                                 help_text='For Exhibit halls - Minimum 10X10')
    location = models.CharField(
        max_length=255, blank=True, null=True, help_text='Optional')

    class Meta:
        ordering = ('building', 'room_number')

    def __unicode__(self):
        return self.name

    def is_available(self, date, start_time, end_time, exclude_activity=None):
        qs = self.activity_set.filter(date=date).filter(
            Q(
                start_time__lte=start_time,
                end_time__gte=start_time
            ) | Q(
                start_time__lte=end_time,
                end_time__gte=end_time
            ) | Q(
                start_time__gte=start_time,
                end_time__lte=end_time
            )
        )

        if not exclude_activity is None:
            qs = qs.exclude(pk=exclude_activity.pk)
        return qs.count() == 0


# Create your models here.
class Event(models.Model):
    name = models.CharField(
        max_length=255, help_text="Event Name")
    event_img = models.ImageField(
        verbose_name='Cover image',
        upload_to='data/events/',
        blank=True,
        null=True
    )
    subject = models.CharField(
        max_length=255, help_text="Event subject", blank=True, null=True
    )
    description = models.TextField(
        help_text='Event description',
        blank=True,
        null=True
    )
    slug = models.CharField(
        max_length=255,
        help_text="Auto.",
        blank=True,
        null=True
    )
    start_time = models.DateField(
        verbose_name="Event start date"
    )
    end_time = models.DateField(
        verbose_name="Event end date"
    )
    reg_start = models.DateTimeField(
        verbose_name="Registration start date and time",
        blank=True,
        null=True
    )
    reg_end = models.DateTimeField(
        verbose_name="Registration end date and time",
        blank=True,
        null=True
    )
    attendee_threshold = models.PositiveIntegerField(
        verbose_name='Attendee threshold',
        help_text='Send sms if attendees more then attendee threshold',
        blank=True,
        null=True
    )

    keynote_speakers_name = models.CharField(
        verbose_name="Keynote speakers name",
        max_length=255,
        blank=True,
        null=True
    )
    keynote_speakers_bio = models.TextField(
        verbose_name="Keynote speaker bio",
        blank=True,
        null=True
    )

    venue = models.CharField(
        verbose_name='Venue',
        max_length=255,
        blank=True,
        null=True
    )
    is_paid = models.BooleanField(
        verbose_name='Event type',
        default=False,
        choices=(
            (True, 'Pay'),
            (False, 'Free')
        )
    )
    fee = models.DecimalField(
        verbose_name='Event fee',
        max_digits=8,
        decimal_places=2,
        blank=True,
        null=True
    )
    merchant_code = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    is_opened = models.BooleanField(
        verbose_name='Event type',
        default=False,
        choices=(
            (False, 'Close'),
            (True, 'Open')
        )
    )

    limit = models.PositiveIntegerField(
        default=0,
        help_text="Specify a cap on the number of RSVPs, or 0 for no limit.",
        blank=True,
        null=True
    )
    require_reg_code = models.BooleanField(
        default=False,
        help_text="Require registration code",
        choices=(
            (False, 'No'),
            (True, 'Yes')
        )
    )
    reg_code = models.CharField(
        max_length=255, help_text="Registration Code.", blank=True, null=True)
    show_schedule = models.BooleanField(
        default=False,
        choices=(
            (False, 'No'),
            (True, 'Yes')
        )
    )
    schedule = RedactorField(help_text="Schedule", blank=True, null=True)
    social = models.BooleanField(
        default=False,
        help_text="Social",
        choices=(
            (False, 'No'),
            (True, 'Yes')
        )
    )
    active = models.BooleanField(
        default=False,
        help_text="Active",
        choices=(
            (False, 'No'),
            (True, 'Yes')
        )
    )
    visibility = models.BooleanField(
        default=False,
        help_text="Private or Public event?",
        choices=(
            (False, 'Private'),
            (True, 'Public')
        )
    )

    feedback = models.BooleanField(
        default=False,
        help_text="Available for feedback",
        choices=(
            (False, 'No'),
            (True, 'Yes')
        )
    )
    moderators = models.ManyToManyField(
        'users.User',
        related_name='moderator_events',
        blank=True,
        null=True
    )
    # contact
    organizer_notes = RedactorField(
        blank=True, null=True,
        help_text="These will only be visible to other organizers")
    phone = PhoneNumberField(null=True, blank=True)
    contact = models.CharField(max_length=10, null=True, blank=True)
    creator = models.ForeignKey(User, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name

    @property
    def attendees(self):
        attendees = Attendee.objects.filter(event=self.id).count()
        return attendees

    def can_moderate(self, user):
        return user in self.moderators.all() or user.is_superuser

    def save(self, *args, **kwargs):
        if not self.slug:
            slug = slugify(self.name)
            if slug:
                same_slug_count = self._default_manager.filter(
                    slug__startswith=slug).count()
                if same_slug_count:
                    slug += str(same_slug_count)
                self.slug = slug
            else:
                # fallback to user id
                slug = self.id
        super(Event, self).save(*args, **kwargs)


class EventDownload(models.Model):
    item = models.FileField(upload_to='events/files')
    description = models.CharField(max_length=255, blank=True, null=True)
    event = models.ForeignKey(Event, blank=True, null=True)


class Attendee(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    attend_as = models.CharField(
        choices=(('1', 'Buyer'), ('2', 'Seller')), max_length=1)
    event = models.ForeignKey(Event, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True, help_text="False if cancelled")


def create_route(route_name, route_pattern, path):

    mailgun_api_key = settings.MAILGUN_API_KEY
    list_domain = settings.LIST_DOMAIN
    # strip the initial slash
    forward_url = os.path.join(list_domain, path)
    forward_url = "https://" + forward_url

    expression = "match_recipient('%s')" % route_pattern

    forward_url = "forward('%s')" % forward_url

    return requests.post("https://api.mailgun.net/v2/routes",
                         auth=("api", mailgun_api_key),
                         data={"priority": 1,
                               "description": route_name,
                               # the route pattern is a string but still needs
                               # to be quoted
                               "expression": expression,
                               "action": forward_url,
                               }
                         )


def create_event_email(sender, instance, created, using, **kwargs):
    if created is True:
        # XXX TODO should probably hash the ID or name of the event so we're
        # not info leaking here, if we care?
        route_pattern = "event%d" % instance.id
        route_name = 'Event %d' % instance.id
        path = "events/message/"
        resp = create_route(route_name, route_pattern, path)

#post_save.connect(create_event_email, sender=Event)


class EventNotifications(models.Model):
    user = models.OneToOneField(User, related_name='event_notifications')
    # send reminders on day-of the event?
    reminders = models.BooleanField(default=True)
    # receive weekly announcements about upcoming events?
    weekly = models.BooleanField(default=False)

User.event_notifications = property(
    lambda u: EventNotifications.objects.get_or_create(user=u)[0])

# override the save method of the User model to create the EventNotifications
# object automatically for new users


def add_user_event_notifications(sender, instance, created, using, **kwargs):
    # just accessing the field will create the object, since the field is
    # defined with get_or_create, above.
    instance.event_notifications
    return

post_save.connect(add_user_event_notifications, sender=User)


class PrimaryFunction(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)


class Host(models.Model):
    user = models.ForeignKey(User)
    co_host = models.BooleanField(default=False)
    arrival = models.DateTimeField(null=True, blank=True)
    departure = models.DateTimeField(null=True, blank=True)
    primary_function = models.ForeignKey(
        PrimaryFunction, null=True, blank=True)


# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length=255)
    event = models.ForeignKey(Event)
    agenda_based_activity_type = models.ForeignKey(AgendaBasedActivityType, help_text="type",
                                                   verbose_name='Agenda based activity type', blank=True, null=True)
    scheduled_based_activity_type = models.ForeignKey(ScheduledBasedActivityType, help_text="type", blank=True,
                                                      null=True, verbose_name='Scheduled based activity type')
    #type = models.CharField(
    #    max_length=1, help_text="type", blank=True, choices=ACTIVITY_TYPE)
    created_by = models.ForeignKey(User, blank=True, null=True)
    location = models.CharField(max_length=255, help_text="Location", blank=True, null=True)
    facility = models.ForeignKey(
        Facility,
        help_text="Select a room in 'check availability' button",
        verbose_name='Room'
    )
    date = models.DateField(
        verbose_name='Activity day'
    )
    start_time = models.TimeField(verbose_name="Start time", blank=True, null=True)
    end_time = models.TimeField(verbose_name="End time", blank=True, null=True)
    limit = models.PositiveIntegerField(default=0, verbose_name='No. of seats',
                                        help_text="Specify a cap on the number of RSVPs, or 0 for no limit.",
                                        blank=True, null=True)
    description = RedactorField(blank=True, null=True)
    social = models.BooleanField(default=False, help_text="Social")
    seat_notification_threshold = models.PositiveIntegerField(default=100, help_text="Set seat notification threshold "
                                                                                     "i.e.150 if room capacity is 200",
                                                                                     blank=True, null=True)
    slots = models.CharField(max_length=255, help_text="Slots/ People per table", blank=True, null=True)
    equipment_required = models.CharField(max_length=255, help_text="i.e., projector, screens, mic...",
                                          blank=True, null=True)
    staff_required = models.CharField(max_length=255, help_text="Staff required", blank=True, null=True)
    seating_type = models.CharField(max_length=1, choices=SEATING_TYPE, blank=True, null=True)
    presentation_file = models.FileField(upload_to='events/files', blank=True)
    email_presentation = models.BooleanField(default=False)
    feedback = models.BooleanField(default=False, help_text="Available for feedback - Survey")
    visibility = models.BooleanField(default=False, help_text="Private or Public event?")
    draft = models.BooleanField(default=False, help_text="Event published of not?")

    #filter preference value for the activity if visibility is private
    compatibility_score = models.CharField(max_length=255, help_text="compatibility score value to filter registrants",
                                           blank=True, null=True)

    # contact
    host = models.ForeignKey(HostRegistration, blank=True, null=True, related_name='activity_host')
    co_host = models.ManyToManyField(HostRegistration, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True)
    industry = models.ManyToManyField(Industry, verbose_name="Industry preference for the activity ",
                                      help_text="Select for industry preference of applicants", blank=True, null=True)

    def __unicode__(self):
        return self.name

    def _get_datetime(self, current_time):
        return datetime.datetime(
            self.date.year,
            self.date.month,
            self.date.day,
            current_time.hour,
            current_time.minute
        )

    @property
    def attendees(self):
        attendees = Activity.objects.filter(event=self.id).count()
        return attendees

    @property
    def start_datetime(self):
        return self._get_datetime(self.start_time)

    @property
    def end_datetime(self):
        return self._get_datetime(self.end_time)


class ActivityAttendee(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    #name = models.CharField(max_length=255, blank=True, null=True)
    #attend_as = models.CharField(choices=(('1', 'VOSB'), ('2', 'VOSB Experience')), max_length=1)
    activity = models.ForeignKey(Activity, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=1, help_text="False if cancelled")
    excess_user = models.BooleanField(default=False, help_text="If the registration is full")
