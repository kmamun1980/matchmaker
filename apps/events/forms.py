import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.utils.translation import ugettext as _
from django.contrib.admin.widgets import AdminSplitDateTime
from django.utils.html import format_html
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.forms.forms import ValidationError
from decimal import Decimal

from crispy_forms.helper import FormHelper
from crispy_forms import layout
from crispy_forms import bootstrap

from models import Event, Activity, ActivityType
from models import BUILDING

from django.core.mail import EmailMultiAlternatives

logger = logging.getLogger('apps.events')

User = get_user_model()


class TemplateObject(object):
    """
    Layout object. Contains path to template for render it
    """
    def __init__(self, template_path):
        self.template_path = template_path

    def render(self, form, form_style, context,
               template_pack=layout.TEMPLATE_PACK):
        return render_to_string(self.template_path, context_instance=context)


class EventForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea, required=False)
    keynote_speakers_bio = forms.CharField(widget=forms.Textarea,
                                           required=False)
    #Enable for moderators field
    #moderators = forms.ModelMultipleChoiceField(
    #    queryset=User.objects.filter(gov_department__isnull=False)
    #)

    class Meta:
        model = Event
        excludes = ('creator', 'created', 'updated', 'moderators')

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        instance = kwargs.get('instance')
        logger.debug(instance)
        if not instance is None:
            submit_name = 'update-event'
            submit_value = 'Update event'
            self.helper.form_id = 'updateEventForm'
            self.helper.form_action = reverse(
                'edit_event',
                kwargs={'pk': instance.pk}
            )
        else:
            submit_name = 'create-event'
            submit_value = 'Create event'
            self.helper.form_id = 'createEventForm'
            self.helper.form_action = reverse('create_event')

        self.helper.layout = layout.Layout(
            layout.Div(
                layout.Div(
                    layout.Div(
                        'name',
                        css_class='row'
                    ),
                    layout.Div(
                        'subject',
                        css_class='row'
                    ),
                    layout.Div(
                        'description',
                        css_class='row'
                    ),
                    layout.Div(
                        'keynote_speakers_name',
                        css_class='row'
                    ),
                    layout.Div(
                        'keynote_speakers_bio',
                        css_class='row'
                    ),
                    css_class="col-md-5"
                ),
                layout.Div(
                    layout.Div(
                        'start_time',
                        css_class='row'
                    ),
                    layout.Div(
                        'end_time',
                        css_class='row'
                    ),
                    layout.Div(
                        'reg_start',
                        css_class='row'
                    ),
                    layout.Div(
                        'reg_end',
                        css_class='row'
                    ),
                    layout.Div(
                        'attendee_threshold',
                        css_class='row'
                    ),

                    css_class='col-md-5 col-md-offset-2'
                ),
                css_class='row well'
            ),
            layout.Div(
                layout.Div(
                    layout.Div(
                        'venue',
                        css_class='row'
                    ),
                    layout.Div(
                        'event_img',
                        css_class='row'
                    ),
                    layout.Div(
                        'is_paid',
                        css_class='row'
                    ),
                    layout.Div(
                        'fee',
                        css_class='row'
                    ),
                    layout.Div(
                        'merchant_code',
                        css_class='row'
                    ),
                    layout.Div(
                        'is_opened',
                        css_class='row'
                    ),
                    css_class='col-md-5 well'
                ),
                layout.Div(
                    layout.Div(
                        'visibility',
                        css_class='row'
                    ),
                    layout.Div(
                        'show_schedule',
                        css_class='row'
                    ),
                    layout.Div(
                        'require_reg_code',
                        css_class='row'
                    ),
                    layout.Div(
                        'reg_code',
                        css_class='row'
                    ),
                    layout.Div(
                        'active',
                        css_class='row'
                    ),
                    layout.Div(
                        'survey',
                        css_class='row'
                    ),
                    layout.Div(
                        'feedback',
                        css_class='row'
                    ),
                    layout.Div(
                        'social',
                        css_class='row'
                    ),
                    css_class='col-md-5 col-md-offset-2 well'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.HTML("""
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                """),
                layout.Submit(
                    submit_name,
                    submit_value,
                    css_class='btn btn-primary'
                ),
                css_class='row'
            )
        )

    def clean_fee(self):
        is_paid = self.cleaned_data.get('is_paid')
        fee = self.cleaned_data.get('fee')

        logger.debug("Clean fee")
        logger.debug(is_paid)
        logger.debug(fee)

        if is_paid and fee is None:
            raise forms.ValidationError("Fee should be set if it is paid event")
        if not is_paid and not fee is None:
            raise forms.ValidationError("Fee shouldn't be set if event is free")
        try:
            fee = Decimal(str(fee).replace(',', '.').replace('-', ''))
        except:
            pass

        return fee

    def clean_merchant_code(self):
        is_paid = self.cleaned_data.get('is_paid')
        merchant_code = self.cleaned_data.get('merchant_code')

        logger.debug("Clean fee")
        logger.debug(is_paid)
        logger.debug(merchant_code)

        if is_paid and not merchant_code:
            raise forms.ValidationError("""
                Merchant code should be set if it is paid event
            """)
        if not is_paid and merchant_code:
            raise forms.ValidationError("""
                Fee shouldn't be set if it is free
            """)

        return merchant_code

    def clean_reg_code(self):
        reg_code = self.cleaned_data.get('reg_code')
        require_reg_code = self.cleaned_data.get('require_reg_code')

        logger.debug("Clean reg code")
        logger.debug(require_reg_code)
        logger.debug(reg_code)

        if reg_code and not require_reg_code:
            raise forms.ValidationError("""
                Require code shouldn't be set if it not required
            """)
        if not reg_code and require_reg_code:
            raise forms.ValidationError("""
                Require code shouldn't be set if it required
            """)

        return reg_code

    def clean_presenter(self):
        return self.check_workshop(
            self.cleaned_data.get('presenter'),
            "Presenter should be set if event type is workshop",
            "Presenter shouldn't be set if event type isn't workshop"
        )

    def clean_room_name(self):
        return self.check_workshop(
            self.cleaned_data.get('room_name'),
            "Room name should be set if event type is workshop",
            "Room name shouldn't be set if event type isn't workshop"
        )

    def clean_room_capacity(self):
        return self.check_workshop(
            self.cleaned_data.get('room_capacity'),
            "Room capacity should be set if event type is workshop",
            "Room capacity shouldn't be set if event type isn't workshop"
        )


class ActivityTypeForm(forms.ModelForm):
    class Meta:
        model = ActivityType
        fields = ('short_name', 'full_name', 'groups',)


class WihtSplitDateTime(AdminSplitDateTime):
    """
    A SplitDateTime Widget that has some specific styling.
    """

    def format_output(self, rendered_widgets):
        return format_html(
            '<p class="datetime">{0}</p> <div class="input-append datetimepicker">{1}'
            '<span class="add-on"><i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar">'
            '</i></span></div> <p class="datetime">{2}</p> <div class="input-append bootstrap-timepicker">{3}'
            '<span class="add-on"><i class="icon-time"></i></span></div>',
            _('Date:'), rendered_widgets[0],
            _('Time:'), rendered_widgets[1])


class ActivityForm(forms.ModelForm):
    #email_presentation = forms.ChoiceField(
    #    widget=forms.RadioSelect,
    #    label="Send presentation by email",
    #    choices=((True, 'Yes '), (False, 'No'),),
    #    initial=False,
    #)

    #visibility = forms.ChoiceField(
    #    widget=forms.RadioSelect,
    #    label="Visibility",
    #    choices=((True, 'Public'), (False, 'Private'),),
    #    initial=True,
    #)

    #feedback = forms.ChoiceField(
    #    widget=forms.RadioSelect,
    #    label="Send survey  ",
    #    choices=((True, 'Yes '), (False, 'No'),),
    #    initial=False,
    #)
    valid_time_formats = ['%H:%M', '%I:%M%p', '%I:%M %p']
    start_time = forms.TimeField(widget=forms.TimeInput(),
                                 input_formats=valid_time_formats)
    end_time = forms.TimeField(widget=forms.TimeInput(),
                               input_formats=valid_time_formats)

    class Meta:
        model = Activity
        exclude = ("event", "created_by", "location",
                   "social", "created", "updated")

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.get('instance')
        if not self.instance is None:
            self.event = self.instance.event
            button_name = 'update-activity'
            button_value = 'Update activity'
        else:
            assert 'event' in kwargs
            self.event = kwargs.pop('event')
            button_name = 'create-activity'
            button_value = 'Publish activity'

        super(ActivityForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()

        self.helper.layout = layout.Layout(
            layout.Div(
                layout.Div(
                    'name',
                    css_class='col-md-6'
                ),




                layout.Div(
                    layout.Div(
                    layout.HTML("""<span><input type="radio" name="type" value=""
                    class="activity_type_option" id="id_scheduled_based_activity" rect="id_agenda_based_activity_type" checked></span>"""),
                       'agenda_based_activity_type',
                       css_class='col-md-3 activity_type'
                        ),
                    layout.Div(
                    layout.HTML("""<span><input type="radio" name="type" value=""
                    class="activity_type_option" id="id_agenda_based_activity" rect="id_scheduled_based_activity_type"></span>"""),
                        'scheduled_based_activity_type',
                    css_class='col-md-3 activity_type'
                    ),
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    'description',
                    css_class='col-md-12'
                ),
                css_class='row'
            ),

            layout.Div(
                layout.Div(
                    'date',
                    css_class='col-md-12'
                ),
                css_class='row '
            ),
            layout.Div(
                layout.Div(
                    'start_time',
                    css_class='col-md-6'
                ),
                layout.Div(
                    'end_time',
                    css_class='col-md-6'
                ),
                css_class='row'
            ),
            layout.Div(
                #layout.Div(
                #    'limit',
                #    css_class='col-md-6'
                #),
                layout.Div(
                    layout.Field('facility', disabled="true"),
                    TemplateObject('events/crispy/facility-hidden-field.html'),
                    css_class='col-md-4'
                ),
                layout.Div(
                    TemplateObject('events/crispy/check-availability.html'),
                    css_class='col-md-2'
                ),

                layout.Div(

                    #layout.Hidden('seat_notification_threshold', 'true'),
                    'seat_notification_threshold',
                    css_class='col-md-6 seat_notification_threshold_hidden',
                ),
                layout.Div(
                    'host',
                    css_class='col-md-6'
                ),

                css_class='row'
            ),

            layout.Div(
                #layout.Div(
                #    'seat_notification_threshold',
                #    layout.Hidden('seat_notification_threshold', 'true'),
                #    css_class='col-md-6',

                #),
                #layout.Div(
                #    'slots',
                #    css_class='col-md-6'
                #),
                css_class='row'
            ),
            layout.Div(
                #layout.Div(
                #    'host',
                #    css_class='col-md-6'
                #),
                #layout.Div(
                #    'co_host',
                #    css_class='col-md-6'
                #),
                css_class='row'
            ),
            layout.Div(
                #layout.Div(
                #    'industry',
                #    css_class='col-md-6'
                #),
                layout.Div(
                #    layout.Div(
                #        'compatibility_score',
                #        css_class='col-md-6 range-slider5'
                #    ),
                    css_class='col-md-6'
                ),
                css_class='row'
            ),
            layout.Div(
                #layout.Div(
                #    'email_presentation',
                #    css_class='col-md-offset-6 col-md-6'
                #),
                css_class='row'
            ),
            layout.Div(
                #layout.Div(
                #    'presentation_file',
                #    css_class='col-md-offset-6 col-md-6'
                #),
                css_class='row'
            ),
            layout.Div(
                #layout.Div(
                #    'visibility',
                #    css_class='col-md-offset-6 col-md-6'
                #),
                css_class='row'
            ),
            layout.Div(
                #layout.Div(
                #    'feedback',
                #    css_class='col-md-offset-6 col-md-6'
                #),
                css_class='row'
            ),
            layout.Div(
                layout.Div(


                    layout.Submit(
                        button_name,
                        button_value,
                        css_class='btn btn-success pull-right'
                    ),


                    layout.Submit(
                        'save-activity',
                        'Save activity for edit',
                        css_class='btn btn-primary pull-right'
                    ),
                    css_class='col-md-12'
                ),
                css_class='row'
            ),
        )

    #def check_email_presentation(self, cleaned_data):
    #    email_presentation_str = cleaned_data.get('email_presentation', '')
    #    email_presentation = email_presentation_str.lower() == 'true'
    #    presentation_file = cleaned_data.get('presentation_file')

    #    if email_presentation and presentation_file is None:
    #        raise forms.ValidationError(r"""
    #        If email presentation is on. Presentation file should be sent
    #        """)

    #    if not email_presentation and not presentation_file is None:
    #        raise forms.ValidationError(r"""
    #        If email presentation is off. You can't send presentation file
    #        """)

    def check_facility(self, cleaned_data):
        facility = cleaned_data.get('facility')
        start_time = cleaned_data.get('start_time')
        end_time = cleaned_data.get('end_time')
        date = cleaned_data.get('date')

        logger.debug(date)

        if facility is None or start_time is None and end_time is None:
            return

        if not facility.is_available(date, start_time, end_time, self.instance):
            raise forms.ValidationError(
                u"Facility %s doesn't aviable for %s - %s" % (
                    unicode(facility),
                    start_time.strftime("%Y-%m-%d %H:%M"),
                    end_time.strftime("%Y-%m-%d %H:%M")
                )
            )

    def check_time(self, value, error_msg):
        if self.event.start_time is None:
            return value
        if self.event.end_time is None:
            return value

        if (
            value < self.event.start_time or
            value > self.event.end_time

        ):
            raise forms.ValidationError(
                error_msg % (
                    self.event.start_time.strftime("%Y-%m-%d"),
                    self.event.end_time.strftime("%Y-%m-%d"),
                )
            )
        return value

    def clean_date(self):
        return self.check_time(
            self.cleaned_data.get('date'),
            "Date should be not earlier %s and not later %s"
        )

    def send_email(self, user):
        host = self.cleaned_data.get('host')

        date = self.cleaned_data.get('date')
        activity = self.cleaned_data.get('name')
        event = self.event.name

        msg = EmailMultiAlternatives(
            subject="Request for hosting %s at %s" % (event, activity),

            body="Dear %s, \n I want to request you if you will be available to HOST the %s on %s."
                 "once you accept, you will be able to choose and invite your own co-host or solo host. \n"
                 "Please accept or deny the request \n"
                 "Sincerely \n"
                 "Admin \n"
                 "%s \n"
                 "%s \n"
                 "support@matchmakingsoftware.com.com \n"
                 ""% (host.user.first_name, activity, date, user.phone, user.email),
            from_email="Support Matchmaking Software <support@matchmakingsoftware.com.com>",
            to=[host.user.email],
            headers={'Reply-To': "Support Matchmaking Software <support@matchmakingsoftware.com.com>"}
            # optional extra headers
            )


        # Optional Mandrill-specific extensions:
        msg.tags = ["matchmaking", "event software"]

        # Send it:
        msg.send()

        return True

    def clean(self):
        cleaned_data = super(ActivityForm, self).clean()
        self.check_facility(cleaned_data)
        #self.check_email_presentation(cleaned_data)
        return cleaned_data


class NewUserForm(UserCreationForm):
    ''' a new user creation form which uses the basic django user model, but requires the email to be provided. '''
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    username = forms.RegexField(
        required=True, max_length=30, regex=r"^[\w.@+-]+$",
        error_messages={'invalid': "This value may contain only letters, numbers and @/./+/-/_ characters."},
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        help_text="30 characters or fewer. Letters, digits and @/./+/-/_ only")
    password1 = forms.CharField(label="Password", required=True,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label="Confirm Password", required=True,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        if commit:
            user.save()
        return user


class SearchFacilityForm(forms.Form):
    building = forms.CharField(
        max_length=30,
        label='Building',
        required=False,
        widget=forms.Select(
            # choices=zip(((None, '--------'),), BUILDING)
            choices=BUILDING
        )
    )
    theater_capacity__gte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Min"
    )
    theater_capacity__lte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Max"
    )
    classroom_capacity__gte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Min"
    )
    classroom_capacity__lte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Max"
    )
    banquet_capacity__gte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Min"
    )
    banquet_capacity__lte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Max"
    )
    sqft__gte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Min"
    )
    sqft__lte = forms.IntegerField(
        min_value=0,
        required=False,
        label="Max"
    )
    looking_date = forms.DateField(label="Select a date",
                                   help_text='This is automatically selected in "Activity day" field')

    def __init__(self, *args, **kwargs):
        super(SearchFacilityForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = layout.Layout(
            layout.Div(
                layout.Div(
                    'building',
                    css_class='col-md-12'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    layout.HTML("Theater capacity"),
                    css_class='col-md-6'
                ),
                layout.Div(
                    'theater_capacity__gte',
                    css_class='col-md-3'
                ),
                layout.Div(
                    'theater_capacity__lte',
                    css_class='col-md-3'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    layout.HTML("Classroom capacity"),
                    css_class='col-md-6'
                ),
                layout.Div(
                    'classroom_capacity__gte',
                    css_class='col-md-3'
                ),
                layout.Div(
                    'classroom_capacity__lte',
                    css_class='col-md-3'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    layout.HTML("Banquet capacity"),
                    css_class='col-md-6'
                ),
                layout.Div(
                    'banquet_capacity__gte',
                    css_class='col-md-3'
                ),
                layout.Div(
                    'banquet_capacity__lte',
                    css_class='col-md-3'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    layout.HTML("Square feet"),
                    css_class='col-md-6'
                ),
                layout.Div(
                    'sqft__gte',
                    css_class='col-md-3'
                ),
                layout.Div(
                    'sqft__lte',
                    css_class='col-md-3'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    layout.Div(
                        css_class='alert alert-danger hide',
                        css_id='dateAlert'
                    ),
                    css_class='col-md-12'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    bootstrap.AppendedText(
                        'looking_date',
                        '<i class="fa fa-calendar"></i>'
                    ),
                    css_class='col-md-12 dateinput hasDatepicker'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    layout.Button(
                        'search-facility',
                        'Search',
                        css_class='btn btn-success pull-right',
                        css_id='searchFacilityButton'
                    ),
                    css_class='col-md-12'
                ),
                css_class='row'
            )
        )
        self.helper.form_id = 'searchFacilityForm'
        self.helper.disable_csrf = True

