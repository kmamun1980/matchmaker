from django import template
register = template.Library()

@register.filter
def get_attendees_count(activity_id, attendees_list):
    attendees_count = attendees_list.filter(activity=activity_id).count()
    return attendees_count