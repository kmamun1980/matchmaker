# -*- coding: utf-8 -*-
from django.template import Library

from ..forms import SearchFacilityForm

register = Library()


@register.assignment_tag()
def get_search_facility_form():
    return SearchFacilityForm()

@register.inclusion_tag('events/tags/search_facility_modal.html')
def search_facility_modal():
    return

@register.inclusion_tag('events/tags/search_facility_js.html')
def search_facility_js():
    return