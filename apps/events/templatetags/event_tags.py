# -*- coding: utf-8 -*-
import logging

from django.template import Library

register = Library()

logger = logging.getLogger('apps.events')


@register.assignment_tag(takes_context=True)
def is_moderator(context, event):
    request = context.get('request')
    user = request.user
    return user.is_authenticated() and event.can_moderate(user)

