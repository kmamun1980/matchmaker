import datetime
import json
import logging
import requests

from django.core.urlresolvers import reverse_lazy, reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpResponseForbidden
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.conf import settings
from django.views.generic import View
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import SingleObjectMixin
from django.views.decorators.csrf import csrf_exempt
from django.views import generic as cbv
from django.template.response import TemplateResponse
from django.contrib.auth.models import Group
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives

from braces.views import (
    SuperuserRequiredMixin, LoginRequiredMixin, AjaxResponseMixin,
    JSONResponseMixin)
from crispy_forms.utils import render_crispy_form
from cities_light.models import Country, City, Region

from apps.users.models import User
from apps.users.decorators import profile_complete_required
from apps.events.forms import EventForm, NewUserForm, ActivityTypeForm
from apps.events.models import (
    Event, Attendee, Activity, ActivityAttendee, ActivityType, Facility)
from apps.events.forms import ActivityForm, SearchFacilityForm
from apps.events.emails import (
    event_approved_notification, event_published_notification)


logger = logging.getLogger('apps.events')


def dashboard(request):
    return render_to_response('events/dashboard.html', RequestContext(request))


class EventMixin(object):
    model = Event

    def get_queryset(self):
        return self.model.objects.all()


class EventListView(LoginRequiredMixin, EventMixin, cbv.ListView):
    template_name = 'events/event_list.html'


class EventDetailsView(LoginRequiredMixin, EventMixin, cbv.DetailView):
    template_name = 'events/event_details.html'


class CreateEventView(SuperuserRequiredMixin, JSONResponseMixin, cbv.FormView):
    form_class = EventForm
    template_name = ''

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.creator = self.request.user
        self.object.save()
        for user in form.cleaned_data['moderators']:
            self.object.moderators.add(user)
        return self.render_json_response({
            'status': 'created',
            'pk': self.object.pk
        })

    def form_invalid(self, form):
        return self.render_json_response({
            'status': 'error',
            'form_html': render_crispy_form(form)
        })

    def get(self, request, *args, **kwargs):
        form = self.get_form(self.get_form_class())

        return self.render_json_response({
            'status': 'new',
            'form_html': render_crispy_form(form)
        })


class EditEventView(SuperuserRequiredMixin, JSONResponseMixin,
                    EventMixin, cbv.UpdateView):
    model = Event
    form_class = EventForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.creator = self.request.user
        self.object.save()
        self.object.moderators.clear()
        for user in form.cleaned_data['moderators']:
            self.object.moderators.add(user)
        return self.render_json_response({
            'status': 'updated',
            'pk': self.object.pk
        })

    def form_invalid(self, form):
        return self.render_json_response({
            'status': 'error',
            'form_html': render_crispy_form(form)
        })

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(EditEventView, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form(self.get_form_class())

        return self.render_json_response({
            'status': 'new',
            'form_html': render_crispy_form(form)
        })


class DeleteEventView(SuperuserRequiredMixin, JSONResponseMixin, EventMixin,
                      cbv.DeleteView):
    model = Event

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return self.render_json_response({
            'status': 'deleted'
        })


@profile_complete_required
def upcoming_events_all_locations(request):
    """
    If a site supports multiple locations this page can be used to show
    events across all locations.
    """
    if request.user.is_authenticated():
        current_user = request.user
    else:
        current_user = None
    today = datetime.datetime.today()
    all_upcoming = Event.objects.upcoming(current_user=request.user)
    culled_upcoming = []
    for event in all_upcoming:
        if event.is_viewable(current_user):
            culled_upcoming.append(event)

    # show 10 events per page
    paged_upcoming = Paginator(culled_upcoming, 10)
    page = request.GET.get('page')
    try:
        events = paged_upcoming.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        events = paged_upcoming.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        events = paged_upcoming.page(Paginator.num_pages)

    return render(request, 'gather_events_list.html',
                  {"events": events, 'current_user': current_user, 'page_title': 'Upcoming Events'})


@profile_complete_required
def upcoming_events(request):
    att = Attendee.objects.filter(user=request.user)
    my_event_list = []
    for i in att:
        i = my_event_list.append(i.event.id)

    my_events = Event.objects.filter(id__in=my_event_list)
    events = Event.objects.exclude(id__in=my_event_list)

    return render_to_response('events/all_events.html', locals(), RequestContext(request))


@profile_complete_required
def user_events(request, username, location_slug):
    user = User.objects.get(username=username)
    today = timezone.now()
    events_organized_upcoming = user.events_organized.all().filter(end__gte=today).order_by('start').filter(location='')
    events_attended_upcoming = user.events_attending.all().filter(end__gte=today).order_by('start').filter(location='')
    events_organized_past = user.events_organized.all().filter(end__lt=today).order_by('-start').filter(location='')
    events_attended_past = user.events_attending.all().filter(end__lt=today).order_by('-start').filter(location='')
    return render(request, 'gather_user_events_list.html', {
        'events_organized_upcoming': events_organized_upcoming,
        'events_attended_upcoming': events_attended_upcoming,
        'events_organized_past': events_organized_past,
        'events_attended_past': events_attended_past,
        'current_user': user, 'page_title': 'Upcoming Events'
    })


@profile_complete_required
def needs_review(request, location_slug=None):
    # if user is not an event admin at this location, redirect
    location_admin_group = EventAdminGroup.objects.get(location=location)
    if not request.user.is_authenticated() or (request.user not in location_admin_group.users.all()):
        return HttpResponseRedirect('/')

    # upcoming events that are not yet live
    today = timezone.now()
    events_pending = Event.objects.filter(status=Event.PENDING).filter(end__gte=today).filter(location=location)
    events_under_discussion = Event.objects.filter(status=Event.FEEDBACK).filter(end__gte=today).filter(
        location=location)
    return render(request, 'gather_events_admin_needing_review.html',
                  {'events_pending': events_pending, 'events_under_discussion': events_under_discussion,
                   'location': location})


@profile_complete_required
def past_events(request, location_slug=None):
    if request.user.is_authenticated():
        current_user = request.user
    else:
        current_user = None
    today = datetime.datetime.today()
    # most recent first
    all_past = Event.objects.filter(start__lt=today).order_by('-start').filter(location=location)
    culled_past = []
    for event in all_past:
        if event.is_viewable(current_user):
            culled_past.append(event)
    # show 10 events per page
    paged_past = Paginator(culled_past, 10)
    page = request.GET.get('page')
    try:
        events = paged_past.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        events = paged_past.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        events = paged_past.page(Paginator.num_pages)

    return render(request, 'apps.events_events_list.html',
                  {"events": events, 'user': current_user, 'page_title': 'Past Events', 'location': location})


@profile_complete_required
def email_preferences(request, username, location_slug=None):
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')

    u = User.objects.get(username=username)
    notifications = u.event_notifications
    if request.POST.get('event_reminders') == 'on':
        notifications.reminders = True
    else:
        notifications.reminders = False

    if request.POST.get('weekly_updates') == 'on':
        notifications.weekly = True
    else:
        notifications.weekly = False
    notifications.save()
    messages.add_message(request, messages.INFO, 'Your preferences have been updated.')
    return HttpResponseRedirect('/people/%s/' % u.username)


# ###########################################
########### AJAX REQUESTS ##################


@login_required
def rsvp_event(request):
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')
    email = request.POST.get('email', None)
    phone = request.POST.get('phone', None)
    event = request.POST.get('id', None)
    event = Event.objects.get(id=int(event))

    Attendee.objects.create(user=request.user, event=event)

    return HttpResponseRedirect('/events/event/' + str(event.id) + '/')


@login_required
def rsvp_cancel(request, event_id, event_slug, location_slug=None):
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')

    user_id_str = request.POST.get('user_id')

    event = Event.objects.get(id=event_id)
    user = User.objects.get(pk=int(user_id_str))
    if user in event.organizers.all():
        user_is_organizer = True
    else:
        user_is_organizer = False

    if user in event.attendees.all():
        event.attendees.remove(user)
        event.save()
        num_attendees = event.attendees.count()
        spots_remaining = event.limit - num_attendees
        return render(request, "snippets/rsvp_info.html",
                      {"num_attendees": num_attendees, "spots_remaining": spots_remaining, "event": event,
                       'current_user': user, 'user_is_organizer': user_is_organizer, 'location': location});
    else:
        pass
    return HttpResponse(status=500);


def rsvp_new_user(request, event_id, event_slug, location_slug=None):
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')


    # get email signup info and remove from form, since we tacked this field on
    # but it's not part of the user model.
    weekly_updates = request.POST.get('email-notifications')
    if weekly_updates == 'on':
        weekly_updates = True
    else:
        weekly_updates = False


    # create new user
    form = NewUserForm(request.POST)
    if form.is_valid():
        new_user = form.save()
        new_user.save()
        notifications = new_user.event_notifications
        notifications.weekly = weekly_updates
        notifications.save()

        password = request.POST.get('password1')
        new_user = authenticate(username=new_user.username, password=password)
        login(request, new_user)
        # RSVP new user to the event
        event = Event.objects.get(id=event_id)
        event.attendees.add(new_user)

        event.save()
        messages.add_message(request, messages.INFO,
                             'Thanks! Your account has been created. Check your email for login info '
                             'and how to update your preferences.')
        return HttpResponse(status=200)
    else:
        errors = json.dumps({"errors": form.errors})
        return HttpResponse(json.dumps(errors))

    return HttpResponse(status=500);


def endorse(request, event_id, event_slug, location_slug=None):
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')

    event = Event.objects.get(id=event_id)

    endorser = request.user
    event.endorsements.add(endorser)
    event.save()
    endorsements = event.endorsements.all()
    return render(request, "snippets/endorsements.html",
                  {"endorsements": endorsements, "current_user": request.user, 'location': location, 'event': event});


def event_approve(request, event_id, event_slug, location_slug=None):
    location = ''
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')
    location_event_admin = EventAdminGroup.objects.get(location=location)
    if request.user not in location_event_admin.users.all():
        return HttpResponseRedirect('/404')

    event = Event.objects.get(id=event_id)

    event.status = Event.READY
    event.save()
    if request.user in location_event_admin.users.all():
        user_is_event_admin = True
    else:
        user_is_event_admin = False
    if request.user in event.organizers.all():
        user_is_organizer = True
    else:
        user_is_organizer = False
    msg_success = "Success! The event has been approved."

    # notify the event organizers
    event_approved_notification(event)

    return render(request, "snippets/event_status_area.html",
                  {'event': event, 'user_is_organizer': user_is_organizer, 'user_is_event_admin': user_is_event_admin})


def event_publish(request, event_id, event_slug, location_slug=None):
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')
    location = ''
    location_event_admin = EventAdminGroup.objects.get(location=location)
    if request.user not in location_event_admin.users.all():
        return HttpResponseRedirect('/404')

    event = Event.objects.get(id=event_id)

    event.status = Event.LIVE
    event.save()
    if request.user in location_event_admin.users.all():
        user_is_event_admin = True
    else:
        user_is_event_admin = False
    if request.user in event.organizers.all():
        user_is_organizer = True
    else:
        user_is_organizer = False
    msg_success = "Success! The event has been published."

    # notify the event organizers and admins
    event_published_notification(event)

    return render(request, "snippets/event_status_area.html",
                  {'event': event, 'user_is_organizer': user_is_organizer, 'user_is_event_admin': user_is_event_admin})


def new_user_email_signup(request, location_slug=None):
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')


    # create new user
    form = NewUserForm(request.POST)
    if form.is_valid():
        new_user = form.save()
        new_user.save()
        notifications = new_user.event_notifications
        notifications.weekly = True
        notifications.save()

        password = request.POST.get('password1')
        new_user = authenticate(username=new_user.username, password=password)
        login(request, new_user)
        messages.add_message(request, messages.INFO,
                             'Thanks! We\'ll send you weekly event updates. You can update your preferences at any '
                             'time on your <a href="/people/%s">profile</a> page' % new_user.username)
        return HttpResponse(status=200)
    else:
        errors = json.dumps({"errors": form.errors})
        return HttpResponse(json.dumps(errors))

    return HttpResponse(status=500);


############################################
########### EMAIL ENDPOINTS ################

@csrf_exempt
def event_message(request, location_slug=None):
    ''' new messages sent to event email addresses are posed to this view '''
    if not request.method == 'POST':
        return HttpResponseRedirect('/404')

    recipient = request.POST.get('recipient')
    from_address = request.POST.get('from')
    sender = request.POST.get('sender')

    subject = request.POST.get('subject')
    body_plain = request.POST.get('body-plain')
    body_html = request.POST.get('body-html')

    # get the event info and make sure the event exists
    # we know that the route is always in the form eventXX, where XX is the
    # event id.

    alias = recipient.split('@')[0]
    event_id = int(alias[5:])

    event = Event.objects.get(id=event_id)

    # find the event organizers and admins
    organizers = event.organizers.all()
    location = ''
    location_event_admin = EventAdminGroup.objects.get(location=location)
    admins = location_event_admin.users.all()

    # bcc list
    bcc_list = []
    for organizer in organizers:
        if organizer.email not in bcc_list:
            bcc_list.append(organizer.email)
    for admin in admins:
        if admin.email not in bcc_list:
            bcc_list.append(admin.email)


    # prefix subject
    if subject.find('[Event Discussion') < 0:
        prefix = '[Event Discussion: %s] ' % event.slug[0:30]
        subject = prefix + subject


    # add in footer
    domain = Site.objects.get_current().domain
    event_url = domain + '/events/%d/%s' % (event.id, event.slug)

    footer = '''\n\n-------------------------------------------\nYou are receving this email because you are one of the organizers or an event admin at this location. Visit this event online at %s.''' % event_url
    body_plain = body_plain + footer
    body_html = body_html + footer

    # send the message
    mailgun_api_key = settings.MAILGUN_API_KEY
    list_domain = settings.LIST_DOMAIN
    resp = requests.post(
        "https://api.mailgun.net/v2/%s/messages" % list_domain,
        auth=("api", mailgun_api_key),
        data={"from": from_address,
              "to": [recipient, ],
              "bcc": bcc_list,
              "subject": subject,
              "text": body_plain,
              "html": body_html
        }
    )

    return HttpResponse(status=200)


@profile_complete_required
def Search(request):
    form = EventForm()
    following = Event.objects.all()

    return render_to_response('events/search.html', locals(), RequestContext(request))


@profile_complete_required
def Register(request, id):
    event = Event.objects.get(id=id)
    if request.method == 'POST':
        try:
            Attendee.objects.get(user=request.user, event=event)
            return HttpResponseRedirect('/events/search/')
        except Attendee.DoesNotExist:
            buyer = request.POST.get('buyer', 2)
            Attendee.objects.create(user=request.user, event=event, attend_as=buyer)
            return HttpResponseRedirect('/profile/')

    return render_to_response('events/register.html', locals(), RequestContext(request))


@profile_complete_required
def OldNewActivity(request):
    form = ActivityForm()

    return render_to_response('events/old/new_activity.html',
                              locals(),
                              RequestContext(request))


class EventObjectMixin(LoginRequiredMixin):
    """
    Gets event by event_pk
    """
    event = None

    def get_event_queryset(self):
        return Event.objects.all()

    def get_event(self):
        if not self.event is None:
            return self.event

        try:
            self.event = self.get_event_queryset().get(
                pk=self.kwargs.get('event_pk', 0)
            )
        except Event.DoesNotExist:
            raise Http404("Event not found")
        return self.event

    def dispatch(self, *args, **kwargs):
        event = self.get_event()
        return super(EventObjectMixin, self).dispatch(*args, **kwargs)


class ActivityModeratePermission(LoginRequiredMixin):
    def dispatch(self, *args, **kwargs):
        event = self.get_event()
        if event.can_moderate(self.request.user):
            return super(ActivityModeratePermission, self).dispatch(*args,
                                                                    **kwargs)
        return HttpResponseForbidden("You can't moderate activity")


class NewActivity(LoginRequiredMixin, cbv.FormView):
    form_class = ActivityForm
    template_name = 'events/new_activity.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.created_by = self.request.user
        self.object.save()
        return redirect('activities')


class AjaxNewActivityView(EventObjectMixin,
                          ActivityModeratePermission, JSONResponseMixin,
                          cbv.FormView):
    form_class = ActivityForm

    def get_form_kwargs(self):
        kwargs = super(AjaxNewActivityView, self).get_form_kwargs()
        kwargs.update({'event': self.event})
        return kwargs

    def get_form(self, form_class):
        form = super(AjaxNewActivityView, self).get_form(form_class)
        form.helper.form_id = 'addActivityForm'
        return form

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.created_by = self.request.user
        self.object.event = self.event
        self.object.save()
        form.send_email(self.request.user)

        return self.render_json_response({
            'status': 'created'
        })

    def form_invalid(self, form):

        return self.render_json_response({
            'status': 'error',
            'form_html': render_crispy_form(form)
        })

    def get(self, request, *args, **kwargs):
        return self.render_json_response({
            'status': 'new',
            'form_html': render_crispy_form(
                self.get_form(self.get_form_class())
            )
        })


class AjaxUpdateActivityView(ActivityModeratePermission, JSONResponseMixin,
                             cbv.UpdateView):
    form_class = ActivityForm
    model = Activity

    def get_event(self):
        self.object = self.get_object()
        return self.object.event

    def get_form(self, form_class):
        form = super(AjaxUpdateActivityView, self).get_form(form_class)
        form.helper.form_id = 'updateActivityForm'
        form.helper.form_action = reverse(
            'ajax_update_activity',
            kwargs={'pk': self.get_object().pk}
        )
        return form

    def form_valid(self, form):
        event = self.get_object().event
        self.object = form.save(commit=False)
        self.object.event = event
        self.object.save()
        form.save_m2m()
        return self.render_json_response({
            'status': 'updated'
        })

    def form_invalid(self, form):
        return self.render_json_response({
            'status': 'error',
            'form_html': render_crispy_form(form)
        })

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form(self.get_form_class())
        return self.render_json_response({
            'form_html': render_crispy_form(form)
        })


class ActivitiesListView(EventObjectMixin, LoginRequiredMixin, cbv.ListView):
    model = Activity
    template_name = 'events/activities.html'

    def get_queryset(self):
        return self.model.objects.filter(
            event=self.event
        ).order_by('pk')

    def get_context_data(self, **kwargs):
        context = super(ActivitiesListView, self).get_context_data(**kwargs)
        context.update({
            'event': self.event,
            'activities_attendees': ActivityAttendee.objects.all()
        })
        return context


@profile_complete_required
def ActivitiesReports(request):
    return render_to_response('events/activities_reports.html', locals(), RequestContext(request))


@profile_complete_required
def ActivitiesList(request):
    activities = Activity.objects.all().order_by('id')
    activities_attendees = ActivityAttendee.objects.filter(user=request.user).values_list('activity', flat=True)

    return render_to_response('events/activities_list.html', locals(), RequestContext(request))


@profile_complete_required
def RegisterActivity(request, id):
    activity = Activity.objects.get(id=id)

    if ActivityAttendee.objects.filter(activity=activity).count() > activity.seat_notification_threshold:
        ActivityAttendee.objects.get_or_create(user=request.user, activity=activity, excess_user=True)

        msg = EmailMultiAlternatives(
            subject="Event registration",
            body="This to notify you that the activity registration has exceeded the limit, we have reserved your "
            "registration in case a bigger room is found you will be notified",
            from_email="Support Matchmaking Software <support@matchmakingsoftware.com.com>",
            to=[request.user.email],
            headers={'Reply-To': "Support Matchmaking Software <support@matchmakingsoftware.com.com>"}
            # optional extra headers
        )

        # Optional Mandrill-specific extensions:
        msg.tags = ["matchmaking", "event software"]
        msg.metadata = {'user_id': request.user.id}

        # Send it:
        msg.send()

    else:
        ActivityAttendee.objects.get_or_create(user=request.user, activity=activity)

    return HttpResponse(json.dumps({'success': 'true', 'id': id}))


@profile_complete_required
def DeleteActivity(request, id):
    Activity.objects.get(id=id).delete()

    return HttpResponse(json.dumps({'success': 'true', 'id': id}))


@profile_complete_required
def EditActivity(request, id):
    activity = None
    try:
        activity = Activity.objects.get(id=id)
    except:
        raise Http404
    form = ActivityForm(request.POST, instance=activity)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('activities'))


# Following code Added by Kulbir


class ActivityTypeCreateView(SuperuserRequiredMixin, CreateView):
    model = ActivityType
    form_class = ActivityTypeForm
    success_url = reverse_lazy('event_type_list')
    template_name = 'events/activitytype_form.html'

    def form_valid(self, form):
        form.instance.creator = self.request.user
        return super(ActivityTypeCreateView, self).form_valid(form)


class ActivityTypeListView(SuperuserRequiredMixin, ListView):
    model = ActivityType
    template_name = 'events/activitytype_list.html'
    context_object_name = 'activity_types'


class ManageRoleView(SuperuserRequiredMixin, View):
    template_name = 'events/manage_role_list.html'

    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, self.template_name,
                                self.get_context_data())

    def get_context_data(self):
        context = {
            'activities': ActivityType.objects.all(),
            'groups': Group.objects.all()
        }
        return context


class SearchFacilityView(JSONResponseMixin, AjaxResponseMixin,
                         LoginRequiredMixin, cbv.View):
    def get_ajax(self, request, *args, **kwargs):
        form = SearchFacilityForm(request.GET)
        if form.is_valid():
            search_data = {
                key: value
                for key, value in form.cleaned_data.iteritems()
                if key != 'looking_date' and value
            }

            logger.debug(search_data)
            looking_date_str = form.cleaned_data['looking_date'].strftime(
                '%Y-%m-%d'
            )
            return self.render_json_response({
                'status': 'ok',
                'facility_list': [
                    {
                        'id': x.id,
                        'name': unicode(x),
                        'available_url': reverse(
                            'facility-available',
                            kwargs={
                                'pk': x.pk,
                                'looking_date': looking_date_str
                            }
                        )
                    }
                    for x in Facility.objects.filter(**search_data)
                ],
                'form_html': render_crispy_form(form)
            })
        else:
            return self.render_json_response({
                'status': 'error',
                'form_html': render_crispy_form(form)
            })


class FacilityAvailableTimeView(JSONResponseMixin, AjaxResponseMixin,
                                LoginRequiredMixin, SingleObjectMixin,
                                cbv.View):
    model = Facility

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        try:
            looking_date = timezone.localtime(
                datetime.datetime.strptime(
                    kwargs['looking_date'],
                    "%Y-%m-%d"
                ).replace(
                    tzinfo=timezone.utc
                )
            )
        except ValueError:
            return self.render_json_response({
                'status': 'error',
                'value': 'wrong date'
            })

        activity_pk = request.GET.get("activity_pk", 0)
        logger.debug(activity_pk)

        try:
            activity = Activity.objects.get(pk=activity_pk)
        except Activity.DoesNotExist:
            activity = None

        time_periods = (
            (
                datetime.time(x / 2, (x % 2) * 30),
                datetime.time(x / 2, (x % 2) * 30 + 29)
            )
            for x in xrange(48)
        )

        return self.render_json_response(
            {
                'status': 'ok',
                'availability_list': [
                    {
                        'number': num,
                        'period': '%s - %s' % (
                            period[0].strftime("%H:%M"),
                            period[1].strftime("%H:%M")
                        ),
                        'is_available': self.object.is_available(
                            looking_date,
                            *period,
                            exclude_activity=activity
                        ),
                        'is_selected': (
                            activity and
                            activity.date == looking_date.date() and
                            (
                                period[0] <= activity.start_time <= period[1] or
                                activity.start_time <= period[0] and
                                activity.end_time >= period[1] or
                                period[0] <= activity.end_time <= period[1]
                            )
                        )
                    } for num, period in enumerate(time_periods)
                ]
            }
        )
