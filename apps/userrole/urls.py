from django.conf.urls import patterns, include, url

urlpatterns = patterns('',


    url(r'^add-role/$', 'apps.userrole.views.add_role', name='add-role'),
    url(r'^add-activity-type/$', 'apps.userrole.views.add_activity_type', name='add-activity'),
    url(r'^edit-role/$', 'apps.userrole.views.edit_role', name='edit-role'),
    url(r'^delete-role/$', 'apps.userrole.views.delete_role', name='delete-role'),
    url(r'^roles/$', 'apps.userrole.views.roles', name='roles'),

    )


