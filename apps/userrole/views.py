from django.shortcuts import render_to_response,RequestContext
from models import Role, ActivityType
from django.http import HttpResponseRedirect

def roles(request):
    roles = Role.objects.all()

    return render_to_response('manage_roles.html',locals(),RequestContext(request))

def add_role(request):
    if request.method == 'POST':
        name = request.POST.get('name',None)
        activity = request.POST.getlist('activity', None)
        profile = request.POST.get('profile',None)
        if profile:
            profile = True
        else:
            profile = False
        role = Role.objects.create(name= name,profile=profile)
        if activity:
            for i in activity:
                act = ActivityType.objects.get(id=i)
                role.activity.add(act)
        role.save()
        return HttpResponseRedirect('/roles/')


    activities = ActivityType.objects.all()
    return render_to_response('add_role.html',locals(),RequestContext(request))

def add_activity_type(request):
    if request.method == 'POST':
        name = request.POST.get('name',None)
        description = request.POST.get('description',None)

        activity = ActivityType.objects.create(full_name= name,creator=request.user,description=description)

        return HttpResponseRedirect('/roles/')
    return render_to_response('add-activity_type.html',locals(),RequestContext(request))


def edit_role(request):
    return render_to_response('edit_role.html',RequestContext(request))

def delete_role(request,r_id):

    return HttpResponseRedirect('/roles/')



