from django.db import models
from django.contrib.auth.models import  Group
from apps.events.models import ActivityType


class Role(models.Model):

    """
    This model associates a group with a role
    Allows for easy retrieval of Group (Role Names) -
    this uses a code name to grant all three permissions to add,change, delete
    """
    name = models.CharField(max_length=255,null=True,blank=True)
    activity = models.ManyToManyField(ActivityType,null=True,blank=True)
    profile = models.BooleanField(default=False)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u"%s" % self.name


