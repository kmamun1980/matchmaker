from django.contrib import admin
from apps.match.models import Session,Schedule
# Register your models here.
admin.site.register(Schedule)
admin.site.register(Session)