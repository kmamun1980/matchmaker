from django.db import models
from apps.users.models import User
from apps.events.models import Activity
import pickle
from django.core.files.base import ContentFile

class Schedule(models.Model):
    creator = models.ForeignKey(User)
    activity = models.ForeignKey(Activity)
    name = models.CharField(max_length=255,blank=True,null=True,verbose_name='Name of Schedule')
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    schedule_data = models.FileField(upload_to='schedules/',blank=True,null=True)

    def get_data(self):
        f = self.schedule_data
        f.open(mode="rb")
        data = pickle.load(f)
        f.close()
        return data        

    def put_data(self, data):
        content = pickle.dumps(data)
        filename = str(self.id) + ".pickle"
        self.schedule_data.save(filename, ContentFile(content))        

    def update_data(self,data):
        fname = self.schedule_data.file.name
        f = open(fname, "wb")
        pickle.dump(data,f)
        f.close()

    def __unicode__(self):
        return self.name

class Session(models.Model):
    schedule = models.ForeignKey(Schedule)
    seller = models.CharField(max_length=255,blank=True,null=True)
    buyer  = models.CharField(max_length=255,blank=True,null=True)
    start  = models.CharField(max_length=255,blank=True,null=True)
    end   =models.CharField(max_length=255,blank=True,null=True)

    def __unicode__(self):
        return self.seller+':'+self.buyer

class Rank(models.Model):
    calculated_at = models.DateTimeField(auto_now=True)
    rank = models.FileField(upload_to='ranks/',blank=True,null=True)


