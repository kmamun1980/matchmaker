from django.core.management.base import BaseCommand, CommandError
from apps.match.views import update_ranks

class Command(BaseCommand):
    help = 'Updates user rankings'

    def handle(self, *args, **options):
        update_ranks()
