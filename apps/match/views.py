import sys
import random
import datetime
import time

import math
import json
import string

from django.shortcuts import render_to_response,RequestContext
from django.http import HttpResponse

from apps.users.models import User
from models import Schedule,Session,Rank
from apps.events import models as events_models
from django.core.files.base import ContentFile
import pickle
import traceback

def match(request):

        schedule = Schedule.objects.create(creator= request.user,name ='Schedule')


        #VISITOR = SELLER
        #PEOPLE = BUYERS
        import sys
        import random
        import datetime
        from datetime import timedelta

        startTime = datetime.datetime.now()
        print startTime.strftime("%Y-%m-%A %I:%M%p")

        start = datetime.datetime(2014,01,01,10)
        timeslots = []
        for x in range(101):
            x= start
            y = x.strftime('%Y-%m-%d %H:%M')

            timeslots.append(y)
            start+=datetime.timedelta(minutes=10)


        HOURS = timeslots
        PEOPLE_IDS =  [user.username for user in User.objects.filter(vgp_type=2)]
        VISITOR_IDS = [user.username for user in User.objects.filter(vgp_type=1)]

        VISITOR_PEOPLE = {}

        for i in VISITOR_IDS:
            VISITOR_PEOPLE[i]= random.sample(PEOPLE_IDS,50)


        class Person:
            def __init__(self, id):
                self.id = id
                self.schedule = [False]*100  # False = free, True = busy
            def scheduleTime(self):
                # schedules next available hour and returns that hour
                for i in range(len(self.schedule)):
                    if not self.schedule[i]:
                        self.schedule[i] = True
                        return HOURS[i]
                return 'unavailable'
            def unscheduleTime(self, index):
                self.schedule[index] = False

        class Visitor:
            def __init__(self, id, people_requests, people):
                self.id = id
                self.schedule = {} # {person_id: hour}
                for p in people_requests:
                    bad_times = set()  # times that Visitor is busy
                    time = people[p].scheduleTime()
                    while time in self.schedule.values():
                    # keep scheduling a time until you get one that works for both the Visitor and Person
                        bad_times.add(time)
                        time = people[p].scheduleTime()
                    self.schedule[p] = time
                    for t in bad_times:  # unschedule bad_times from Person
                        people[p].unscheduleTime(HOURS.index(t))
            def printSchedule(self):
                #text_file = open("Output.txt", "a")
                #text_file.write('Schedule for %s [Person (time)]:\n' % self.id)

                print 'Creating schedule for'+self.id


                    #print 'Schedule for %s [Person (time)]: ' % self.id
                for p,t in self.schedule.items():
                    #print '    %s (%s)' % (p,t)
                    Session.objects.create(seller=self.id,buyer = p,start=t,schedule=schedule)
                    #text_file.write('    %s (%s) \n' % (p,t))



                #text_file.close()
        x = datetime.datetime.now()-startTime
        print	datetime.datetime.now().strftime("%Y-%m-%A %I:%M%p")

        def run():
            people = {}
            for id in PEOPLE_IDS:
                people[id] = Person(id)
            visitors = {}
            for id in VISITOR_IDS:
                visitors[id] = Visitor(id, VISITOR_PEOPLE[id], people)
            for v in visitors.values():
                v.printSchedule()




        return render_to_response('match/schedule.html',RequestContext(request))


def EventSchedule(request,id):

    schedule = Schedule.objects.get(id=id)

    users = User.objects.all()

    com_users = users.filter(vgp_type = 1).count()
    gov_users = users.filter(vgp_type = 2).count()
    user_count = users.count()

    user_dict = {}

    for user in users:
        user_dict[user.username] = user



    sessions = Session.objects.filter(schedule=schedule)
    total_sessions = sessions.count()
    session_length = 10
    session_break = 0
    meetings = []

    for s in sessions:

        start = reg_start = datetime.datetime.strptime(s.start, '%Y-%m-%d %H:%M')
        end = start+datetime.timedelta(minutes=10)
        meetings.append({'seller':user_dict[s.seller],
                         'buyer':user_dict[s.buyer],'start':start,'codes':user_dict[s.buyer].naics.all(),'end':end})



    return render_to_response('schedule.html',locals(),RequestContext(request))


def ListSchedule(request):
    schedules = Schedule.objects.all()
    return render_to_response('schedule_list.html',locals(),RequestContext(request))

def create_schedule(request):
    activities = events_models.Activity.objects.all().select_related()
    params = {'activities' : activities}
    act_id = request.REQUEST.get('activity_id',None)
    if act_id is not None:
        act = events_models.Activity.objects.get(id=act_id)
        schedules = Schedule.objects.filter(activity=act).order_by('-id')
        if len(schedules) >0 : 
            schedule = schedules[0]
            schedule_calculated = schedule.get_data()
        else:
            schedule = Schedule()
            schedule.activity = act
            schedule.creator = request.user
            schedule.name = "Created Schedule"
            schedule.save()
            schedule_calculated = create_schedule_from_params(request)
            schedule.put_data(schedule_calculated)
        params['schedule_calculated'] = schedule_calculated
        params['schedule_id'] = schedule.id
    return render_to_response('schedule.html', params, RequestContext(request))
        
def create_schedule_from_params(request):
    total_tables = int(request.REQUEST.get('total_tables'))
    seats_per_table = int(request.REQUEST.get('seats_per_table',2)) + 1 #(one buyer also seat)
    meeting_duration = int(request.REQUEST.get('meeting_duration'))*60 #from form we get in minutes
    interval_between_meetings = int(request.REQUEST.get('interval_between_meetings'))*60
    lunch_duration = int(request.REQUEST.get('lunch_duration',30))*60                       
    activity_id = int(request.REQUEST.get('activity_id'))

    activity = events_models.Activity.objects.get(id=activity_id)

    ast = activity.start_time
    aet = activity.end_time

    start_time_seconds = ast.hour*60*60 + ast.minute*60 + ast.second
    end_time_seconds = aet.hour*60*60 + aet.minute*60 + aet.second
    act_dur = end_time_seconds - start_time_seconds


    activity_schedule = [
      {'start_time' : start_time_seconds, 'duration' : act_dur }
    ]

    buyers = events_models.Attendee.objects.filter(event=activity.event, attend_as='1').exclude(user__is_superuser=True)
    sellers = events_models.Attendee.objects.filter(event=activity.event, attend_as='2').exclude(user__is_superuser=True)

    p = {
     'buyers_total' : len(buyers),                          # buyers total
     'sellers_total' : len(sellers),                         # total number of sellers
     'seats_per_table' : seats_per_table,                        # total number of people per table, i.e. 1 buyer + several suppliers
     'meeting_duration' : meeting_duration,                   # one meeting session duration in seconds
     'interval_between_meetings' : interval_between_meetings,           # coffee break duration in seconds
     'lunch_duration' : lunch_duration,                     # lunch duration in seconds
     'activity_schedule' : activity_schedule,      # total activity duration during one day in seconds
     'total_tables' : total_tables,                            # we assume tables are numbered 0... N-1
     'min_rank_to_match' : 0                  #minimal rank for a supplier to be matched with the buyer
    }

    buyers_ids =  [(x.user.id, x.user.full_name()) for x in buyers]
    sellers_ids = [(x.user.id, x.user.full_name()) for x in sellers]
    sellers_dict = {}

    for s in sellers:
        sellers_dict[s.user.id] = {'id': s.user.id, 'full_name': s.user.full_name()}

    #buyers_ids = random.sample(xrange(1, p['buyers_total']*2), p['buyers_total'])
    #sellers_ids = random.sample(xrange(p['buyers_total']*3, p['buyers_total']*3+p['sellers_total']*2), p['sellers_total'])
    #ranks = create_ranks(buyers_ids, sellers_ids)

    rank_object = Rank.objects.all().order_by('-id')[0]
    f = rank_object.rank
    f.open(mode='rb')
    lines = f.readlines()
    ranks = json.loads(lines[0])
    f.close()

    p['buyers_ids'] = buyers_ids
    p['sellers_ids'] = sellers_ids
    p['sellers_dict'] = sellers_dict
    p['ranks'] = ranks

    meeting_grid = create_meeting_grid(p)
    p['meeting_grid'] = meeting_grid
    schedule_calculated = create_schedule_algorithm(p)

    return schedule_calculated
   
def create_schedule_algorithm(s):
    total_tables = s['total_tables']
    meeting_grid = s['meeting_grid']
    seats_per_table = s['seats_per_table']
    total_seats_for_supplier = s['total_tables']*(s['seats_per_table']-1)
    ranks = s['ranks']
    min_rank_to_match = s['min_rank_to_match']
    buyer_meeting_index = {}
    buyers_ids = s['buyers_ids']
    sellers_ids = s['sellers_ids']
    sellers_dict = s['sellers_dict']
    meeting_idx = 0

    for m in meeting_grid:
        if m['kind'] == 'meeting':
            buyers_ids_now = buyers_ids[0:total_tables]
            m['meetings'] = {}
            supplier_meeting_index = {}
            table = 0
            for buyer in buyers_ids_now:
                b = buyer[0]
                b_name = buyer[1]
                try:
                    ranks[str(b)]

                    if not buyer_meeting_index.has_key(b):
                        buyer_meeting_index[b] = {}

                    m['meetings'][table] = {}
                    m['meetings'][table]['table'] = table
                    m['meetings'][table]['buyer_id'] = b
                    m['meetings'][table]['buyer_name'] = b_name
                    m['meetings'][table]['suppliers'] = []
                    m['meetings'][table]['id'] = meeting_idx
                    meeting_idx += 1

                    for sup in ranks[str(b)]:
                        sup_id = sup[0]
                        sup_rank = sup[1]
                        if sup_rank >= min_rank_to_match:
                            if not supplier_meeting_index.has_key(sup_id):  #this supplier was not scheduled in this session
                                if not buyer_meeting_index[b].has_key(sup_id): #this buyer and supplier never met before
                                    m['meetings'][table]['suppliers'].append({
                                      'id': sup_id,
                                      'full_name' : sellers_dict[sup_id]['full_name'],
                                      'r' : sup_rank
                                    })
                                    buyer_meeting_index[b][sup_id] = 1
                                    supplier_meeting_index[sup_id] = 1
                                    if len(m['meetings'][table]['suppliers']) == seats_per_table -1: #table completed
                                        break
                    table += 1
                except:
                    pass    
            buyers_ids = buyers_ids[total_tables:] + buyers_ids[0:total_tables] #round-robin buyers after session
    return meeting_grid

#random ranking generator
def create_ranks(b_ids,s_ids):
    r = {}
    for b in b_ids:
        r[b] = {}
        for s in s_ids:
            r[b][s] = random.randint(0,100)
        r[b] = sorted(r[b].items(), key=lambda t:100-t[1])
    return r

def update_ranks():
    sellers = User.objects.filter(vgp_type='2').select_related()
    buyers = User.objects.filter(vgp_type='1').select_related()
    r = {}
    s_naics = {}
    for b in buyers:
        b_naics = set([c.code for c in b.naics.all()])
        r[b.id] = {}
        for s in sellers:
            try:
                len(s_naics[s.id])
            except:
                s_naics[s.id] = set([c.code for c in s.naics.all()])
            if len(b_naics)>0 and b.id != s.id: s_naics[s.id] = set([c.code for c in s.naics.all()])
            if len(b_naics)>0 and b.id != s.id:
                r[b.id][s.id] = { 
                   'rank' : int( 100* float(len(b_naics.intersection(s_naics[s.id])))/float(len(b_naics))),
                   'intersected' : tuple( b_naics.intersection(s_naics[s.id]) )
                }
            else:
                r[b.id][s.id] = {
                   'rank' : 0,
                   'intersected' : ()
                } 
        r[b.id] = sorted(r[b.id].items(), key = lambda t: 100-t[1]['rank'])

    rank = Rank()
    rank.save()
    content = json.dumps(r)
    filename = ''.join(random.choice(string.lowercase) for i in range(6))+".json"
    rank.rank.save(filename, ContentFile(content))
    return r

def hours_min_sec(seconds):
    hours = math.floor(seconds/3600.0)
    minutes = math.floor((seconds - 3600*hours)/60.0)
    sec = seconds - 60*minutes - 3600*hours
    return (int(hours), int(minutes), int(sec))

def create_meeting_grid(s):
    grid = []
    meeting_slot = s['meeting_duration'] + s['interval_between_meetings']
    current_day = 0
    session_idx = 1
    total_session_idx = 0
    for ac in s['activity_schedule']:
        time_for_meetings = ac['duration'] - s['lunch_duration']
        number_of_meetings = int(math.floor(time_for_meetings/meeting_slot))
        number_of_meetings_before_lunch = int(math.floor(number_of_meetings/2))
        number_of_meetings_after_lunch = number_of_meetings - number_of_meetings_before_lunch
        current_time = ac['start_time'] + 24*60*60*current_day
        for m in xrange(0,number_of_meetings_before_lunch):
            daily_t_sec = current_time - 24*60*60*current_day
            (dh,dm,ds) = hours_min_sec(daily_t_sec)
            (dhe,dme,dse) = hours_min_sec(daily_t_sec + meeting_slot - s['interval_between_meetings'])
            grid.append({
              'day' : current_day,
              'current_time' : current_time,
              'daily_time' : daily_t_sec,
              'kind' : 'meeting',
              'ts' : datetime.time(dh,dm,ds),
              'ts_end' : datetime.time(dhe,dme,dse),
              'session' : session_idx,
              'total_session_idx' : total_session_idx
            })
            current_time += meeting_slot
            session_idx += 1
            total_session_idx += 1
        current_time = current_time - s['interval_between_meetings']
        (dh,dm,ds) = hours_min_sec(current_time - 24*60*60*current_day)
        (dhe,dme,dse) = hours_min_sec(current_time + meeting_slot - 24*60*60*current_day)
        grid.append({
              'day' : current_day,
              'current_time' : current_time,
              'daily_time' : current_time - 24*60*60*current_day,
              'kind' : 'lunch',
              'ts' : datetime.time(dh,dm,ds),
              'ts_end' : datetime.time(dhe,dme,dse),
              'session' : 'lunch',
              'total_session_idx' : total_session_idx
        })
        total_session_idx += 1
        current_time = current_time + s['lunch_duration']
        for m in xrange(0,number_of_meetings_after_lunch):
            daily_t_sec = current_time - 24*60*60*current_day
            (dh,dm,ds) = hours_min_sec(daily_t_sec)
            (dhe,dme,dse) = hours_min_sec(daily_t_sec + meeting_slot - s['interval_between_meetings'])
            grid.append({
              'day' : current_day,
              'current_time' : current_time,
              'daily_time' : daily_t_sec,
              'kind' : 'meeting',
              'ts' : datetime.time(dh,dm,ds),
              'ts_end' : datetime.time(dhe,dme,dse),
              'session' : session_idx, 
              'total_session_idx' : total_session_idx
            })
            current_time += meeting_slot
            session_idx += 1
            total_session_idx += 1
        current_day += 1
    return grid

def get_schedule_meeting_details(request):
    schedule_id = int(request.REQUEST.get('schedule_id',0))
    total_session_idx = int(request.REQUEST.get('t_s_idx',-1))
    table_id = int(request.REQUEST.get('table_id',-1))
    remove_id = int(request.REQUEST.get('remove_id',-1))
    add_id = int(request.REQUEST.get('add_id',-1))
    try:
        s = Schedule.objects.get(id=schedule_id)
        data = s.get_data()
        session = data[total_session_idx]
        meeting = session['meetings'][table_id]
        
        if remove_id > 0:
            for ms in meeting['suppliers']:
                if ms['id'] == remove_id:
                    meeting['suppliers'].remove(ms)
                    s.put_data(data)
        if add_id >0:
            met = False
            for ms in meeting['suppliers']:
                if ms['id'] == add_id:
                    met = True
            if not met:
                user = User.objects.get(id=add_id)
                buyer_id = meeting['buyer_id']
                b = User.objects.get(id=buyer_id)
                b_naics = set([c.code for c in b.naics.all()])
                s_naics = set([c.code for c in user.naics.all()])
            
                if len(b_naics)>0:
                    r = {
                       'rank' : int( 100* float(len(b_naics.intersection(s_naics)))/float(len(b_naics))),
                       'intersected' : tuple( b_naics.intersection(s_naics) )
                    }
                else:
                    r = {
                       'rank' : 0,
                       'intersected' : ()
                    }
                meeting['suppliers'].append({
                   'id': user.id,
                   'full_name' : user.full_name(),
                   'r' : r
                })
                s.put_data(data)
        params = {
          'total_session_idx' : total_session_idx,
          'schedule_id' : schedule_id,
          'table_id' : table_id,
          'suppliers' : meeting['suppliers']
        }
    except Exception,ex:
        print traceback.format_exc()
        params = {}
    return HttpResponse(json.dumps(params), content_type="application/json")   
