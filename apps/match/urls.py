from django.conf.urls import patterns, include, url

urlpatterns = patterns('',

    url(r'^schedule/$', 'apps.match.views.match', name='match'),
    url(r'^schedule-list/$', 'apps.match.views.ListSchedule', name='match-list'),
    url(r'^myschedule/(?P<id>\d+)/$', 'apps.match.views.EventSchedule', name='matches'),
    url(r'^create-schedule/$', 'apps.match.views.create_schedule', name='new-match'),
    url(r'^schedule_meeting_details/$', 'apps.match.views.get_schedule_meeting_details', name='schedule_meeting_details'),
)

