from django.views.generic import TemplateView

from apps.events.models import Event


class HomeView(TemplateView):
    template_name = 'flatpages/home.html'

    def get_context_data(self, **kwargs):
        events = Event.objects.order_by('-id')[:5]
        return {
            'events': events,
            'details': events[0] if events else None
        }
