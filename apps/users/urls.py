from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .views.views import (
    GroupListView, GroupCreateView, RegisterView, LoginView, RegisterTypeView,
    PublicProfile)
from .views.profiles import (
    FillGovView, FillCompany, EditPersonalProfile, CreateUserCertificate,
    DeleteUserCertificate, ResetUsersPassword)
from apps.users.decorators import profile_complete_required

urlpatterns = patterns(
    '',
    url(r'^login/$', LoginView.as_view(template_name='pages-signin.html'), name='user_login'),
    url(r'^logout/$', 'apps.users.views.views.do_logout', name='logout'),

    url(r'^password/reset/$', 'django.contrib.auth.views.password_reset',
        {'post_reset_redirect': '/password/reset/done', 'is_admin_site': True}, name='reset_password'),
    url(r'^password/reset/done', 'django.contrib.auth.views.password_reset_done',
        {'template_name': 'password_reset/reset-done.html'}),
    url(r'^password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect': '/accounts/password/complete', 'template_name': 'password_reset/reset-password.html'},
        name='password_reset_confirm'),
    url(r'^accounts/password/complete$', 'django.contrib.auth.views.password_reset_complete',
        {'template_name': 'password_reset/reset-complete.html'}),

    url(r'^register/$', RegisterView.as_view(template_name='pages-signup.html'), name='register'),
    url(r'^verified/$', 'apps.users.views.views.ActivateSuccess', name='activate_success'),
    url(r'^verify-code/$', 'apps.users.views.views.VerifyCode', name='verify_code'),
    url(r'^phone-verified/$', 'apps.users.views.views.VerifySuccess', name='verify_success'),
    url(r'^registration-successful/$', 'apps.users.views.views.RegSuccess', name='reg_success'),
    url(r'^register-type/$',
        login_required(RegisterTypeView.as_view(template_name='users/register-type.html')),
        name='register_type'),
    url(r'^fill-company/$',
        login_required(FillCompany.as_view(template_name='profile/fill_company.html')),
        name='fill_company'),
    url(r'^settings/$', 'apps.users.views.views.user_settings', name='users.settings'),
    url(r'^user/confirm/(?P<activation_key>\w{1,50})/$', 'apps.users.views.views.ActivateAccount', 
        name='users_confirm'),
    url(r'^forgot-password/$', 'apps.users.views.views.ResetPass', name='reset_pass'),
    url(r'^reset-password-email/$', 'apps.users.views.views.ResetSuccess', name='reset_success'),
    url(r'^reset/(?P<activation_key>\w+)/$', 'apps.users.views.views.NewPass', name='new_password'),
    url(r'^all-events/$', 'apps.users.views.views.EventsAll', name='events_all'),
    url(r'^opportunities/$', 'apps.users.views.views.Opportunity', name='opp_all'),
    url(r'^groups/$', 'apps.users.views.views.group', name='group_all'),
    url(r'^profile/$', 'apps.users.views.views.Profile', name='profile'),
    url(r'^reset-successful/$', 'apps.users.views.views.ResetSuccess', name='reset_success'),
    url(r'^edit-personal-profile/$',
        login_required(EditPersonalProfile.as_view()),
        name='edit_personal_profile'),
    url(r'^create-individual-certificate/$', CreateUserCertificate.as_view(), 
        name='create_individual_certificate'),
    url(r'^delete-individual-certificate/$', DeleteUserCertificate.as_view(), 
        name='delete_individual_certificate'),

    url(r'^user/(?P<username>[-\w]+)/$',
        profile_complete_required(PublicProfile.as_view()), name='public_profile'),
    url(r'^connections/$', 'apps.users.views.views.Connections', name='connections'),

    url(r'^connect/(?P<id>\d+)/$', 'apps.users.views.views.connect', name='connect'),
    #url(r'^edit-profile-fields/$', 'apps.users.views.views.DProfile', name='dynamic_profile'),
    url(r'^profile-fields/$', 'apps.users.views.profiles.ProfileFields', 
        name='dynamic_field_get'),
    url(r'^add-profile-field/$', 'apps.users.views.profiles.CreateProfileField', 
        name='dynamic_field_add'),
    url(r'^edit-profile-field/(?P<id>\d+)/$', 'apps.users.views.profiles.EditProfileField', 
        name='dynamic_field_edit'),
    url(r'^delete-profile-field/(?P<id>\d+)/$', 'apps.users.views.profiles.DeleteProfileField', 
        name='dynamic_field_delete'),
    url(r'^all-users/$', 'apps.users.views.profiles.all_users', name='all_users'),
    url(r'^fill-organization/$','apps.users.views.profiles.FillOrganization', 
        name='fill_organization'),

    url(r'^choose-department/$', FillGovView.as_view(template_name='profile/fill_gov_org.html'), name='fill_gov'),
    url(r'^admin-reset-user-password/$', ResetUsersPassword.as_view(), name='admin_reset_user_password'),
    url(r'^dashboard/$', 'apps.reports.views.BusinessList', name='dashboard'),


    #url(r'^create-profile/$', 'apps.users.views.CreateProfile', name='create_profile'),

    url(r'^create-pdmfunction/$', 'apps.users.views.views.CreatePDMFunction', 
        name='create_pdmfunction'),
    url(r'^edit-pdmfunction/(?P<id>\d+)/$', 'apps.users.views.views.EditPDMFunction', 
        name='edit_pdmfunction'),

    url(r'^create-pdmprofile/$', 'apps.users.views.views.CreateBusCategory', 
        name='create_pdmprofile'),
    url(r'^edit-pdmprofile/(?P<id>\d+)/$', 'apps.users.views.views.EditBusCategory', 
        name='edit_pdmprofile'),

    url(r'^create-buscategory/$', 'apps.users.views.views.CreateBusCategory', 
        name='create_buscategory'),
    url(r'^edit-buscategory/(?P<id>\d+)/$', 'apps.users.views.views.EditBusCategory', 
        name='edit_buscategory'),

    url(r'^create-office/$', 'apps.users.views.views.CreateOffice', 
        name='create_office'),
    url(r'^edit-office/(?P<id>\d+)/$', 'apps.users.views.views.EditOffice', 
        name='edit_office'),

    url(r'^create-psccode/$', 'apps.users.views.views.CreatePSCCode', 
        name='create_psccode'),
    url(r'^edit-psccode/(?P<id>\d+)/$', 'apps.users.views.views.EditPSCCode', 
        name='edit_psccode'),

    url(r'^create-organisation/$', 'apps.users.views.views.CreateOrganisation', 
        name='create_organisation'),
    url(r'^edit-organisation/(?P<id>\d+)/$', 'apps.users.views.views.EditOrganisation', 
        name='edit_organisation'),

    url(r'^create-orgcategory/$', 'apps.users.views.views.CreateOrgCategory', 
        name='create_orgcategory'),
    url(r'^edit-orgcategory/(?P<id>\d+)/$', 'apps.users.views.views.EditOrgCategory', 
        name='edit_orgcategory'),

    url(r'^create-agency/$', 'apps.users.views.views.CreateAgency', 
        name='create_agency'),
    url(r'^edit-agency/(?P<id>\d+)/$', 'apps.users.views.views.EditAgency', 
        name='edit_agency'),


    url(r'^create-department/$', 'apps.users.views.views.CreateDepartment', 
        name='create_department'),
    url(r'^edit-department/(?P<id>\d+)/$', 'apps.users.views.views.EditDepartment', 
        name='edit_department'),

    url(r'^create-vosbexperience/$', 'apps.users.views.views.CreateVOSBExperience', 
        name='create_vosbexperience'),
    url(r'^edit-vosbexperience/(?P<id>\d+)/$', 'apps.users.views.views.EditVOSBExperience', 
        name='edit_vosbexperience'),


    url(r'^create-businesscert/$', 'apps.users.views.views.CreateBusinessCert', 
        name='create_businesscert'),
    url(r'^edit-businesscert/(?P<id>\d+)/$', 'apps.users.views.views.EditBusinessCert', 
        name='edit_businesscert'),


    url(r'^create-vosb/$', 'apps.users.views.views.CreateVOSB', name='create_vosb'),
    url(r'^edit-vosb/(?P<id>\d+)/$', 'apps.users.views.views.EditVOSB', name='edit_vosb'),

    url(r'^create-individualcert/$', 'apps.users.views.views.CreateIndividualCert', 
        name='create_individualcert'),
    url(r'^edit-individualcert/(?P<id>\d+)/$', 'apps.users.views.views.EditIndividualCert', 
        name='edit_individualcert'),

    url(r'^create-hostregistration/$', 'apps.users.views.views.CreateHostRegistration', 
        name='create_hostregistration'),
    url(r'^edit-hostregistration/(?P<id>\d+)/$', 'apps.users.views.views.EditHostRegistration', 
        name='edit_hostregistration'),

    url(r'^create-gpartneringneed/$', 'apps.users.views.views.CreateGPartneringNeed', 
        name='create_gpartneringneed'),
    url(r'^edit-gpartneringneed/(?P<id>\d+)/$', 'apps.users.views.views.EditGPartneringNeed', 
        name='edit_gpartneringneed'),


    url(r'^create-cpartneringneed/$', 'apps.users.views.views.CreateCPartneringNeed', 
        name='create_cpartneringneed'),
    url(r'^edit-cpartneringneed/(?P<id>\d+)/$', 'apps.users.views.views.EditCPartneringNeed', 
        name='edit_cpartneringneed'),
    url(
        regex=r'user/group/create/$',
        view=GroupCreateView.as_view(),
        name='group_create'
    ),
    url(
        regex=r'user/group/list/$',
        view=GroupListView.as_view(),
        name='group_list'
    ),

)
