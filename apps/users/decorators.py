from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect


def profile_complete_required(function=None):
    """
    Decorator for views that checks that the user has complete profile,
    redirecting to the profile edit page if necessary.
    It also checks if user is authenticated (logged in) so there is no need to use
    login_required decorator with this one.
    """

    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if request.user and not request.user.is_authenticated():
                return HttpResponseRedirect(reverse_lazy('user_login'))
            if not request.user.gov_department and not request.user.commmercial_org:
                return HttpResponseRedirect(reverse_lazy('register_type'))
            if not request.user.profile_complete:
                return HttpResponseRedirect(reverse_lazy('edit_personal_profile'))
            return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)
