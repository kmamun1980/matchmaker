import json
import re
import hashlib
import random
from django.http import Http404
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import (
    render, redirect, render_to_response, get_object_or_404)
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, TemplateView, DetailView, CreateView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from apps.users.models import RoleChoices
from captcha.models import CaptchaStore
from captcha.helpers import captcha_image_url
from braces.views import SuperuserRequiredMixin, JSONResponseMixin
from twilio.rest import TwilioRestClient

from apps.events.models import Attendee
from apps.users.models import (
    PDMFunction, PDMProfile, BusCategory, IndividualCert, activation_email, UserTrack
)
from apps.company.models import (
    Office, PSCCode, Organisation, OrgCategory, GVGPOrgProfile, Agency,
    Department, BusinessCert)
from apps.users.models import (
    VOSBExperience, VOSB, HostRegistration, GPartneringNeed, ActivationCode,
    ResetCode, User, InvalidLogin, VGPTypeChoices)
from apps.company.forms import RegisterTypeGovForm
from apps.users.forms import (
    PDMFunctionForm, PDMProfileForm, BusCategoryForm, OfficeForm, PSCCodeForm,
    OrganisationForm, OrgCategoryForm, AgencyForm, DepartmentForm,
    VOSBExperienceForm, VOSBForm, BusinessCertForm, IndividualCertForm,
    HostRegistrationForm, GPartneringNeedForm,
    CPartneringNeedForm, SignupForm, GroupForm, LoginForm, ProfileTypeForm
)
from apps.users.decorators import profile_complete_required

TWILIO_CLIENT = TwilioRestClient(settings.TWILIO_SID, settings.TWILIO_TOKEN)

SHA1_RE = re.compile('^[a-f0-9]{40}$')


def home(request):
    return render_to_response('home.html', RequestContext(request))


class RegisterView(TemplateView, JSONResponseMixin):

    def post(self, request, *args, **kwargs):
        form = SignupForm(request.POST)
        self.content_type = u"application/json"

        if not form.is_valid():
            return self.render_json_response(
                {'success': False,
                 'message': form.errors}
            )

        form.save()
        user = form.instance

        sms_text = 'Hello {0} Your Matchmaker Unlock code is {1}'.format(
            user.username, user.reg_code)

        try:
            TWILIO_CLIENT.sms.messages.create(
                body=sms_text, to=user.phone, from_=settings.TWILIO_NUMBER)
        except Exception:
            # TODO: logger.error()
            pass

        code = ActivationCode.objects.create(user=user)
        activation_email(code.token, user)

        return self.render_json_response({
            'success': True,
            'id': user.id,
            'message': 'Registration Complete now verify your code'
        })


class RegisterTypeView(TemplateView, JSONResponseMixin):
    def post(self, request, *args, **kwargs):
        user = request.user
        form = ProfileTypeForm(request.POST, instance=user)
        if not form.is_valid():
            kwargs['form'] = form
            return self.get(request, *args, **kwargs)



        if form.cleaned_data['vgp_type'] == VGPTypeChoices.GOV:
            gov_form = RegisterTypeGovForm(request.POST)

            if not gov_form.is_valid():
                kwargs['gov_form'] = gov_form
                return self.get(request, *args, **kwargs)
            request.session['register_type'] = request.POST


            org = GVGPOrgProfile.objects.filter(department=form.data['department'],
                                    agency=form.data['agency'],
                                    office=form.data['office'])
            if org:
                user.gov_department = org[0]
                user.save()
            return HttpResponseRedirect(reverse('edit_personal_profile'))
        else:
            user.vgp_type = VGPTypeChoices.COM
            user.save()

        return HttpResponseRedirect(reverse('fill_company'))

    def get_context_data(self, **kwargs):
        return {
            'form': kwargs.get('form', ProfileTypeForm()),
            'gov_form': kwargs.get('gov_form', RegisterTypeGovForm()),
            'gov_orgs': GVGPOrgProfile.objects.all(),
            'roles':[role[0] for role in RoleChoices.choices],
        }


def VerifyCode(request):
    if request.method == 'POST':
        code = request.POST.get('code', None)
        user = User.objects.filter(reg_code=code)

        if code and user.exists():
            user = user[0]
            user.verified = True
            user.save()

            user_auth = authenticate(
                username=user.username,
                password=''
            )
            login(request, user_auth)

            data = json.dumps({
                'message': 'Code is Correct, please continue your registration',
                'success': True
            })
            return HttpResponse(data)

        data = json.dumps({
            'message': 'Code not Correct, please check and try again',
            'success': False
        })
        return HttpResponse(data)


def VerifySuccess(request):
    return render_to_response('users/verify_success.html',
                              RequestContext(request))


def ActivateAccount(request, activation_key):

        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
        if SHA1_RE.search(activation_key):
            try:
                token = ActivationCode.objects.get(token=activation_key)
                user = User.objects.get(id=token.user_id)
                user.verified = True
                user.save()

                user_auth = authenticate(
                    username=user.username,
                    password=''
                )
                login(request, user_auth)

                return HttpResponseRedirect(reverse('register_type'))

            except ActivationCode.DoesNotExist:
                return HttpResponse(
                    'No Account Was found, Activation Code was tampered with')
        else:
            return HttpResponse('Activation Key is Wrong!')


def ActivateSuccess(request):

    return render_to_response('users/activate.html', RequestContext(request))


class LoginView(TemplateView, JSONResponseMixin):
    invalid_login = None

    def get_context_data(self, **kwargs):
        if 'view' not in kwargs:
            kwargs['view'] = self
        kwargs['form'] = LoginForm()
        kwargs['show_captcha'] = self.validate_captcha()
        return kwargs

    def get_client_ip(self):
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = self.request.META.get('REMOTE_ADDR')
        return ip

    def get_invalid_login(self):
        if not self.invalid_login:
            self.invalid_login, created = InvalidLogin.objects.get_or_create(
                ip_address=self.get_client_ip())

    def validate_captcha(self):
        self.get_invalid_login()
        return self.invalid_login.effort_count >= InvalidLogin.MAX_MISTAKES_COUNT

    def clean_invalid_login(self):
        self.get_invalid_login()
        self.invalid_login.effort_count = 0
        self.invalid_login.save()

    def report_invalid_login(self, form):
        self.get_invalid_login()
        self.invalid_login.effort_count += 1
        self.invalid_login.username = form.data.get('username')
        self.invalid_login.save()
        return self.validate_captcha()

    def get_new_captcha(self):
        key = CaptchaStore.generate_key()
        return {'key': key, 'image_url': captcha_image_url(key)}

    def post(self, request, *args, **kwargs):
        self.content_type = u"application/json"
        form = LoginForm(request.POST, validate_capcha=self.validate_captcha())
        if not form.is_valid():
            return self.render_json_response({
                'success': False,
                'show_captcha': self.report_invalid_login(form),
                'captcha_data': self.get_new_captcha(),
                'message': '<br>'.join(
                    '{0} {1}'.format(k, v[0]) for k, v in form.errors.items())
            })

        try:
            username = User.objects.get(
                email__iexact=form.data.get('username')).username
        except User.DoesNotExist:
            username = form.data.get('username')
        password = form.data.get('password')

        user = authenticate(username=username, password=password)
        status = show_captcha = False
        message = 'Wrong username or password'

        if user is not None:
            if user.is_active:
                login(request, user)
                user = request.user

                uc = UserTrack.objects.filter(user=user).count()
                if not uc:
                    ut = UserTrack(user=user,active=True)
                    ut.save()
                else:
                    ut = UserTrack.objects.get(user=user)
                    ut.active = True
                    ut.save()
                status = True
                message = 'Success'
                self.clean_invalid_login()
            else:
                message = 'Account Is disabled, contact us to resolve this'

        if not status:
            show_captcha = self.report_invalid_login(form)
        if status:
            status = 'true'

        return self.render_json_response({
            'success': status,
            'message': message,
            'show_captcha': show_captcha,
            'captcha_data': self.get_new_captcha()
        })






# TODO: change to django logout view
def do_logout(request):
    # user = request.user
    # delUserTrack(user)
    user = request.user
    try:
        ut, created = UserTrack.objects.get_or_create(user=user)
        ut.active = False
        ut.save()
    except:
        pass
    logout(request)
    
    return redirect('/')


@profile_complete_required
def teams(request):
    return render(request, 'users/teams.html')


@profile_complete_required
def campaign(request):
    return render(request, 'users/campaign.html')


@profile_complete_required
def contacts(request):
    return render(request, 'users/contacts.html')


def RegSuccess(request):

    return render_to_response('reg_success.html', RequestContext(request))


@profile_complete_required
def Events(request):
    profile = User.objects.get(id=request.user.id)

    return render_to_response(
        'users/events.html', locals(), RequestContext(request))


@profile_complete_required
def EventsAll(request):

    return render_to_response(
        'users/../../templates/events/all_events.html',
        locals(), RequestContext(request))


@profile_complete_required
def Opportunity(request):

    return render_to_response(
        'users/search.html', locals(), RequestContext(request))


@profile_complete_required
def group(request):

    return render_to_response(
        'users/groups.html', locals(), RequestContext(request))


@profile_complete_required
def social_media(request):
    return render(request, 'users/social_media.html')


@profile_complete_required
def resume(request):
    return render(request, 'users/resume.html')


@profile_complete_required
def sms_campaign(request):
    return render(request, 'users/sms_campaign.html')


def ResetSuccess(request):
    return render(request, 'users/reset_success.html')


@profile_complete_required
def email_campaign(request):
    return render(request, 'users/email_campaign.html')


@profile_complete_required
def voice_campaign(request):
    return render(request, 'users/voice_campaign.html')


@profile_complete_required
def PurchasePlan(request):
    return HttpResponseRedirect('/user/dashboard')


@profile_complete_required
def user_settings(request):
    users = request.user
    if request.method == "POST":
        users.first_name = request.POST['firstname']
        users.last_name = request.POST['lastname']
        users.address = request.POST['address']
        users.company = request.POST['company']

        users.designation = request.POST['designation']
        users.fax = request.POST['fax']
        users.contact_no = request.POST['contactnumber']
        users.email = request.POST['email']
        users.save()

        return render_to_response(
            'users/settings.html', RequestContext(request))

    return render(request, 'users/settings.html')


@csrf_exempt
def ResetPass(request):
    if request.method == "POST":
        user = User.objects.filter(
            email__exact=request.POST['password_reset_email'])
        if user:
            email = request.POST['password_reset_email']
            try:
                user = User.objects.get(email__exact=email)

                salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
                username = user.username.encode('utf-8')
                activation_key = hashlib.sha1(salt+username).hexdigest()
                ResetCode.objects.create(
                    user=user, token_type=2, token=activation_key)

                json_data = json.dumps(
                    {'message': "Reset email was sent! ",
                     'success': 'true',
                     'key': activation_key})
                return HttpResponse(json_data)

            except Exception:

                data = {'message': 'Account not found'}
                return HttpResponse(json.dumps(data))

        else:
            data = {'message': 'Account Not Found', 'success': 'false'}

            json_data = json.dumps(data)

            return HttpResponse(json_data)

    else:
        return render_to_response('pages-signin.html',RequestContext(request))


def NewPass(request, activation_key):
        if request.method == 'POST':

            password = request.POST.get('password1', None)
            password1 = request.POST.get('password2', None)
            if password != password1:
                return HttpResponse(
                    json.dumps({'message': 'Passwords must match',
                                'success': 'false'})
                )

        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
            if SHA1_RE.search(activation_key):
                try:
                    profile = ResetCode.objects.get(token=activation_key)
                    u = User.objects.get(id=profile.user.id)
                    u.set_password(password)
                    u.save()
                    user = authenticate(username=u.username, password=password)
                    if user is not None:
                        if user.is_active:
                            login(request, user)
                            return HttpResponse(json.dumps(
                                {'message': 'Passwords Changed',
                                 'success': 'true'}
                            ))

                        else:
                            return HttpResponse(
                                json.dumps({
                                    'message': 'Password Changed but unable '
                                               'to log you in',
                                    'success':'true'}
                                )
                            )

                except ActivationCode.DoesNotExist:
                    return HttpResponse(json.dumps({'message':'Activation Key is wrong','success':'false'}))
            else:
                return HttpResponse('Activation Key was tampered with')
        else:

        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
            if SHA1_RE.search(activation_key):
                try:
                    profile = ResetCode.objects.get(token=activation_key)

                    return render_to_response('new_pass.html',RequestContext(request))
                except ActivationCode.DoesNotExist:
                    return  HttpResponse('Activation Keyrejected')
            else:
                return HttpResponse('Activation Key was tampered with')


@profile_complete_required
def Profile(request):
    #check whether verified

    if request.user.verified:
        profile = request.user
        connections = request.user.relationships.following()
        data = []
        for x in connections:
            if x.id != request.user.id:
                data.append(x.id)
        following = User.objects.filter(id__in = data)

        events = Attendee.objects.filter(user=request.user)[:3]

        return render_to_response(
            'users/profile.html', locals(), RequestContext(request))

    else:
        return HttpResponseRedirect('/registration-successful/')


def ResetSuccess(request):
   return render_to_response(
       'profile/reset_success.html', RequestContext(request))


class PublicProfile(DetailView):
    template_name = 'users/public_profile.html'
    context_object_name = 'profile'

    def get_object(self, queryset=None):
        username = self.kwargs.get('username', None)
        return User.objects.get(username__exact=username)

    def get_context_data(self, **kwargs):
        context = {}
        if self.object:
            context['events'] = Attendee.objects.filter(user=self.object.id)[:3]

            connections = self.object.relationships.following()
            data = []
            for x in connections:
                if x.id != self.object.id:
                    data.append(x.id)
            context['following'] = User.objects.filter(id__in=data)

        context.update(kwargs)

        return super(PublicProfile, self).get_context_data(**context)


@profile_complete_required
def Connections(request):

    profile = User.objects.get(id=request.user.id)

    connections = request.user.relationships.following()

    data = []
    for x in connections:
        if x.id != request.user.id:
            data.append(x.id)
    following = User.objects.filter(id__in=data)
    data = []
    for user in following:
        user2 = [code.code for code in user.naics.all()]
        user1 = [code.code for code in request.user.naics.all()]
        x = set(user1).intersection(user2)

        if len(x) < len(user1):
            match = int(float(len(x))/float(len(user1))*float(100))
        else:
            match = 100
        data.append({'user': user, 'match':match})

        #print str(match) + user.username
    following = data

    return render_to_response(
        'users/connections.html', locals(), RequestContext(request))


@profile_complete_required
def connect(request, id):

    frd = User.objects.get(id=id)
    request.user.relationships.add(frd)

    return render_to_response(
        'users/connect.html', locals(), RequestContext(request))


####################################################################################

@profile_complete_required
def CreatePDMFunction(request):
    if request.method == 'POST':
        form = PDMFunctionForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = PDMFunctionForm()

    return render_to_response(
        'users/crude/create_pdmfunction.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditPDMFunction(request, id):
    profile = None
    try:
       profile = PDMFunction.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = PDMFunctionForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = PDMFunctionForm(instance=profile)

    return render_to_response(
        'users/crude/edit_pdmfunction.html', locals(), RequestContext(request))


@profile_complete_required
def CreatePDMProfile(request):
    if request.method == 'POST':
        form = PDMProfileForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = PDMProfileForm()

    return render_to_response(
        'users/crude/create_pdmprofile.html', locals(), RequestContext(request))


@profile_complete_required
def EditPDMProfile(request, id):
    profile = None
    try:
       profile = PDMProfile.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = PDMProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = PDMProfileForm(instance=profile)

    return render_to_response(
        'users/crude/edit_pdmprofile.html', locals(), RequestContext(request))


@profile_complete_required
def CreateBusCategory(request):
    if request.method == 'POST':
        form = BusCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = BusCategoryForm()

    return render_to_response(
        'users/crude/create_buscategory.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditBusCategory(request, id):
    profile = None
    try:
       profile = BusCategory.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = BusCategoryForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = BusCategoryForm(instance=profile)

    return render_to_response(
        'users/crude/edit_buscategory.html', locals(), RequestContext(request))


@profile_complete_required
def CreateOffice(request):
    if request.method == 'POST':
        form = OfficeForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = OfficeForm()

    return render_to_response(
        'users/crude/create_office.html', locals(), RequestContext(request))


@profile_complete_required
def EditOffice(request, id):
    profile = None
    try:
       profile = Office.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = OfficeForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = OfficeForm(instance=profile)

    return render_to_response(
        'users/crude/edit_office.html', locals(), RequestContext(request))


@profile_complete_required
def CreatePSCCode(request):
    if request.method == 'POST':
        form = PSCCodeForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = PSCCodeForm()

    return render_to_response(
        'users/crude/create_psccode.html', locals(), RequestContext(request))


@profile_complete_required
def EditPSCCode(request, id):
    profile = None
    try:
       profile = PSCCode.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = PSCCodeForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = PSCCodeForm(instance=profile)

    return render_to_response(
        'users/crude/edit_psccode.html', locals(), RequestContext(request))


@profile_complete_required
def CreateOrganisation(request):
    if request.method == 'POST':
        form = OrganisationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = OrganisationForm()

    return render_to_response(
        'users/crude/create_organisation.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditOrganisation(request, id):
    profile = None
    try:
       profile = Organisation.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = OrganisationForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = OrganisationForm(instance=profile)

    return render_to_response(
        'users/crude/edit_organisation.html', locals(), RequestContext(request))


@profile_complete_required
def CreateOrgCategory(request):
    if request.method == 'POST':
        form = OrgCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = OrgCategoryForm()

    return render_to_response(
        'users/crude/create_orgcategory.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditOrgCategory(request, id):
    profile = None
    try:
       profile = OrgCategory.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = OrgCategoryForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = OrgCategoryForm(instance=profile)

    return render_to_response(
        'users/crude/edit_orgcategory.html', locals(), RequestContext(request))


@profile_complete_required
def CreateAgency(request):
    if request.method == 'POST':
        form = AgencyForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = AgencyForm()

    return render_to_response(
        'users/crude/create_agency.html', locals(), RequestContext(request))


@profile_complete_required
def EditAgency(request, id):
    profile = None
    try:
       profile = Agency.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = AgencyForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = AgencyForm(instance=profile)

    return render_to_response(
        'users/crude/edit_agency.html', locals(), RequestContext(request))


@profile_complete_required
def CreateDepartment(request):
    if request.method == 'POST':
        form = DepartmentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = DepartmentForm()

    return render_to_response(
        'users/crude/create_department.html', locals(), RequestContext(request))


@profile_complete_required
def EditDepartment(request, id):
    profile = None
    try:
       profile = Department.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = DepartmentForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = DepartmentForm(instance=profile)

    return render_to_response(
        'users/crude/edit_department.html', locals(), RequestContext(request))


@profile_complete_required
def CreateVOSBExperience(request):
    if request.method == 'POST':
        form = VOSBExperienceForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = VOSBExperienceForm()

    return render_to_response(
        'users/crude/create_vosbexperience.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditVOSBExperience(request, id):
    profile = None
    try:
       profile = VOSBExperience.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = VOSBExperienceForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = VOSBExperienceForm(instance=profile)

    return render_to_response(
        'users/crude/edit_vosbexperience.html', locals(),
        RequestContext(request))


@profile_complete_required
def CreateVOSB(request):
    if request.method == 'POST':
        form = VOSBForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = VOSBForm()

    return render_to_response(
        'users/crude/create_vosb.html', locals(), RequestContext(request))


@profile_complete_required
def EditVOSB(request, id):
    profile = None
    try:
       profile = VOSB.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = VOSBForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = VOSBForm(instance=profile)

    return render_to_response(
        'users/crude/edit_vosb.html', locals(), RequestContext(request))


@profile_complete_required
def CreateBusinessCert(request):
    if request.method == 'POST':
        form = BusinessCertForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = BusinessCertForm()

    return render_to_response(
        'users/crude/create_businesscert.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditBusinessCert(request, id):
    profile = None
    try:
       profile = BusinessCert.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = BusinessCertForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = BusinessCertForm(instance=profile)

    return render_to_response(
        'users/crude/edit_businesscert.html', locals(), RequestContext(request))


@profile_complete_required
def CreateIndividualCert(request):
    if request.method == 'POST':
        form = IndividualCertForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = IndividualCertForm()

    return render_to_response(
        'users/crude/create_individualcert.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditIndividualCert(request, id):
    profile = None
    try:
       profile = IndividualCert.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = IndividualCertForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = IndividualCertForm(instance=profile)

    return render_to_response(
        'users/crude/edit_individualcert.html', locals(),
        RequestContext(request))


@profile_complete_required
def CreateHostRegistration(request):
    if request.method == 'POST':
        form = HostRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = HostRegistrationForm()

    return render_to_response(
        'users/crude/create_hostregistration.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditHostRegistration(request, id):
    profile = None
    try:
       profile = HostRegistration.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = HostRegistrationForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = HostRegistrationForm(instance=profile)

    return render_to_response(
        'users/crude/edit_hostregistration.html', locals(),
        RequestContext(request))


@profile_complete_required
def CreateGPartneringNeed(request):
    if request.method == 'POST':
        form = GPartneringNeedForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = GPartneringNeedForm()

    return render_to_response(
        'users/crude/create_gpartneringneed.html', locals(),
        RequestContext(request))


@profile_complete_required
def EditGPartneringNeed(request, id):
    profile = None
    try:
       profile = GPartneringNeed.objects.get(id=id)
    except:
        raise Http404
    if request.method == 'POST':
        form = GPartneringNeedForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = GPartneringNeedForm(instance=profile)

    return render_to_response(
        'users/crude/edit_gpartneringneed.html', locals(),
        RequestContext(request))


@profile_complete_required
def CreateCPartneringNeed(request):
    if request.method == 'POST':
        form = CPartneringNeedForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.dashboard'))
    else:
        form = CPartneringNeedForm()

    return render_to_response(
        'users/crude/create_cpartneringneed.html', locals(),
        RequestContext(request))


class GroupCreateView(SuperuserRequiredMixin, CreateView):
    model = Group
    form_class = GroupForm
    success_url = reverse_lazy('group_list')
    template_name = 'users/group_form.html'


class GroupListView(SuperuserRequiredMixin, ListView):
    model = Group
    template_name = 'users/group_list.html'
    context_object_name = 'groups'


def EditCPartneringNeed(request, id):
    pass

    # profile = None
    # try:
    #    profile = CPartneringNeed.objects.get(id=id)
    # except:
    #     raise Http404
    # if request.method == 'POST':
    #     form = CPartneringNeedForm(request.POST, instance=profile)
    #     if form.is_valid():
    #         form.save()
    #         return HttpResponseRedirect(reverse('apps.dashboard'))
    # else:
    #     form = CPartneringNeedForm(instance = profile)
    #
    # return render_to_response(
    #     'users/crude/edit_cpartneringneed.html',
    #     locals(), RequestContext(request)
    # )
    #
    #
    #

