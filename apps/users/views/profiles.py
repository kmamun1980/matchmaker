from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import password_reset
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db.models import get_model
from django.shortcuts import render_to_response, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, Http404,HttpResponse
from django.template import RequestContext
from django.views.generic import (
    TemplateView, UpdateView, CreateView, DeleteView, View)
from django.conf import settings
from django.contrib import messages
import json


from braces.views import JSONResponseMixin

from apps.company.models import GVGPOrgProfile, CVGPOrgProfile
from apps.users.models import User, Question, VGPTypeChoices
from apps.users.models import Role
from apps.company.forms import GVGPOrgProfileSelectForm, RegisterTypeComForm
from apps.users.forms import UserProfileForm

ACCOUNT_SID = settings.TWILIO_SID
AUTH_TOKEN = settings.TWILIO_TOKEN


class FillCompany(TemplateView):


    def post(self, request, *args, **kwargs):
        form = RegisterTypeComForm(request.POST)

        if not form.is_valid():
            print form.errors
            kwargs['form'] = form
            return self.get(request, *args, **kwargs)

        user = request.user
        # don't change field name, it's true name :)

        user.commmercial_org_id = form.cleaned_data.get('company')
        user.years_of_exp = 0

        user.save()
        return HttpResponseRedirect(reverse('edit_personal_profile'))

    def get_context_data(self, **kwargs):
        return {
            'form': kwargs.get('form', RegisterTypeComForm()),
            'orgs': CVGPOrgProfile.objects.all()
        }


@csrf_exempt
def FillOrganization(request):
    if request.method == 'POST':
        gov = GVGPOrgProfile.objects.get(id=int(request.POST.get('company')))
        request.user.gov_department = gov
        request.user.save()
        return HttpResponseRedirect(reverse('edit_personal_profile'))
    companies = GVGPOrgProfile.objects.all()
    if request.session.get('register_type', False):
        type_dict = request.session['register_type']
        companies = companies.filter(
            department__id=type_dict['department'],
            agency__id=type_dict['agency'],
            office__id=type_dict['office']
        )
    return render_to_response(
        'profile/fill_organization.html', locals(), RequestContext(request))


class FillGovView(TemplateView):

    def post(self, request, *args, **kwargs):
        form = GVGPOrgProfileSelectForm(request.POST)
        if not form.is_valid():
            kwargs['form'] = form
            return self.get(request, *args, **kwargs)


        org = GVGPOrgProfile.objects.filter(department=form.data['department'],
                                    agency=form.data['agency'],
                                    office=form.data['office'])
        #org_category=form.data['org_category'],

        user = request.user
        if org:
            user.gov_department = org[0]
        user.role = form.data['role']
        user.save()

        return HttpResponseRedirect(reverse('profile'))

    def get_context_data(self, **kwargs):
        return {
            'form': kwargs.get('form', GVGPOrgProfileSelectForm()),
            'roles': Role.objects.all(),
            'gov_orgs': GVGPOrgProfile.objects.all()
        }


class EditPersonalProfile(UpdateView):
    form_class = UserProfileForm
    template_name = 'profile/edit_profile.html'
    context_object_name = 'profile'

    def get_success_url(self):
        messages.success(self.request, 'Profile details updated')
        return reverse('edit_personal_profile')

    def get_object(self, queryset=None):
        return User.objects.get(id=self.request.user.id)

    def form_valid(self, form):
        self.object = form.save()
        self.object.naics.clear()
        for item in form.cleaned_data['naics']:
            self.object.naics.add(item)

        self.object.psc_fsc.clear()
        for item in form.cleaned_data['psc_fsc']:
            self.object.psc_fsc.add(item)

        return HttpResponseRedirect(self.get_success_url())



class CreateUserCertificate(CreateView, JSONResponseMixin):
    template_name = 'profile/individual_cert.html'

    def post(self, request, *args, **kwargs):
        self.model = get_model(
            request.POST.get('app'),
            request.POST.get('model')
        )
        return super(CreateUserCertificate, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        form.save()

        return self.render_to_response({'row': form.instance})

    def form_invalid(self, form):
        self.content_type = u"application/json"
        return self.render_json_response({
            'status': 'error',
            'errors': form.errors
        })


class DeleteUserCertificate(DeleteView, JSONResponseMixin):
    success_url = '/'  # it does not matter

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        print queryset
        print self.kwargs.get('users')

        pk = self.kwargs.get(self.pk_url_kwarg, None)
        user_id = self.kwargs.get('user')
        queryset = queryset.filter(pk=pk, user__id=user_id)

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except ObjectDoesNotExist:
            raise Http404("Row not found")

        return obj

    def delete(self, request, *args, **kwargs):
        self.kwargs = request.POST.dict()
        self.model = get_model(
            request.POST.get('app'),
            request.POST.get('model')
        )
        super(DeleteUserCertificate, self).delete(request, *args, **kwargs)
        self.content_type = u"application/json"
        return self.render_json_response({
            'status': 'success'
        })


@csrf_exempt
def CreateProfileField(request):

    if request.method == 'POST':

        name = request.POST.get('name', None)
        required = request.POST.get('required', None)
        active = request.POST.get('active', False)
        question_type = request.POST.get('question_type', None)
        choices = request.POST.get('choices', None)

        if active:
            active = True
        else:
            active = False
        if required:
            required = True
        else:
            required = False
        field = Question.objects.create(
            name=name, question_type=question_type, required=required,
            active=active, choices=choices)
        return HttpResponseRedirect('/profile-fields/')

    return render_to_response(
        'profile/create_profile_field.html', locals(), RequestContext(request))


@csrf_exempt
def ProfileFields(request):
    data = Question.objects.all()

    return render_to_response(
        'profile/profile_fields.html', locals(), RequestContext(request))


@csrf_exempt
def EditProfileField(request, id):
    field = Question.objects.get(id=int(id))

    if request.method == 'POST':

        name = request.POST.get('name',None)
        required = request.POST.get('required',None)
        active = request.POST.get('active',False)
        question_type = request.POST.get('question_type',None)
        choices = request.POST.get('choices',None)
        if active:
            field.active = True
        else:
            field.active = False
        if required:
            field.required = True
        else:
            field.required = False

        field.question_type = question_type
        field.choices = choices
        field.name = name
        field.save()
        return HttpResponseRedirect('/profile-fields/')

    return render_to_response(
        'profile/edit_profile_field.html', locals(), RequestContext(request))


@csrf_exempt
def DeleteProfileField(request, id):

    if request.method == 'POST':
        data = Question.objects.get(id=int(id))
        data.delete()
        return HttpResponseRedirect('/profile-fields/')

    field = Question.objects.get(id=int(id))
    return render_to_response(
        'profile/delete_profile_field.html', locals(), RequestContext(request))


@login_required
@staff_member_required
def all_users(request):
    users = User.objects.exclude(id=request.user.id)

    return render_to_response('profile/all_users.html', {
        'users': users,
        'user_count': users.count(),
        'gov_users': users.filter(vgp_type=VGPTypeChoices.GOV).count(),
        'com_users': users.filter(vgp_type=VGPTypeChoices.COM).count()
    }, RequestContext(request))


class ResetUsersPassword(View, JSONResponseMixin):

    @method_decorator(login_required)
    @method_decorator(staff_member_required)
    def post(self, request, *args, **kwargs):

        ids = request.POST.getlist('ids[]')
        for row in ids:
            user = User.objects.get(id=row)
            request.POST = {
                'email': user.email
            }
            password_reset(request, is_admin_site=True, post_reset_redirect='/')

        self.content_type = u"application/json"
        return self.render_json_response({
            'status': 'success',
            'message': 'Mail with notification send to users'
        })


def Dashboard(request):

    return render_to_response(
        'reports/flot.html', locals(), RequestContext(request))


def reverify(request):

    if request.method == 'POST':
        email = request.POST.get('email')

        user = User.objects.filter(email=email)
        if user:
            user = user[0]

            sms_text = 'Hello {0} Your Matchmaker Unlock code is {1}'.format(
            user.username, user.reg_code)
            return HttpResponse(json.dumps({'success':'true'}))

            try:
                TWILIO_CLIENT.sms.messages.create(
                    body=sms_text, to=user.phone, from_=settings.TWILIO_NUMBER)
            except Exception:
                # TODO: logger.error()
                pass

            code = ActivationCode.objects.get(user=user)
            activation_email(code.token, user)

            code = ActivationCode.objects.create(user=user)
            activation_email(code.token, user)

        else:
            return HttpResponse(json.dumps({'success':'false'}))

