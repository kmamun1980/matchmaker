from django.contrib import admin
from models import (
    ActivationCode, ResetCode, User, Response, Question,
    PDMFunction, PDMProfile, BusCategory, Role,
    VOSBExperience, VOSB,
    HostRegistration, IndividualCert
)

class UserAdmin(admin.ModelAdmin):
    change_form_template = 'loginas/change_form.html'


class QAdmin(admin.ModelAdmin):
    list_display = ['name', 'question_type']

admin.site.register(ActivationCode)

admin.site.register(ResetCode)
admin.site.register(Response)
admin.site.register(Question, QAdmin)

admin.site.register(PDMProfile)
admin.site.register(PDMFunction)
admin.site.register(VOSB)
admin.site.register(BusCategory)
admin.site.register(VOSBExperience)
admin.site.register(User,UserAdmin)
admin.site.register(Role)
admin.site.register(IndividualCert)
admin.site.register(HostRegistration)






