# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PDMFunction'
        db.create_table(u'users_pdmfunction', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['PDMFunction'])

        # Adding model 'Role'
        db.create_table(u'users_role', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['Role'])

        # Adding model 'BusCategory'
        db.create_table(u'users_buscategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['BusCategory'])

        # Adding model 'User'
        db.create_table(u'users_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('vgp_type', self.gf('django.db.models.fields.CharField')(default='1', max_length=1)),
            ('vosb_type', self.gf('django.db.models.fields.SmallIntegerField')(default=1, null=True, blank=True)),
            ('salutation', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('preffered_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('market_segment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('org_creator', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('veteran_status', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('veteran_type', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('branch', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('bio', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('responsibilities', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('years_of_exp', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('gov_department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.GVGPOrgProfile'], null=True, blank=True)),
            ('commmercial_org', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.CVGPOrgProfile'], null=True, blank=True)),
            ('rfid', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('duns', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('reg_code', self.gf('django.db.models.fields.CharField')(default=44834L, max_length=255, null=True, blank=True)),
            ('verified', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('activation_method', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('agenda_email', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_unusable_password', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cities_light.Country'], null=True, blank=True)),
            ('region', self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['cities_light.Region'], null=True, blank=True)),
            ('city', self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['cities_light.City'], null=True, blank=True)),
            ('zip', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('linkedin_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('facebook_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('twitter_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('website_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('profile_complete', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('date_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('is_private_salutation', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_first_name', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_last_name', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_preffered_name', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_bio', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_responsibilities', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_email', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_phone', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_street', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_city', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_region', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_country', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_zip', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_linkedin_url', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_facebook_url', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_twitter_url', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_website_url', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_role', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_market_segment', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_years_of_exp', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_veteran_status', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_veteran_type', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_private_duns', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'users', ['User'])

        # Adding M2M table for field groups on 'User'
        m2m_table_name = db.shorten_name(u'users_user_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'users.user'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'User'
        m2m_table_name = db.shorten_name(u'users_user_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'users.user'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'permission_id'])

        # Adding M2M table for field naics on 'User'
        m2m_table_name = db.shorten_name(u'users_user_naics')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'users.user'], null=False)),
            ('nscode', models.ForeignKey(orm[u'company.nscode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'nscode_id'])

        # Adding M2M table for field psc_fsc on 'User'
        m2m_table_name = db.shorten_name(u'users_user_psc_fsc')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'users.user'], null=False)),
            ('psccode', models.ForeignKey(orm[u'company.psccode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'psccode_id'])

        # Adding model 'GPartneringNeed'
        db.create_table(u'users_gpartneringneed', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vgp_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.GVGPOrgProfile'])),
            ('visibility', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('description', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('market_segment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('client', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('poc', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('incubent_contractor', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('estimated_value', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('procurement_method', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('current_exp_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('estimated_start_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('security_requirement', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('documents', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('small_business_types', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('place_of_work', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('geographic_constraints', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('capabilities_constraints', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('relevant_links', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['GPartneringNeed'])

        # Adding model 'HostRegistration'
        db.create_table(u'users_hostregistration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'])),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('primary_function', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.PDMFunction'])),
            ('arrival', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('departure', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('industry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.Industry'], null=True, blank=True)),
            ('primary_naics', self.gf('django.db.models.fields.related.ForeignKey')(related_name='host_p_naics', to=orm['company.NSCode'])),
            ('secondary_naics', self.gf('django.db.models.fields.related.ForeignKey')(related_name='host_s_naics', to=orm['company.NSCode'])),
            ('tertiary_naics', self.gf('django.db.models.fields.related.ForeignKey')(related_name='host_t_naics', to=orm['company.NSCode'])),
            ('other', self.gf('django.db.models.fields.related.ForeignKey')(related_name='other_t_naics', to=orm['company.NSCode'])),
            ('duns', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['HostRegistration'])

        # Adding model 'IndividualCert'
        db.create_table(u'users_individualcert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('certification_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('date_obtained', self.gf('django.db.models.fields.DateField')()),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'])),
            ('document', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['IndividualCert'])

        # Adding model 'PDMProfile'
        db.create_table(u'users_pdmprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'])),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('naics', self.gf('django.db.models.fields.related.ForeignKey')(related_name='pdm_profile_naics', to=orm['company.NSCode'])),
            ('description', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('exp_s_timing', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('estimated_value', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['PDMProfile'])

        # Adding model 'VOSB'
        db.create_table(u'users_vosb', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vosb_creator', to=orm['users.User'])),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.CVGPOrgProfile'])),
            ('duns', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('primary_naics', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vosb_p_naics', to=orm['company.NSCode'])),
            ('secondary_naics', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vosb_s_naics', to=orm['company.NSCode'])),
            ('tertiary_naics', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vosb_t_naics', to=orm['company.NSCode'])),
            ('industry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.Industry'])),
            ('contract_vehicle', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('prime_sub', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('employees', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('vet_employees', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('dcaa_compliant', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('second_core_strength', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('third_core_strength', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('total_cost', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('pop', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('contract', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('customer', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['VOSB'])

        # Adding model 'VOSBExperience'
        db.create_table(u'users_vosbexperience', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vosb_experience', to=orm['users.User'])),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['company.CVGPOrgProfile'])),
            ('duns', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('contract_vehicle', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('prime_sub', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('first_core_strength', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('second_core_strength', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('third_core_strength', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('total_cost', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('pop_start', self.gf('django.db.models.fields.DateField')(max_length=255, null=True, blank=True)),
            ('pop_finish', self.gf('django.db.models.fields.DateField')(max_length=255, null=True, blank=True)),
            ('contract', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('customer', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['VOSBExperience'])

        # Adding M2M table for field naics on 'VOSBExperience'
        m2m_table_name = db.shorten_name(u'users_vosbexperience_naics')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('vosbexperience', models.ForeignKey(orm[u'users.vosbexperience'], null=False)),
            ('nscode', models.ForeignKey(orm[u'company.nscode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['vosbexperience_id', 'nscode_id'])

        # Adding model 'ActivationCode'
        db.create_table(u'users_activationcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'])),
            ('token', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('expired', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('created_at', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('token_type', self.gf('django.db.models.fields.CharField')(default=1, max_length=1)),
        ))
        db.send_create_signal(u'users', ['ActivationCode'])

        # Adding model 'ResetCode'
        db.create_table(u'users_resetcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'])),
            ('token', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('expired', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('created_at', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('token_type', self.gf('django.db.models.fields.CharField')(default=1, max_length=1)),
        ))
        db.send_create_signal(u'users', ['ResetCode'])

        # Adding model 'Question'
        db.create_table(u'users_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('required', self.gf('django.db.models.fields.BooleanField')()),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('question_type', self.gf('django.db.models.fields.CharField')(default='text', max_length=200)),
            ('choices', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['Question'])

        # Adding model 'Response'
        db.create_table(u'users_response', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'])),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.Question'])),
            ('value', self.gf('django.db.models.fields.TextField')()),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['Response'])

        # Adding model 'InvalidLogin'
        db.create_table(u'users_invalidlogin', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ip_address', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('effort_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['InvalidLogin'])

        # Adding model 'UserTrack'
        db.create_table(u'users_usertrack', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'], null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'users', ['UserTrack'])


    def backwards(self, orm):
        # Deleting model 'PDMFunction'
        db.delete_table(u'users_pdmfunction')

        # Deleting model 'Role'
        db.delete_table(u'users_role')

        # Deleting model 'BusCategory'
        db.delete_table(u'users_buscategory')

        # Deleting model 'User'
        db.delete_table(u'users_user')

        # Removing M2M table for field groups on 'User'
        db.delete_table(db.shorten_name(u'users_user_groups'))

        # Removing M2M table for field user_permissions on 'User'
        db.delete_table(db.shorten_name(u'users_user_user_permissions'))

        # Removing M2M table for field naics on 'User'
        db.delete_table(db.shorten_name(u'users_user_naics'))

        # Removing M2M table for field psc_fsc on 'User'
        db.delete_table(db.shorten_name(u'users_user_psc_fsc'))

        # Deleting model 'GPartneringNeed'
        db.delete_table(u'users_gpartneringneed')

        # Deleting model 'HostRegistration'
        db.delete_table(u'users_hostregistration')

        # Deleting model 'IndividualCert'
        db.delete_table(u'users_individualcert')

        # Deleting model 'PDMProfile'
        db.delete_table(u'users_pdmprofile')

        # Deleting model 'VOSB'
        db.delete_table(u'users_vosb')

        # Deleting model 'VOSBExperience'
        db.delete_table(u'users_vosbexperience')

        # Removing M2M table for field naics on 'VOSBExperience'
        db.delete_table(db.shorten_name(u'users_vosbexperience_naics'))

        # Deleting model 'ActivationCode'
        db.delete_table(u'users_activationcode')

        # Deleting model 'ResetCode'
        db.delete_table(u'users_resetcode')

        # Deleting model 'Question'
        db.delete_table(u'users_question')

        # Deleting model 'Response'
        db.delete_table(u'users_response')

        # Deleting model 'InvalidLogin'
        db.delete_table(u'users_invalidlogin')

        # Deleting model 'UserTrack'
        db.delete_table(u'users_usertrack')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'cities_light.city': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('region', 'name'), ('region', 'slug'))", 'object_name': 'City'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'feature_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'population': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'search_names': ('cities_light.models.ToSearchTextField', [], {'default': "''", 'max_length': '4000', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'cities_light.country': {
            'Meta': {'ordering': "['name']", 'object_name': 'Country'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'code3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'continent': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"}),
            'tld': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '5', 'blank': 'True'})
        },
        u'cities_light.region': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('country', 'name'), ('country', 'slug'))", 'object_name': 'Region'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'geoname_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'company.agency': {
            'Meta': {'object_name': 'Agency'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Department']", 'null': 'True', 'blank': 'True'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.cvgporgprofile': {
            'Meta': {'object_name': 'CVGPOrgProfile'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'agency_program': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'average_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'avg_rev': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'bonding_level': ('django.db.models.fields.CharField', [], {'max_length': '55', 'null': 'True', 'blank': 'True'}),
            'bonding_number_aggregate': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'bonding_number_contract': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'business_partner_ship': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'business_relation_ship': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cage_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cbla': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cbla_per_contract': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'city': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['cities_light.City']", 'null': 'True', 'blank': 'True'}),
            'corporate_overview': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']", 'null': 'True', 'blank': 'True'}),
            'cve_verified_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_established': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'dcaa_compliant_acc': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.DCAACompliantAccount']", 'null': 'True', 'blank': 'True'}),
            'documents_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'duns_no': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Industry']", 'null': 'True', 'blank': 'True'}),
            'location': ('geoposition.fields.GeopositionField', [], {'default': "'0,0'", 'max_length': '42', 'null': 'True', 'blank': 'True'}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'naics': ('select2.fields.ManyToManyField', [], {'related_name': "u'cvgporgprofiles'", 'to': u"orm['company.NSCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'organization_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'overview_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'performance_visiblity': ('django.db.models.fields.CharField', [], {'default': "u'Pub'", 'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'phone': ('phonenumber_field.modelfields.PhoneNumberField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'primary_poc': ('django.db.models.fields.CharField', [], {'max_length': '55', 'null': 'True', 'blank': 'True'}),
            'psc_fsc': ('select2.fields.ManyToManyField', [], {'related_name': "u'cvgporgprofiles'", 'to': u"orm['company.PSCCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'region': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'relevant_links': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'sec_clearance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.SecurityClearanceLevel']", 'null': 'True', 'blank': 'True'}),
            'small_business_category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['company.SmallBusinessCategory']", 'null': 'True', 'blank': 'True'}),
            'states_you_do_business': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'states_u_do'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['cities_light.Region']"}),
            'states_you_wish_to_do': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'states_u_wish'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['cities_light.Region']"}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '55', 'null': 'True', 'blank': 'True'}),
            'total_employees': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vet_employees': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'youtube_video': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.dcaacompliantaccount': {
            'Meta': {'object_name': 'DCAACompliantAccount'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'company.department': {
            'Meta': {'object_name': 'Department'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'org_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.OrgCategory']", 'null': 'True', 'blank': 'True'})
        },
        u'company.gvgporgprofile': {
            'Meta': {'object_name': 'GVGPOrgProfile'},
            'agency': ('smart_selects.db_fields.ChainedForeignKey', [], {'blank': 'True', 'related_name': "u'org_agency'", 'null': 'True', 'to': u"orm['company.Agency']"}),
            'c_needs_for': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'contractor_needs': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'org_department'", 'null': 'True', 'to': u"orm['company.Department']"}),
            'gov_office': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'greatest_challenges': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'industry'", 'null': 'True', 'to': u"orm['company.Industry']"}),
            'key_objectives': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'major_initiatives': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'mission': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'naics': ('select2.fields.ManyToManyField', [], {'related_name': "u'gvgporgprofiles'", 'to': u"orm['company.NSCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'office': ('smart_selects.db_fields.ChainedForeignKey', [], {'blank': 'True', 'related_name': "u'org_agency'", 'null': 'True', 'to': u"orm['company.Office']"}),
            'org_category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'org_category'", 'null': 'True', 'to': u"orm['company.OrgCategory']"}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'org_visn'", 'null': 'True', 'to': u"orm['company.Organisation']"}),
            'program_names': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'psc_fsc': ('select2.fields.ManyToManyField', [], {'related_name': "u'gvgporgprofiles'", 'to': u"orm['company.PSCCode']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'search_field': 'None'}),
            'relevant_links': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '255'}),
            'youtube_video': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'company.industry': {
            'Meta': {'object_name': 'Industry'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'company.nscode': {
            'Meta': {'object_name': 'NSCode'},
            'about': ('django.db.models.fields.TextField', [], {'default': "u'no name'", 'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nature': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'company.office': {
            'Meta': {'object_name': 'Office'},
            'agency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Agency']", 'null': 'True', 'blank': 'True'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.organisation': {
            'Meta': {'object_name': 'Organisation'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.orgcategory': {
            'Meta': {'object_name': 'OrgCategory'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'company.psccode': {
            'Meta': {'object_name': 'PSCCode'},
            'about': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nature': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'company.securityclearancelevel': {
            'Meta': {'object_name': 'SecurityClearanceLevel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'company.smallbusinesscategory': {
            'Meta': {'object_name': 'SmallBusinessCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Industry']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'relationships.relationship': {
            'Meta': {'ordering': "('created',)", 'unique_together': "(('from_user', 'to_user', 'status', 'site'),)", 'object_name': 'Relationship'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'from_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'from_users'", 'to': u"orm['users.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'related_name': "'relationships'", 'to': u"orm['sites.Site']"}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relationships.RelationshipStatus']"}),
            'to_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'to_users'", 'to': u"orm['users.User']"}),
            'weight': ('django.db.models.fields.FloatField', [], {'default': '1.0', 'null': 'True', 'blank': 'True'})
        },
        u'relationships.relationshipstatus': {
            'Meta': {'ordering': "('name',)", 'object_name': 'RelationshipStatus'},
            'from_slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'symmetrical_slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'to_slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'verb': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'sites.site': {
            'Meta': {'ordering': "(u'domain',)", 'object_name': 'Site', 'db_table': "u'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'users.activationcode': {
            'Meta': {'object_name': 'ActivationCode'},
            'created_at': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'expired': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'token_type': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']"})
        },
        u'users.buscategory': {
            'Meta': {'object_name': 'BusCategory'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'users.gpartneringneed': {
            'Meta': {'object_name': 'GPartneringNeed'},
            'capabilities_constraints': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'client': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'current_exp_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'documents': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'estimated_start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'estimated_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'geographic_constraints': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incubent_contractor': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'market_segment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'place_of_work': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'poc': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'procurement_method': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'relevant_links': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'security_requirement': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'small_business_types': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'vgp_user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.GVGPOrgProfile']"}),
            'visibility': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'users.hostregistration': {
            'Meta': {'object_name': 'HostRegistration'},
            'arrival': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'departure': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'duns': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Industry']", 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'other': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'other_t_naics'", 'to': u"orm['company.NSCode']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'primary_function': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.PDMFunction']"}),
            'primary_naics': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'host_p_naics'", 'to': u"orm['company.NSCode']"}),
            'secondary_naics': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'host_s_naics'", 'to': u"orm['company.NSCode']"}),
            'tertiary_naics': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'host_t_naics'", 'to': u"orm['company.NSCode']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']"})
        },
        u'users.individualcert': {
            'Meta': {'object_name': 'IndividualCert'},
            'certification_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_obtained': ('django.db.models.fields.DateField', [], {}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']"})
        },
        u'users.invalidlogin': {
            'Meta': {'object_name': 'InvalidLogin'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'effort_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'users.pdmfunction': {
            'Meta': {'object_name': 'PDMFunction'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'users.pdmprofile': {
            'Meta': {'object_name': 'PDMProfile'},
            'description': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'estimated_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'exp_s_timing': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'naics': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pdm_profile_naics'", 'to': u"orm['company.NSCode']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']"})
        },
        u'users.question': {
            'Meta': {'object_name': 'Question'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'choices': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'question_type': ('django.db.models.fields.CharField', [], {'default': "'text'", 'max_length': '200'}),
            'required': ('django.db.models.fields.BooleanField', [], {})
        },
        u'users.resetcode': {
            'Meta': {'object_name': 'ResetCode'},
            'created_at': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'expired': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'token_type': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']"})
        },
        u'users.response': {
            'Meta': {'object_name': 'Response'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.Question']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']"}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'users.role': {
            'Meta': {'object_name': 'Role'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'users.user': {
            'Meta': {'object_name': 'User'},
            'activation_method': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'agenda_email': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'bio': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'branch': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'city': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['cities_light.City']", 'null': 'True', 'blank': 'True'}),
            'commmercial_org': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.CVGPOrgProfile']", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']", 'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'duns': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'facebook_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'gov_department': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.GVGPOrgProfile']", 'null': 'True', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            'has_unusable_password': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_private_bio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_city': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_country': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_duns': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_email': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_facebook_url': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_first_name': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_last_name': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_linkedin_url': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_market_segment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_phone': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_preffered_name': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_region': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_responsibilities': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_role': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_salutation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_street': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_twitter_url': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_veteran_status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_veteran_type': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_website_url': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_years_of_exp': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_private_zip': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'linkedin_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'market_segment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'naics': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'users'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['company.NSCode']"}),
            'org_creator': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'preffered_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'profile_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'psc_fsc': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'users'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['company.PSCCode']"}),
            'reg_code': ('django.db.models.fields.CharField', [], {'default': '21479L', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'region': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'relationships': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'related_to'", 'symmetrical': 'False', 'through': u"orm['relationships.Relationship']", 'to': u"orm['users.User']"}),
            'responsibilities': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'rfid': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'salutation': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'twitter_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'veteran_status': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'veteran_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'vgp_type': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'vosb_type': ('django.db.models.fields.SmallIntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'website_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'years_of_exp': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'users.usertrack': {
            'Meta': {'object_name': 'UserTrack'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']", 'null': 'True', 'blank': 'True'})
        },
        u'users.vosb': {
            'Meta': {'object_name': 'VOSB'},
            'contract': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'contract_vehicle': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'dcaa_compliant': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'duns': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'employees': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.Industry']"}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.CVGPOrgProfile']"}),
            'pop': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'primary_naics': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vosb_p_naics'", 'to': u"orm['company.NSCode']"}),
            'prime_sub': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'second_core_strength': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'secondary_naics': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vosb_s_naics'", 'to': u"orm['company.NSCode']"}),
            'tertiary_naics': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vosb_t_naics'", 'to': u"orm['company.NSCode']"}),
            'third_core_strength': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'total_cost': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vosb_creator'", 'to': u"orm['users.User']"}),
            'vet_employees': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'users.vosbexperience': {
            'Meta': {'object_name': 'VOSBExperience'},
            'contract': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'contract_vehicle': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'duns': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'first_core_strength': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'naics': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'vosb_exp_naics'", 'symmetrical': 'False', 'to': u"orm['company.NSCode']"}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['company.CVGPOrgProfile']"}),
            'pop_finish': ('django.db.models.fields.DateField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'pop_start': ('django.db.models.fields.DateField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'prime_sub': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'second_core_strength': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'third_core_strength': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'total_cost': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vosb_experience'", 'to': u"orm['users.User']"})
        }
    }

    complete_apps = ['users']