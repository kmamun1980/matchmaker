# forms.py
import uuid
import logging

from django import forms
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from django.template.defaultfilters import slugify
from django_password_strength.widgets import PasswordStrengthInput

from captcha.fields import CaptchaField, CaptchaTextInput
from crispy_forms.helper import FormHelper
from apps.users.models import (
    User, PDMFunction, PDMProfile, BusCategory, PSCCode, VGPTypeChoices,
    VOSBExperience, VOSB,  IndividualCert,
    HostRegistration, GPartneringNeed,
)

from apps.company.models import (
    Department, Office, Agency, Organisation,
    OrgCategory, BusinessCert, CPartneringNeed,
    PSCCode, NSCode
)

logger = logging.getLogger("apps.users")


class SignupForm(forms.ModelForm):

#     password = forms.CharField(
#         widget=PasswordStrengthInput()
#     )
#     password_confirm = forms.CharField()
    name = forms.CharField()

    class Meta:
        model = User
#         fields = ['first_name', 'last_name', 'email', 'username', 'password',
#                   'phone', 'vgp_type', 'activation_method']
        fields = ['first_name', 'last_name', 'email', 'phone']

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        name = cleaned_data.get('name')
        #password = cleaned_data.get('password')
        #password1 = cleaned_data.get('password_confirm')
        email = cleaned_data.get('email')

        try:
            (cleaned_data['first_name'],
             cleaned_data['last_name']) = name.split(' ')
        except (ValueError, AttributeError):
            pass

        if not cleaned_data.get(
                'first_name') or not cleaned_data.get('last_name'):
            msg = "Enter Full Name"
            self._errors["name"] = self.error_class([msg])
            del cleaned_data['name']
#
#         if password != password1:
#             msg = "Password must Match"
#             self._errors["password_confirm"] = self.error_class([msg])
#             del cleaned_data['password_confirm']

        if email and User.objects.filter(email__exact=email).exists():
            msg = 'Email {0} already in use!'.format(email)
            self._errors["email"] = self.error_class([msg])
            del cleaned_data['email']

        return cleaned_data

    def _generate_username(self, counter=1):
        username = slugify(u'{0}-{1}-{2}'.format(
            self.instance.first_name,
            self.instance.last_name,
            counter))[:25]

        users = User.objects.filter(username=username)

        if users.exists():
            return self._generate_username(counter+1)

        return username

    def save(self, commit=True):
        self.instance.username = self._generate_username()
        self.instance.password = make_password(self.instance.password)
        self.instance.has_unusable_password = True
        self.instance.rfid = uuid.uuid4()
        return super(SignupForm, self).save(commit)


class ProfileTypeForm(forms.ModelForm):
    vgp_type = forms.ChoiceField(choices=VGPTypeChoices.choices,
                                 initial=VGPTypeChoices.GOV,
                                 widget=forms.RadioSelect())
   # vgp_type = forms.ChoiceField(choices=VOSBTypeChoices.choices)

    class Meta:
        model = User
        fields = ['vgp_type', 'vosb_type']


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()
    captcha = CaptchaField(required=False, widget=CaptchaTextInput(
        output_format='%(image)s <a class="js-captcha-refresh">Refresh</a>'
                      '%(hidden_field)s %(text_field)s',
        attrs={
            "placeholder": "Enter the content of image",
            "class": "form-control input-lg",
        }
    ))

    def __init__(self, *args, **kwargs):

        validate_capcha = kwargs.pop('validate_capcha', False)

        super(LoginForm, self).__init__(*args, **kwargs)

        if validate_capcha:
            self.fields['captcha'].required = True


class UserProfileForm(forms.ModelForm):
    change_password = forms.CharField(
        label='Change password', required=False, widget=forms.PasswordInput)
    confirm_password = forms.CharField(
        label='Confirm password', required=False, widget=forms.PasswordInput)

    # naics = forms.ModelMultipleChoiceField(
    #     widget=forms.MultipleHiddenInput,
    #     queryset=NSCode.objects.all(),
    #     cache_choices=False
    # )
    # psc_fsc = forms.ModelMultipleChoiceField(
    #     widget=forms.MultipleHiddenInput,
    #     queryset=PSCCode.objects.all(),
    #     cache_choices=False
    # )

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields.get(field_name)
            if field:
                if type(field.widget) in (forms.TextInput, forms.DateInput):
                    field.widget.attrs['placeholder'] = field.label
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['naics'].label = 'Search by NAICS code'
        self.fields['psc_fsc'].label = 'Search by PSC code'

        user = kwargs.get('instance')
        logger.debug(user)
        logger.debug("Is gov: %s" % str(user.is_com))
        logger.debug("Is com %s" % str(user.is_gov))

        if user.vgp_type == VGPTypeChoices.GOV:
            self.fields['gov_department'].required = True
            self.fields['gov_department'].label = 'Organization'
            self.fields['role'].label = 'Primary Function'
            del self.fields['duns']
        elif user.vgp_type == VGPTypeChoices.COM:
            del self.fields['facebook_url']
            del self.fields['twitter_url']
            del self.fields['website_url']
            del self.fields['gov_department']
            del self.fields['role']
            del self.fields['duns']
            # self.fields['duns'].required = True

        if user.has_unusable_password:
            self.fields['change_password'].required = True
            self.fields['confirm_password'].required = True

    def clean(self):
        cleaned_data = super(UserProfileForm, self).clean()
        change_password = cleaned_data.get('change_password')
        confirm_password = cleaned_data.get('confirm_password')
        validated = True
        MIN_PASSWORD_LENGTH = 8 # TODO: This should be a global config value.

        if not change_password and not confirm_password:
            validated = False
            if 'change_password' in cleaned_data:
                del cleaned_data['change_password']
            if 'confirm_password' in cleaned_data:
                del cleaned_data['confirm_password']

        elif len(change_password) < MIN_PASSWORD_LENGTH:
            validated = False
            msg = 'Password must be at least ' + str(MIN_PASSWORD_LENGTH) + ' characters.'
            self._errors['change_password'] = self.error_class([msg])

        elif change_password != confirm_password:
            validated = False
            msg = 'Password must match'
            self._errors['change_password'] = self.error_class([msg])
            self._errors['confirm_password'] = self.error_class([msg])

        if not validated:
            if 'change_password' in cleaned_data:
                del cleaned_data['change_password']
            if 'confirm_password' in cleaned_data:
                del cleaned_data['confirm_password']

        return cleaned_data

    def save(self, commit=True):
        user = super(UserProfileForm, self).save(commit=False)
        change_password = self.cleaned_data.get('change_password')
        if change_password:
            user.set_password(change_password)
            user.has_unusable_password = False
        if user.username and user.first_name and user.last_name and not user.has_unusable_password:
            user.profile_complete = True
        if commit:
            user.save()
            self.save_m2m()
        return user

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'picture',
                  'salutation', 'preffered_name', 'bio', 'responsibilities',
                  'email', 'phone', 'street', 'country', 'region', 'city',
                  'zip', 'linkedin_url', 'twitter_url', 'facebook_url',
                  'website_url', 'agenda_email',
                  'is_private_salutation', 'is_private_first_name',
                  'is_private_last_name', 'is_private_preffered_name',
                  'is_private_bio', 'is_private_responsibilities',
                  'is_private_email', 'is_private_phone', 'is_private_street',
                  'is_private_city', 'is_private_region',
                  'is_private_country', 'is_private_zip',
                  'is_private_linkedin_url', 'is_private_facebook_url',
                  'is_private_twitter_url', 'is_private_website_url', 'gov_department',
                  'role', 'market_segment', 'years_of_exp', 'veteran_status', 'veteran_type',
                  'is_private_role', 'is_private_market_segment', 'is_private_years_of_exp',
                  'is_private_veteran_status', 'is_private_veteran_type', 'naics', 'psc_fsc',
                  'duns', 'is_private_duns',
                  ]

        widgets = {
            'bio': forms.Textarea(),
            'responsibilities': forms.Textarea(),
            'naics': forms.MultipleHiddenInput(),
            'psc_fsc': forms.MultipleHiddenInput()
        }


class ProfileForm(forms.ModelForm):

    class Meta:
        model = User
        #exclude = ['codes']


class PDMFunctionForm(forms.ModelForm):

    class Meta:
        model = PDMFunction

    def __init__(self, *args, **kwargs):
        super(PDMFunctionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class PDMProfileForm(forms.ModelForm):

    class Meta:
        model = PDMProfile

    def __init__(self, *args, **kwargs):
        super(PDMProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class BusCategoryForm(forms.ModelForm):

    class Meta:
        model = BusCategory

    def __init__(self, *args, **kwargs):
        super(BusCategoryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class OfficeForm(forms.ModelForm):

    class Meta:
        model = Office

    def __init__(self, *args, **kwargs):
        super(OfficeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class PSCCodeForm(forms.ModelForm):

    class Meta:
        model = PSCCode

    def __init__(self, *args, **kwargs):
        super(PSCCodeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class OrganisationForm(forms.ModelForm):

    class Meta:
        model = Organisation

    def __init__(self, *args, **kwargs):
        super(OrganisationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class OrgCategoryForm(forms.ModelForm):

    class Meta:
        model = OrgCategory

    def __init__(self, *args, **kwargs):
        super(OrgCategoryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class AgencyForm(forms.ModelForm):

    class Meta:
        model = Agency

    def __init__(self, *args, **kwargs):
        super(AgencyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class DepartmentForm(forms.ModelForm):

    class Meta:
        model = Department

    def __init__(self, *args, **kwargs):
        super(DepartmentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class VOSBExperienceForm(forms.ModelForm):

    class Meta:
        model = VOSBExperience

    def __init__(self, *args, **kwargs):
        super(VOSBExperienceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class VOSBForm(forms.ModelForm):

    class Meta:
        model = VOSB

    def __init__(self, *args, **kwargs):
        super(VOSBForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class BusinessCertForm(forms.ModelForm):

    class Meta:
        model = BusinessCert

    def __init__(self, *args, **kwargs):
        super(BusinessCertForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class IndividualCertForm(forms.ModelForm):

    class Meta:
        model = IndividualCert

    def __init__(self, *args, **kwargs):
        super(IndividualCertForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class HostRegistrationForm(forms.ModelForm):

    class Meta:
        model = HostRegistration
        exclude = []

    def __init__(self, *args, **kwargs):
        super(HostRegistrationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class GPartneringNeedForm(forms.ModelForm):

    class Meta:
        model = GPartneringNeed

    def __init__(self, *args, **kwargs):
        super(GPartneringNeedForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class CPartneringNeedForm(forms.ModelForm):

    class Meta:
        model = CPartneringNeed

    def __init__(self, *args, **kwargs):
        super(CPartneringNeedForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.form_show_errors = True


class GroupForm(forms.ModelForm):

    class Meta:
        model = Group
