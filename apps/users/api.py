from util.base_tastypie import ModelResource
from apps.users import models
from tastypie.authentication import MultiAuthentication,SessionAuthentication
from tastypie.constants import ALL,ALL_WITH_RELATIONS
from tastypie.authorization import DjangoAuthorization

class UserResource(ModelResource):
    class Meta:
        queryset = models.User.objects.all()
        filtering = dict([(n,ALL_WITH_RELATIONS) for n in queryset.model._meta.get_all_field_names()])
        include_resource_uri = False
        authorization = DjangoAuthorization()
        resource_name = 'users/user'
        excludes = ['email', 'password', 'is_superuser']
        authentication = SessionAuthentication()
