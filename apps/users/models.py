import hashlib
import random
import uuid
import urlparse
import os.path

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.conf import settings

from djchoices import DjangoChoices, ChoiceItem
from cities_light.models import City, Region, Country
from select2.fields import ManyToManyField as s2_ManyToManyField
from smart_selects.db_fields import ChainedForeignKey
from redactor.fields import RedactorField
from mailUtil import send_templated_email
from sorl.thumbnail import get_thumbnail, ImageField
from sorl.thumbnail.helpers import ThumbnailError


from apps.company.models import (
    CVGPOrgProfile, Industry, GVGPOrgProfile, NSCode, PSCCode
)

ACTIVATION_STATUS = ((1, "ACTIVE"),
                     (2, "INACTIVE"),
                     (0, "PENDING"))

CONTRACT_VEHICLE = ((1, "MOBIS"),
                    (2, "CEC"),
                    (3, "Schedule 70"),
                    (4, "T4"),
                    (5, "GWAC"))

PRIME_SUB = (("1", "Prime"),
             ("2", "Sub"),)


class ActivationMethodChoices(DjangoChoices):
    EMAIL = ChoiceItem('1', 'Email')
    SMS = ChoiceItem('2', 'Phone - SMS')


class VGPTypeChoices(DjangoChoices):
    GOV = ChoiceItem('1', 'Government')
    COM = ChoiceItem('2', 'Commercial')


class VOSBTypeChoices(DjangoChoices):
    VOSB = ChoiceItem(1, 'VOSB')
    VOSBE = ChoiceItem(2, 'VOSB Experience')

class RoleChoices(DjangoChoices):
    EXE = ChoiceItem('EXE', 'EXE')
    PDM = ChoiceItem('PDM', 'PDM')
    PDMC = ChoiceItem('PDMC', 'PDMC')
    PDMP = ChoiceItem('PDMP', 'PDMP')
    SBS = ChoiceItem('SBS', 'SBS')
    SES = ChoiceItem('SES', 'SES')
    SESC = ChoiceItem('SESC', 'SESC')
    SESP = ChoiceItem('SESP', 'SESP')
    SESS = ChoiceItem('SESS', 'SESS')
    SPT = ChoiceItem('SPT', 'SPT')
    LDR = ChoiceItem('LDR', 'LDR')
    SPK = ChoiceItem('SPK', 'SPK')
    PART = ChoiceItem('PART', 'PART')

class MarketSegmentChoices(DjangoChoices):
    SELECTED = ChoiceItem('SELECTED', 'Selected')
    NOT_SELECTED = ChoiceItem('NOT_SELECTED', 'Not Selected')
    INTERMEDIATE = ChoiceItem('INTERMEDIATE', 'Indeterminate')

class VeteranTypeChoices(DjangoChoices):
    SELECTED = ChoiceItem('SELECTED', 'Selected')
    NOT_SELECTED = ChoiceItem('NOT_SELECTED', 'Not Selected')
    INTERMEDIATE = ChoiceItem('INTERMEDIATE', 'Indeterminate')

class VeteranStatus(DjangoChoices):
    Active = ChoiceItem('active', 'Active')
    Disabled = ChoiceItem('disabled', 'Disabled')

TOKEN = (("2", "PASSWORD_RESET"),
         ("1", "ACCOUNT_ACTIVATION"),
         ("3", "EMAIL_CHANGE"),)


class PDMFunction(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Role(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class BusCategory(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class User(AbstractUser):

    picture = models.ImageField(upload_to='profile', blank=True, null=True)

    # TODO: change to small integer fields and djchoices
    vgp_type = models.CharField(
        max_length=1, choices=VGPTypeChoices.choices,
        help_text='Commercial or Government', default=VGPTypeChoices.GOV
    )

    vosb_type = models.SmallIntegerField(choices=VOSBTypeChoices.choices, help_text='Type of Commercial User',
        blank=True, null=True,default=1)

    salutation = models.CharField(max_length=255, blank=True, null=True)
    preffered_name = models.CharField('Preferred name', max_length=255, blank=True, null=True)
    role = models.CharField(max_length=255, blank=True, null=True, choices=RoleChoices.choices)
    naics = models.ManyToManyField(
        NSCode, blank=True, null=True, related_name='users',
        # comment line below if you have problems with creating schema migration
        #ajax=True, search_field=lambda q: models.Q(code__icontains=q) | models.Q(about__icontains=q)
    )
    psc_fsc = models.ManyToManyField(
        PSCCode, blank=True, null=True, related_name='users',
        # comment line below if you have problems with creating schema migration
        #ajax=True, search_field=lambda q: models.Q(code__icontains=q) | models.Q(about__icontains=q)
    )
    market_segment = models.CharField(max_length=255, blank=True, null=True, choices=MarketSegmentChoices.choices)
    org_creator = models.CharField(max_length=255, blank=True, null=True)
    veteran_status = models.CharField(max_length=255, blank=True, null=True, choices=VeteranStatus.choices)
    veteran_type = models.CharField(max_length=255, blank=True, null=True, choices=VeteranTypeChoices.choices)
    branch = models.CharField(max_length=255, blank=True, null=True)
    bio = RedactorField(blank=True, null=True)
    responsibilities = RedactorField(blank=True, null=True)
    years_of_exp = models.IntegerField('Years Of Experience', default=0, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    street = models.CharField(max_length=255, blank=True, null=True)
    gov_department = models.ForeignKey(GVGPOrgProfile, blank=True, null=True)
    commmercial_org = models.ForeignKey(CVGPOrgProfile, blank=True, null=True)
    rfid = models.CharField(max_length=255, blank=True, null=True)
    duns = models.CharField(max_length=255, verbose_name='DUNS', blank=True, null=True)

    # TODO: change to integer
    reg_code = models.CharField(
        max_length=255, blank=True, null=True,
        default=lambda: uuid.uuid4().get_time_mid())
    verified = models.BooleanField(default=False)
    # TODO: change to small integer fields and djchoices
    activation_method = models.CharField(
        max_length=1, blank=True, null=True,
        choices=ActivationMethodChoices.choices)
    agenda_email = models.BooleanField(
        default=False, verbose_name='Receive daily agenda emails?')
    has_unusable_password = models.BooleanField(default=False)

    # Location
    country = models.ForeignKey(Country, null=True, blank=True)
    region = ChainedForeignKey(
        Region,
        chained_field="country",
        chained_model_field="country",
        show_all=False,
        auto_choose=True, null=True, blank=True,
        verbose_name='State'
    )

    city = ChainedForeignKey(
        City,
        chained_field="region",
        chained_model_field="region",
        show_all=False,
        auto_choose=True, null=True, blank=True
    )
    zip = models.CharField("Zip", max_length=255, blank=True, null=True)

    # Links
    linkedin_url = models.URLField('LinkedIn link', blank=True, null=True)
    facebook_url = models.URLField('Facebook link', blank=True, null=True)
    twitter_url = models.URLField('Twitter link', blank=True, null=True)
    website_url = models.URLField('Website', blank=True, null=True)

    # Privacy
    profile_complete = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    # Privacy for fields
    is_private_salutation = models.BooleanField(default=False)
    is_private_first_name = models.BooleanField(default=False)
    is_private_last_name = models.BooleanField(default=False)
    is_private_preffered_name = models.BooleanField(default=False)
    is_private_bio = models.BooleanField(default=False)
    is_private_responsibilities = models.BooleanField(default=False)
    is_private_email = models.BooleanField(default=False)
    is_private_phone = models.BooleanField(default=False)
    is_private_street = models.BooleanField(default=False)
    is_private_city = models.BooleanField(default=False)
    is_private_region = models.BooleanField(default=False)
    is_private_country = models.BooleanField(default=False)
    is_private_zip = models.BooleanField(default=False)
    is_private_linkedin_url = models.BooleanField(default=False)
    is_private_facebook_url = models.BooleanField(default=False)
    is_private_twitter_url = models.BooleanField(default=False)
    is_private_website_url = models.BooleanField(default=False)
    is_private_role = models.BooleanField(default=False)
    is_private_market_segment = models.BooleanField(default=False)
    is_private_years_of_exp = models.BooleanField(default=False)
    is_private_veteran_status = models.BooleanField(default=False)
    is_private_veteran_type = models.BooleanField(default=False)
    is_private_duns = models.BooleanField(default=False)

    def __unicode__(self):
        return self.username

    @property
    def is_gov(self):
        if self.vgp_type:
            return self.vgp_type == VGPTypeChoices.GOV
        return None

    @property
    def is_com(self):
        if self.is_gov is None:
            return None
        return not self.is_gov

    @property
    def picture_file_name(self):
        if self.picture:
            return os.path.basename(self.picture.name)
        return ''

    @property
    def company(self):
        if self.is_gov:
            return self.gov_department

        return self.commmercial_org

    def get_absolute_url(self):
            return reverse('profile', kwargs={'pk': self.pk})

    def full_name(self):
        if self.first_name or self.last_name:
            return "%s %s" % (self.first_name, self.last_name)
        return self.username
    
    def get_picture_thumbnail(self):
        if self.picture:
            try:
                return get_thumbnail(self.picture, settings.PROFILE_PICTURE_THUMBNAIL_SIZE, crop='center')
            except (IOError, ThumbnailError):
                pass
        return None

    def get_picture_thumbnail_url(self):
        thumb = self.get_picture_thumbnail()
        if thumb:
            return thumb.url
        return ''
    
    def get_small_picture_thumbnail(self):
        if self.picture:
            try:
                return get_thumbnail(self.picture, settings.SMALL_PROFILE_PICTURE_THUMBNAIL_SIZE, crop='center')
            except (IOError, ThumbnailError):
                pass
        return None

    def get_small_picture_thumbnail_url(self):
        thumb = self.get_small_picture_thumbnail()
        if thumb:
            return thumb.url
        return ''


class GPartneringNeed(models.Model):
    vgp_user = models.ForeignKey(GVGPOrgProfile)
    visibility = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='Visibility of Business Requirement'
    )
    name = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='Business Requirement name'
    )
    description = RedactorField(
        blank=True, null=True,
        verbose_name='Visibility of Business Requirement'
    )
    market_segment = models.CharField(max_length=255, blank=True, null=True)
    client = models.CharField(max_length=255, blank=True, null=True)
    poc = models.CharField(max_length=255, blank=True, null=True)
    incubent_contractor = models.CharField(
        max_length=255, blank=True, null=True)
    estimated_value = models.CharField(max_length=255, blank=True, null=True)
    procurement_method = models.CharField(
        max_length=255, blank=True, null=True)
    current_exp_date = models.DateField(
        blank=True, null=True, verbose_name='Current contract expiration date'
    )
    estimated_start_date = models.DateField(blank=True, null=True)
    security_requirement = models.DateField(blank=True, null=True)
    documents = models.FileField(upload_to='documents', blank=True, null=True)
    small_business_types = models.CharField(
        max_length=255, blank=True, null=True)
    place_of_work = models.CharField(max_length=255, blank=True, null=True)
    geographic_constraints = RedactorField(blank=True, null=True)
    capabilities_constraints = RedactorField(blank=True, null=True)
    relevant_links = RedactorField(blank=True, null=True)


class HostRegistration(models.Model):
    user = models.ForeignKey(User)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    primary_function = models.ForeignKey(PDMFunction)
    arrival = models.DateTimeField(blank=True, null=True)
    departure = models.DateField(blank=True, null=True)
    industry = models.ForeignKey(Industry, blank=True, null=True)
    primary_naics = models.ForeignKey(NSCode, related_name='host_p_naics')
    secondary_naics = models.ForeignKey(NSCode, related_name='host_s_naics')
    tertiary_naics = models.ForeignKey(NSCode, related_name='host_t_naics')
    other = models.ForeignKey(NSCode, related_name='other_t_naics')
    duns = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='DUNS Number'
    )

    def __unicode__(self):
        return "{0}  {1}".format(self.user.first_name, self.last_name)


class IndividualCert(models.Model):
    certification_name = models.CharField(max_length=255, blank=True, null=True)
    date_obtained = models.DateField()
    user = models.ForeignKey(User)
    document = models.FileField(upload_to='documents', blank=True, null=True)
    
    @property
    def file_name(self):
        if self.document:
            return os.path.basename(self.document.name)
        return ''


class PDMProfile(models.Model):
    user = models.ForeignKey(User)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    naics = models.ForeignKey(NSCode, related_name='pdm_profile_naics')
    description = RedactorField(blank=True, null=True)
    exp_s_timing = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='Expected Solicitation Timing'
    )
    estimated_value = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='Estimated value of contract'
    )

    def __unicode__(self):
        return self.user.username


class VOSB(models.Model):
    user = models.ForeignKey(User, related_name='vosb_creator')
    organisation = models.ForeignKey(CVGPOrgProfile)
    duns = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='DUNS Number'
    )
    primary_naics = models.ForeignKey(NSCode, related_name='vosb_p_naics')
    secondary_naics = models.ForeignKey(NSCode, related_name='vosb_s_naics')
    tertiary_naics = models.ForeignKey(NSCode, related_name='vosb_t_naics')
    industry = models.ForeignKey(Industry)
    # TODO: change to small integer fields and djchoices
    contract_vehicle = models.CharField(
        max_length=1, blank=True, null=True, choices=CONTRACT_VEHICLE)
    # TODO: change to small integer fields and djchoices
    prime_sub = models.CharField(
        max_length=1, blank=True, null=True,
        choices=PRIME_SUB, verbose_name='Prime/Sub')

    employees = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='No of Vet Employees'
    )
    vet_employees = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name='No of Vet Employees'
    )
    dcaa_compliant = models.BooleanField(default=False)
    second_core_strength = models.CharField(
        max_length=255, blank=True, null=True)
    third_core_strength = models.CharField(
        max_length=255, blank=True, null=True)
    # past Perfomance
    total_cost = models.CharField(max_length=255, blank=True, null=True)
    pop = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='PoP')
    contract = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='PoP')
    customer = models.CharField(
        max_length=255, blank=True, null=True,
        help_text='Customer/Department/Firm'
    )


class VOSBExperience(models.Model):
    user = models.ForeignKey(User, related_name='vosb_experience')
    organisation = models.ForeignKey(CVGPOrgProfile)
    duns = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='DUNS Number')
    naics = models.ManyToManyField(NSCode, related_name='vosb_exp_naics')
    # TODO: change to small integer fields and djchoices
    contract_vehicle = models.CharField(
        max_length=1, blank=True, null=True, choices=CONTRACT_VEHICLE)
    # TODO: change to small integer fields and djchoices
    prime_sub = models.CharField(
        max_length=1, blank=True, null=True,
        choices=PRIME_SUB, verbose_name='Prime/Sub')
    first_core_strength = models.CharField(
        max_length=255, blank=True, null=True)
    second_core_strength = models.CharField(
        max_length=255, blank=True, null=True)
    third_core_strength = models.CharField(
        max_length=255, blank=True, null=True)
    # past Perfomance
    total_cost = models.CharField(max_length=255, blank=True, null=True)
    pop_start = models.DateField(
        max_length=255, blank=True, null=True, verbose_name='Past Perfomance(POP)  Start')
    pop_finish = models.DateField(
        max_length=255, blank=True, null=True, verbose_name='Past Perfomance(POP)  End' )
    contract = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='contract')
    customer = models.CharField(
        max_length=255, blank=True, null=True,
        help_text='Customer/Department/Firm'
    )


class ActivationCode(models.Model):
    user = models.ForeignKey(User)
    token = models.CharField(max_length=255, unique=True)
    expired = models.BooleanField(default=False)
    created_at = models.DateField(auto_now_add=True)
    token_type = models.CharField(max_length=1, choices=TOKEN, default=1)

    def __unicode__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        # Make the activation token
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        username = self.user.username.encode('utf-8')
        token = hashlib.sha1(salt+username).hexdigest()

        if not self.token:
            self.token = token

        super(ActivationCode, self).save(*args, **kwargs)


class ResetCode(models.Model):
    user = models.ForeignKey(User)
    token = models.CharField(max_length=255, unique=True)
    expired = models.BooleanField(default=False)
    created_at = models.DateField(auto_now_add=True)
    # TODO: change to small integer fields and djchoices
    token_type = models.CharField(max_length=1, choices=TOKEN,default=1)

    def __unicode__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        # Make the activation token
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        username = self.user.username.encode('utf-8')
        token = hashlib.sha1(salt+username).hexdigest()
        if not self.token:
            self.token = token
        super(ResetCode, self).save(*args, **kwargs)


# TODO: move to helpers
def activation_email(activation_code, user):
        url = urlparse.urljoin(settings.SITE_URL,
                               reverse('users_confirm', args=[activation_code]))

        try:
            from string import Template
            send_templated_email(
                subject="Welcome to BusinessMatchMakingSoftware",
                email_template_name='templated_email/reg_email.html',
                sender=settings.NOREPLY_EMAIL,
                recipients=user.email,
                email_context={
                    'activation_url': url,
                    'username': user.username,
                    'reg_code': user.reg_code
                }
            )
        except Exception:
            pass


# TODO: move to helpers
def reset_email(activation_code, username, email):
        current_site = Site.objects.get_current()

        HOSTNAME = current_site.domain
        url = "http://" + HOSTNAME + "/reset/" + activation_code+'/'
        host = "http://" + HOSTNAME + "/static/"

        try:
            from string import Template
              # email_body_tmpl = Template(mtn.sms_reply_email_body)
              # email_body = email_body_tmpl.substitute(name=sender_name)
            send_templated_email(
                subject="Reset your BusinessMatchMaking Account Password",
                email_template_name='templated_email/reset_email.html',
                sender='no-reply@businessmatchmakingsoftware.com',
                recipients=email,
                email_context={
                    'activation_url': url,
                    'username': username,
                    'host': host

                }
            )
        except Exception:
            pass


# TODO: move to signals
# probably we don't need these signals
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        ActivationCode.objects.create(user=instance)


# TODO: move to signals
# probably we don't need these signals
def send_acc_activation_email(sender, instance, created, **kwargs):
    if created:
        user = User.objects.get(id=instance.user.id)

        activation_email(instance.token, user.username, user.email)


# TODO: move to signals
def send_reset_email(sender, instance, created, **kwargs):
    if created:
        user = User.objects.get(id=instance.user.id)

        reset_email(instance.token, user.username, user.email)


class Question(models.Model):

    TEXT = 'text'
    RADIO = 'radio'
    SELECT = 'select'
    SELECT_MULTIPLE = 'select-multiple'
    INTEGER = 'int'
    CHARS = 'char'

    QUESTION_TYPES = (
        (TEXT, 'text'),
        (RADIO, 'radio'),
        (SELECT, 'select'),
        (SELECT_MULTIPLE, 'Select Multiple'),
        (INTEGER, 'Number'),
        (CHARS, 'Characters'),
    )

    name = models.TextField()
    required = models.BooleanField()
    active = models.BooleanField(default=True)
    # TODO: change to small integer fields and djchoices
    question_type = models.CharField(
        max_length=200, choices=QUESTION_TYPES, default=TEXT)
    # the choices field is only used if the question type
    choices = models.TextField(
        blank=True, null=True,
        help_text='if the question type is "radio," "select," or '
                  '"select multiple" provide a comma-separated '
                  'list of options for this question .'
    )

    def get_choices(self):
        """
        parse the choices field and return a tuple formatted appropriately
        for the 'choices' argument of a form widget.
        """
        choices = self.choices.split(',')
        choices_list = []
        for c in choices:
            #choices_list.append((c,c))
            choices_list.append(c)
        #choices_tuple = tuple(choices_list)

        return choices_list

    def __unicode__(self):
        return self.question_type


class Response(models.Model):
    # a response object is just a collection of questions and answers with a
    # unique interview uuid
    user = models.ForeignKey(User)
    question = models.ForeignKey(Question)
    value = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{0}:{1}#{2}'.format(
            self.user.username, self.question.name, self.value)


class InvalidLogin(models.Model):
    # Object, which collect ip addresses after failed logins
    MAX_MISTAKES_COUNT = 10000

    ip_address = models.CharField(max_length=64)
    username = models.CharField(max_length=255, blank=True, null=True)
    effort_count = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{0}:{1}#{2}'.format(
            self.user.username, self.question.name, self.value)

#TODO probably we don't need these signals
#post_save.connect(create_user_profile, sender=User)
#post_save.connect(send_acc_activation_email, sender=ActivationCode)
post_save.connect(send_reset_email, sender=ResetCode)


class UserTrack(models.Model):
    user = models.ForeignKey(User,  blank=True, null=True)
    active = models.BooleanField(default=True)

