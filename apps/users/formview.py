from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings

from apps.users.models import NSCode
from apps.users.models import Question
from apps.users.forms import ProfileForm

ACCOUNT_SID = settings.TWILIO_SID
AUTH_TOKEN = settings.TWILIO_TOKEN


def DProfile(request):

    profile = request.user

    form = ProfileForm(instance=profile)
    codes = NSCode.objects.all()
    qs = Question.objects.all()

    return render_to_response(
        'users/../../templates/profile/edit_profile.html', locals(),
        RequestContext(request))


def CreateFields(request):

    profile = request.user

    form = ProfileForm(instance=profile)
    codes = NSCode.objects.all()
    qs = Question.objects.all()

    return render_to_response(
        'users/create_fields.html', locals(), RequestContext(request))

