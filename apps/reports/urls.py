from django.conf.urls import patterns, include, url

from django.conf import settings
from django.views.generic import TemplateView


urlpatterns = patterns('',
    url(r'^activity/$', 'apps.reports.views.ActivityEventList', name='activitiy_event_list'),
    url(r'^activity/details/(?P<id>\d+)/$', 'apps.reports.views.ActivityDetails', name='activitiy_details'),
    url(r'^dynamic/$', 'apps.reports.views.LiveActivitiesList', name='activities_list'),
    url(r'^dynamic/(?P<id>\d+)/$', 'apps.reports.views.ActivityList', name='attendees_list'),
    url(r'^activity/attendee/(?P<id>\d+)/$', 'apps.reports.views.ActivityDetail', name='activities_detail'),
    url(r'^activity/activityattendee/(?P<id>\d+)/$', 'apps.reports.views.ActivityAttendeeDetail', name='activity_attendee_detail'),
    url(r'^activity/(?P<id>\d+)/$', 'apps.reports.views.ActivitiesList', name='activities_list'),
    url(r'^host/$', 'apps.reports.views.HostList', name='host_list'),
    url(r'^schedule/$','apps.reports.views.ScheduleEventList', name='schedule_list'),
    url(r'^schedule/(?P<id>\d+)/$','apps.reports.views.SchedulesDetail', name='schedules_detail'),
    url(r'^schedule/export/(?P<id>\d+)/$', 'apps.reports.views.ScheduleExport', name='export2Csv'),
    url(r'^business/$','apps.reports.views.BusinessList',name='business_list'),
    url(r'^business/json/$','apps.reports.views.IndustryDetails',name='industry_details'),
    url(r'^events/$','apps.reports.views.EventList',name='event_list'),
    url(r'^events/(?P<id>\d+)/$','apps.reports.views.EventDetail',name='activity_detail'),
    url(r'^activity/export/(?P<id>\d+)/$','apps.reports.views.ActivityExport',name='activity_export'),
    url(r'^events/export/$','apps.reports.views.EventExport',name='event_export'),
    url(r'^admin/$','apps.reports.views.AdminReport',name='admin_report'),
    url(r'^admin/(?P<id>\d+)/$','apps.reports.views.ActivityReport',name='activity_report')
)