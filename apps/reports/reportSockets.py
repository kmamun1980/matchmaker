from geventwebsocket import WebSocketServer, WebSocketApplication, Resource
import json,time,random
from datetime import timedelta

class ServerReport(WebSocketApplication):
    
    def on_open(self):
        print "Connection opened"
        while True:
            graphData={}
            graphData['data1']=random.randint(1, 10)
            graphData['data2']=random.randint(1, 10)
            self.ws.send(json.dumps(graphData))
            time.sleep(5)

    def on_message(self, message):
        print message
        self.broadcast(message)

    def on_close(self, reason):
        print reason

    def broadcast(self, message):
        for client in self.ws.handler.server.clients.values():
            client.ws.send(message) 

class ActivityReport(WebSocketApplication):
    
    def on_open(self):
        print "Connection opened"
        while True:
            graphData={}
            graphData['data1']=time.strftime("%Y-%m-%d")
            graphData['data2']=random.randint(1, 4)
            self.ws.send(json.dumps(graphData))
            time.sleep(5)

    def on_message(self, message):
        print message
        self.broadcast(message)

    def on_close(self, reason):
        print reason

    def broadcast(self, message):
        for client in self.ws.handler.server.clients.values():
            client.ws.send(message) 

class BusinessReport(WebSocketApplication):
    
    def on_open(self):
        print "Connection opened"
        while True:
            graphData={}
            graphData['data1']=random.randint(1, 10)
            graphData['data2']=random.randint(1, 10)
            self.ws.send(json.dumps(graphData))
            time.sleep(5)

    def on_message(self, message):
        print message
        self.broadcast(message)

    def on_close(self, reason):
        print reason

    def broadcast(self, message):
        for client in self.ws.handler.server.clients.values():
            client.ws.send(message) 

class DynamicReport(WebSocketApplication):
    
    def on_open(self):
        print "Connection opened"
        while True:
            graphData={}
            graphData['data1']=random.randint(1, 2)
            graphData['data2']=random.randint(1, 2)
            self.ws.send(json.dumps(graphData))
            time.sleep(5)

    def on_message(self, message):
        print message
        self.broadcast(message)

    def on_close(self, reason):
        print reason

    def broadcast(self, message):
        for client in self.ws.handler.server.clients.values():
            client.ws.send(message) 

class EventReport(WebSocketApplication):
    
    def on_open(self):
        print "Connection opened"
        while True:
            graphData={}
            graphData['data1']=random.randint(1, 10)
            graphData['data2']=random.randint(1, 10)
            self.ws.send(json.dumps(graphData))
            time.sleep(5)

    def on_message(self, message):
        print message
        self.broadcast(message)

    def on_close(self, reason):
        print reason

    def broadcast(self, message):
        for client in self.ws.handler.server.clients.values():
            client.ws.send(message) 


class HostReport(WebSocketApplication):
    
    def on_open(self):
        print "Connection opened"
        while True:
            graphData={}
            graphData['data1']=random.randint(1, 10)
            graphData['data2']=random.randint(1, 10)
            self.ws.send(json.dumps(graphData))
            time.sleep(5)

    def on_message(self, message):
        print message
        self.broadcast(message)

    def on_close(self, reason):
        print reason

    def broadcast(self, message):
        for client in self.ws.handler.server.clients.values():
            client.ws.send(message) 

class ScheduleReport(WebSocketApplication):
    
    def on_open(self):
        print "Connection opened"
        while True:
            graphData={}
            graphData['data1']=random.randint(1, 10)
            graphData['data2']=random.randint(1, 10)
            self.ws.send(json.dumps(graphData))
            time.sleep(5)

    def on_message(self, message):
        print message
        self.broadcast(message)

    def on_close(self, reason):
        print reason

    def broadcast(self, message):
        for client in self.ws.handler.server.clients.values():
            client.ws.send(message) 




