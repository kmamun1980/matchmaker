from django.core.management import BaseCommand
from geventwebsocket import WebSocketServer, Resource
from apps.reports.reportSockets import (ServerReport, ActivityReport, DynamicReport, BusinessReport, EventReport,
    HostReport, ScheduleReport)
from django.conf import settings

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        WebSocketServer(
            ('', settings.WEBSOCKET_PORT),
            Resource({'/': ServerReport, '/activity': ActivityReport,
                      '/business': BusinessReport, '/dynamic': DynamicReport,
                      '/events': EventReport, '/host': HostReport, '/schedule': ScheduleReport}),
        ).serve_forever()