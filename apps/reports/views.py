from django.contrib.auth.decorators import login_required
from PIL import Image
from django.http import HttpResponse, HttpResponseRedirect,Http404
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from apps.events.models import Attendee, Activity, ActivityAttendee, Host, Facility, Event
from apps.users.models import User, UserTrack
from apps.match.models import Schedule,Session
from apps.company.models import Industry
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
import json
from django.core import serializers
import requests,random,time
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from django.db.models import Q
from django.conf import settings


@login_required

def ActivityEventList(request):
    events = Event.objects.all().order_by('id')
    websocket_port = settings.WEBSOCKET_PORT
    return render_to_response('reports/activities_reports.html', locals(), RequestContext(request))

def ActivitiesList(request,id):
    activities = Activity.objects.filter(event=id)
    data = []
    for activity in activities:
        activityData = {}
        activityData['id'] = activity.id
        activityData['name'] = activity.name
        
        data.append(activityData)
    return HttpResponse(json.dumps(data),mimetype='application/json')

def ActivityDetails(request,id):
    activities = Activity.objects.filter(id=id)
    data = []
    for activity in activities:
        activityData = {}
        activityData['id'] = activity.id
        activityData['name'] = activity.name
        activityData['type'] = activity.type.short_name
        activityData['created_by'] = activity.created_by.username
        activityData['description'] = activity.description
        activityData['location'] = activity.location
        if activity.start_time:
            activityData['start_time'] = activity.start_time.strftime("%Y-%m-%d")
        else:
            activityData['start_time'] = ''
        if activity.end_time:
            activityData['end_time'] = activity.end_time.strftime("%Y-%m-%d")
        else:
            activityData['end_time'] = ''
        data.append(activityData)
    return HttpResponse(json.dumps(data),mimetype='application/json')

def ActivityList(request,id):
    registrant_count = ActivityAttendee.objects.filter(activity=id).count()
    data = []
    activities = Activity.objects.filter(id=id).values()
    for activity in activities:
        activityData = {}
        activityData['event'] = activity.get('event_id');
        activityData['activityname'] = activity.get('name')
        activityData['description'] = activity.get('description')
        activityData['date'] = activity.get('start_time').strftime("%Y-%m-%d")
        activityData['registrantcount'] = registrant_count
        activityData['capacity'] = activity.get('limit')
        activityData['location'] = activity.get('location')
        data.append(activityData)
    return HttpResponse(json.dumps(data),mimetype='application/json')

def ActivityAttendeeDetail(request,id):
    activities_attendees = ActivityAttendee.objects.filter(activity=id).values()
    data = []
    for attendees in activities_attendees:
        attendesData={}
        attendesData['id'] = attendees.get('id')
        attendesData['user_id'] = attendees.get('user_id')
        attendesData['active'] = attendees.get('active')
        attendesData['user'] = {}
        user = User.objects.filter(id=attendees.get('user_id')).values()
        user=user[0]
        attendesData['user']['first_name'] = user.get('first_name') 
        attendesData['user']['last_name'] = user.get('last_name') 
        attendesData['user']['phone'] = user.get('phone') 
        attendesData['user']['company'] = user.get('last_name') 
        attendesData['user']['position'] = user.get('last_name') 
        if user.get('gov_department_id') :
            attendesData['user']['user_type'] = 'Gov'
        elif user.get('commmercial_org_id') :
            attendesData['user']['user_type'] = 'Commercial'
        attendesData['user']['role'] = user.get('role') 
        attendesData['created'] = attendees.get('created').strftime("%Y-%m-%d")
        attendesData['stamp'] = attendees.get('created').strftime("%Y-%m")
        print attendesData
        data.append(attendesData)
    return HttpResponse(json.dumps(data), mimetype='application/json')

def ActivityDetail(request,id):
    registrants = ActivityAttendee.objects.filter(activity=id).count()
    cancelees = ActivityAttendee.objects.filter(activity=id,active=0).count()
    attendeesCount = registrants - cancelees
    activities_attendees = ActivityAttendee.objects.filter(activity=id)
    activities_attendees_count = ActivityAttendee.objects.all().count()
    activity_count = Activity.objects.all().count()
    data = []
    for attendees in activities_attendees:
        activityData={}
        if attendees.created.strftime("%Y-%m-%d"):
            print attendees.created.strftime("%Y-%m-%d")
            activityData['created'] = attendees.created.strftime("%Y-%m-%d")
        else:
            activityData['created'] = ''
        activityData['registrants'] = registrants
        activityData['attendees'] = attendeesCount
        activityData['cancelees'] = cancelees
        activityData['allattendees'] = activities_attendees_count
        activityData['activities'] = activity_count
        activityData['name'] = attendees.activity.name
        data.append(activityData)
    return HttpResponse(json.dumps(data),mimetype='application/json')

def HostList(request):
    websocket_port = settings.WEBSOCKET_PORT
    hosts = Host.objects.all().order_by('id')
    return render_to_response('reports/host_reports.html', locals(), RequestContext(request))

def LiveActivitiesList(request):
    hosts = Host.objects.all().order_by('id')
    websocket_port = settings.WEBSOCKET_PORT
    activities = Activity.objects.all().order_by('id')
    values1 = [random.randint(1, 10),random.randint(1, 10),random.randint(1, 10),random.randint(1, 10),random.randint(1, 10)]
    values2 = [random.randint(1, 10),random.randint(1, 10),random.randint(1, 10),random.randint(1, 10),random.randint(1, 10)]
    return render_to_response('reports/dynamic_reports.html', locals(), RequestContext(request))

def LiveAttendeesList(request,id):
    registrant_count = ActivityAttendee.objects.filter(activity=id).count()
    data = []
    activities = Activity.objects.filter(id=id).values()
    for activity in activities:
        activityData = {}
        activityData['event'] = activity.get('event_id');
        activityData['activityname'] = activity.get('name')
        activityData['description'] = activity.get('description')
        activityData['date'] = activity.get('start_time').strftime("%Y-%m-%d")
        activityData['registrantcount'] = registrant_count
        activityData['capacity'] = activity.get('limit')
        activityData['location'] = activity.get('location')
        data.append(activityData)
    return HttpResponse(json.dumps(data),mimetype='application/json')

def ScheduleEventList(request):
    events = Event.objects.all().order_by('id')
    websocket_port = settings.WEBSOCKET_PORT
    return render_to_response('reports/schedule_reports.html', locals(), RequestContext(request))


def SchedulesDetail(request,id):
    schedules   = Schedule.objects.filter(activity=id).values()
    data        = []
    for schedule in schedules:
        scheduleid = schedule.get('id')
        sessions    = Session.objects.filter(schedule=scheduleid).values()
        activity    = Activity.objects.get(id=id)
        facilityid  = activity.facility
        building    = activity.facility.name
        for session in sessions:
            scheduleid              = session.get('schedule')
            sessionData             = {}
            sessionData['seller']   = session.get('seller');
            sessionData['buyer']    = session.get('buyer')
            sessionData['start']    = session.get('start')
            sessionData['end']      = session.get('end')
            sessionData['building'] = building
            data.append(sessionData)
    return HttpResponse(json.dumps(data),mimetype='application/json')

def export2Csv(request,id):
    import json
    import csv
    schedules   = Schedule.objects.filter(activity=id).values()
    data        = []
    for schedule in schedules:
        scheduleid = schedule.get('id')
        sessions    = Session.objects.filter(schedule=scheduleid).values()
        activity    = Activity.objects.get(id=id)
        facilityid  = activity.building
        building    = activity.building.name
        for session in sessions:
            scheduleid              = session.get('schedule')
            sessionData             = {}
            sessionData['seller']   = session.get('seller');
            sessionData['buyer']    = session.get('buyer')
            sessionData['start']    = session.get('start')
            sessionData['end']      = session.get('end')
            sessionData['building'] = building
            data.append(sessionData)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="schedule.csv"'
    writer = csv.writer(response)   
    writer.writerow(['Room', 'Table', 'Meeting Time', 'Meet with', ' Matching Criteria'])
    for item in data:
        writer.writerow([item['building'], '' ,item['start']+' - '+item['end'],item['buyer']+' - '+item['seller'],''])

    return response

def ActivityExport(request,id):
    import json
    import csv
    activities = Activity.objects.filter(event=id)
    data        = []
    for activity in activities:
        activityData = {}
        activityData['name'] = activity.name
        activityData['type'] = activity.type
        activityData['created_by'] = activity.created_by.username
        activityData['description'] = activity.description
        activityData['location'] = activity.location
        activityData['start_time'] = activity.start_time.strftime("%Y-%m-%d")
        activityData['end_time'] = activity.end_time.strftime("%Y-%m-%d")
        data.append(activityData)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Activity.csv"'
    writer = csv.writer(response) 
    row1 = ['Name Type Created_by Description Location Start_Time End_Time']
    # row1.append()  
    writer.writerow(row1)
    for item in data:
        writer.writerow([item['name']+' '+item['type']+' '+item['created_by']+' '+item['description']+' '+item['location']+' '+item['start_time']+' '+item['end_time']])
    return response

def EventExport(request):
    import json
    import csv
    events = Event.objects.all().order_by('id')
    data = []
    for event in events:
        eventData = {}
        eventData['name'] = event.name
        eventData['start_time'] = event.start_time.strftime("%Y-%m-%d")
        eventData['end_time'] = event.end_time.strftime("%Y-%m-%d")
        eventData['reg_start'] = event.reg_start.strftime("%Y-%m-%d")
        eventData['reg_end'] = event.reg_end.strftime("%Y-%m-%d")
        eventData['limit'] = str(event.limit)
        eventData['phone'] = str(event.phone)
        eventData['contact'] = str(event.contact)
        data.append(eventData)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Events.csv"'
    writer = csv.writer(response) 
    row1 = ['Name Start_Time End_Time Registration_Start Registration_End Limit Phone Contact']
    # row1.append()  
    writer.writerow(row1)
    for item in data:
        writer.writerow([item['name']+' '+item['start_time']+' '+item['end_time']+' '+item['reg_start']+' '+item['reg_end']+' '+item['limit']+' '+item['phone']+' '+item['contact']])

    return response


def ScheduleExport(request,id):
    import json
    import csv
    schedules   = Schedule.objects.filter(activity=id).values()
    data        = []
    for schedule in schedules:
        scheduleid = schedule.get('id')
        sessions    = Session.objects.filter(schedule=scheduleid).values()
        activity    = Activity.objects.get(id=id)
        facilityid  = activity.building
        building    = activity.building.name
        for session in sessions:
            scheduleid              = session.get('schedule')
            sessionData             = {}
            sessionData['seller']   = session.get('seller');
            sessionData['buyer']    = session.get('buyer')
            sessionData['start']    = session.get('start')
            sessionData['end']      = session.get('end')
            sessionData['building'] = building
            data.append(sessionData)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Schedule.csv"'
    writer = csv.writer(response)   
    writer.writerow(['Room', 'Table', 'Meeting Time', 'Meet with', ' Matching Criteria'])
    for item in data:
        writer.writerow([item['building'], '' ,item['start']+' - '+item['end'],item['buyer']+' - '+item['seller'],''])
    return response

def BusinessList(request):
    industries = Industry.objects.all().order_by('id')
    activities = Activity.objects.all().order_by('id')
    websocket_port = settings.WEBSOCKET_PORT
    data = []
    total = 0
    for industry in industries:
        industryData = {}
        industryData['name'] = industry.name
        count = 0
        for activity in activities:
            i=0
            while i<activity.industry.count():
                if(industry.name==activity.industry.all()[i].name):
                    count=count+1
                i=i+1

        industryData['count'] = count
        total = total+count
        data.append(industryData)
    
    newData = []
    maxCode=""
    maxName=""
    i=0
    while i<len(data):
        industryData = {}
        industryData['name'] = data[i]['name']
        if total != 0:
            percentage = (data[i]['count'] * 100)/total
        else:
            percentage = 0
        industryData['count'] = data[i]['count']
        if maxCode =="":
            maxCode = data[i]['count']
            maxName = data[i]['name']
        elif maxCode < data[i]['count']:
            maxCode = data[i]['count']
            maxName = data[i]['name']
        industryData['share'] = percentage
        industryData['total'] = total
        newData.append(industryData)
        i=i+1
    
    activityusers = ActivityAttendee.objects.filter(user=request.user.id)
    sessions = Session.objects.all().order_by('id')
    return render_to_response('reports/business_reports.html', locals(), RequestContext(request))

def IndustryDetails(request):
    name = request.GET.get('name')
    activities = Activity.objects.all().order_by('id')
    codes = []
    for activity in activities:
        i=0
        regcode = 0
        while i<activity.industry.count():
            if name==activity.industry.all()[i].name:
                regcode = activity.event.reg_code
            i=i+1
        if regcode != 0:
            if len(codes)==0:
                codes.append({'code':regcode,'count':1})
            else :
                isFound=0
                for codeData in codes:
                    if codeData['code'] == regcode :
                        codeData['count'] = codeData['count']+1
                        isFound = 1
                if isFound == 0:
                    codes.append({'code':regcode,'count':1})
    return HttpResponse(json.dumps(codes),mimetype='application/json')



def BusinessList1(request):
    current_user = request.user
    user = current_user.id
    activityusers = ActivityAttendee.objects.filter(user=user)
    sessions = Session.objects.all().order_by('id')
    industries = Industry.objects.all().order_by('id')
    websocket_port = settings.WEBSOCKET_PORT
    data = []
    return render_to_response('reports/business_reports.html', locals(), RequestContext(request))

def EventList(request):
    events = Event.objects.all().order_by('id')
    websocket_port = settings.WEBSOCKET_PORT
    return render_to_response('reports/event_reports.html', locals(), RequestContext(request))

def EventDetail(request,id):
    registrants = Attendee.objects.filter(event=id).count()
    cancelees = Attendee.objects.filter(event=id,active=0).count()
    attendees = registrants - cancelees
    activities = Activity.objects.filter(event=id)
    data = []
    eventData = {}
    eventData['industry']=[]
    for activity in activities:
        i=0
        while i<activity.industry.count():
            eventData['industry'].append(activity.industry.all()[i].name)
            i=i+1
    eventData['registrants'] = registrants
    eventData['attendees'] = attendees
    eventData['cancelees'] = cancelees
    data.append(eventData)
    return HttpResponse(json.dumps(data),mimetype='application/json')

def AdminReport(request):
    websocket_port = settings.WEBSOCKET_PORT
    no_of_users = User.objects.all().count()
    no_of_users_system = UserTrack.objects.filter(active=True).count()
    events = Event.objects.all().order_by('id')
    return render_to_response('reports/admin_reports.html', locals(), RequestContext(request))

def ActivityReport(request,id):
    activities = Activity.objects.filter(event=id)
    data = []
    for activity in activities:
        activityData = {}
        activityid = activity.id
        activityData['attendee_count'] = ActivityAttendee.objects.filter(activity=activityid).count()
        activityData['activity_name'] = activity.name
        data.append(activityData)
    return HttpResponse(json.dumps(data),mimetype='application/json')



import reportSockets
