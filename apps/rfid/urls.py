from django.conf.urls import patterns, include, url
from .views import ListUsers, UsersDetails

urlpatterns = patterns(
    '',
    url(r'attendees/$', ListUsers.as_view()),
    url(r'attendee/$', UsersDetails.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
