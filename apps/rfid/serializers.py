import django_filters
from rest_framework import serializers
from apps.events.models import Attendee
from apps.users.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('rfid', 'first_name', 'last_name')


class AttendeeFilter(django_filters.FilterSet):
    active = django_filters.BooleanFilter()

    class Meta:
        model = Attendee
        fields = ['active']


class AttendeeSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)

    class Meta:
        model = Attendee
        fields = ('user', 'name', 'attend_as', 'event', 'created', 'updated', 'active')
