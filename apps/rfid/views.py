from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from apps.rfid.serializers import AttendeeSerializer, AttendeeFilter
from apps.events.models import Attendee


class ListUsers(APIView):

    def get(self, request, *args, **kwargs):
        attendees = AttendeeFilter(request.GET)
        serializer = AttendeeSerializer(attendees, many=True)
        return Response(serializer.data)


class UsersDetails(GenericAPIView):
    model = Attendee
    serializer_class = AttendeeSerializer
    lookup_field = 'id'

    def get(self, request, *args, **kwargs):
        self.kwargs = request.GET.dict()
        attendee = self.get_object()
        serializer = AttendeeSerializer(attendee)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        self.kwargs = request.POST.dict()
        attendee = self.get_object()
        attendee.active = self.kwargs.get('active')
        attendee.save()
        serializer = AttendeeSerializer(attendee)
        return Response(serializer.data)


