from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect,Http404
from apps.rfidAPI.models import UserTag,RoomUserLog
from apps.users.models import User
from apps.events.models import Facility
from django.views.decorators.csrf import csrf_exempt
import json, requests

dataFormat = {'error':'','status':'true','data':[]}

@csrf_exempt
def userList(request,urlType):
	data = {'error':'','status':'true','data':[]}
	userStatus = []
	if urlType == "all":
		userStatus = User.objects.all()
	elif urlType == "true":
		userStatus = User.objects.all()
	elif urlType == "false":
		userStatus = User.objects.all()
	else:
		data['error'] = "URL not defined"
		data['status'] = "false"
	dataum = []
	for user in userStatus:
		dataParts = {}
		dataParts['id'] = user.id
		dataParts['userId'] = user.id
		dataParts['salutation'] = user.salutation
		dataParts['name'] = user.first_name+' '+user.last_name
		dataParts['phone'] = user.phone
		dataParts['rfid'] = user.rfid
		dataParts['address'] = {}
		dataParts['address']['street'] = user.street
		dataParts['address']['city'] = str(user.city)
		dataParts['address']['region'] = str(user.region)
		dataParts['address']['zip'] = user.zip
		dataParts['address']['country'] = str(user.country)
		dataParts['status'] = 'false'
		userDatas = UserTag.objects.filter(user=user.id)
		for userData in userDatas:
			dataParts['status'] = userData.status
		if dataParts:
			if urlType == "true":
				if dataParts['status'] == 'true':
					dataum.append(dataParts)
			elif urlType == "false":
				if dataParts['status'] == 'false':
					dataum.append(dataParts)
			elif urlType == "all":
				dataum.append(dataParts)
	data['data'] =dataum
	return HttpResponse(json.dumps(data),mimetype='application/json')

@csrf_exempt
def userFindList(request,id):
	data = {'error':'','status':'true','data':[]}
	userDataum = User.objects.filter(id=id)
	dataum = []
	if userDataum :
		for userData in userDataum:
			dataParts = {}
			dataParts['id'] = userData.id
			dataParts['userId'] = userData.id
			dataParts['salutation'] = userData.salutation
			dataParts['name'] = userData.preffered_name
			dataParts['phone'] = userData.phone
			dataParts['rfid'] = userData.rfid
			dataParts['address'] = {}
			dataParts['address']['street'] = userData.street
			dataParts['address']['city'] = str(userData.city)
			dataParts['address']['region'] = str(userData.region)
			dataParts['address']['zip'] = userData.zip
			dataParts['address']['country'] = str(userData.country)
			if dataParts:
				dataum.append(dataParts)
		data['data'] =dataum
	else :
		data['error'] = "User id not found"
		data['status'] = "false"
	return HttpResponse(json.dumps(data),mimetype='application/json')

@csrf_exempt
def changeStatus(request,userId):
	data = {'error':'','status':'true','data':[]}
	if request.method == 'POST':
		if request.POST.get('status') :
			statusData = request.POST.get('status')
			users = User.objects.filter(id=userId)
			for user in users:
				userData,created = UserTag.objects.get_or_create(user=user)
				if userData:
					print userData
					userData.status=statusData
					userData.save()
					dataum = {}
					dataum['userId'] = userData.user.id 
					dataum['status'] = userData.status
					data['data'].append(dataum)
				else :
					data['error'] = "User id not found"
					data['status'] = "false"
		else :
			data['error'] = "status field missing"
			data['status'] = "false"
	else :
		data['error'] = "URL not defined"
		data['status'] = "false"
	return HttpResponse(json.dumps(data),mimetype='application/json')

def notifyUserChange(userId):
	data = {'error':'','status':'true','data':[]}
	userDataum = User.objects.filter(id=userId)
	dataum = []
	if userDataum :
		for userData in userDataum:
			dataParts = {}
			dataParts['id'] = userData.id
			dataParts['userId'] = userData.id
			dataParts['salutation'] = userData.salutation
			dataParts['name'] = userData.preffered_name
			dataParts['phone'] = userData.phone
			dataParts['rfid'] = userData.rfid
			dataParts['address'] = {}
			dataParts['address']['street'] = userData.street
			dataParts['address']['city'] = userData.city
			dataParts['address']['region'] = userData.region
			dataParts['address']['zip'] = userData.zip
			dataParts['address']['country'] = userData.country
			if dataParts:
				dataum.append(dataParts)
		data['data'] =dataum
	accesscode = request.GET.get('code')
	redirect_uri = ''
	url = ''
	r = requests.post(url, data=data)

@csrf_exempt
def getRoomList(request):
	data = {'error':'','status':'true','data':[]}
	rooms = Facility.objects.all()
	dataum = []
	for room in rooms:
		dataParts = {}
		dataParts['id'] = room.id
		dataParts['name'] = room.room_number
		if dataParts:
			dataum.append(dataParts)
	data['data'] =dataum
	return HttpResponse(json.dumps(data),mimetype='application/json')

@csrf_exempt
def getRoomUserList(request):
	data = {'error':'','status':'true','data':[]}
	rooms = RoomUserLog.objects.all()
	dataum = []
	for room in rooms:
		dataParts = {}
		dataParts['id'] = room.id
		dataParts['userId'] = room.user.id
		dataParts['roomId'] = room.room.id
		dataParts['date'] = room.dateTime
		if dataParts:
			dataum.append(dataParts)
	data['data'] =dataum
	return HttpResponse(json.dumps(data),mimetype='application/json')

@csrf_exempt
def roomUserLog(request):
	data = {'error':'','status':'true','data':[]}
	data['data']=[]
	if request.method == 'POST':
		roomId = request.POST.get('roomId')
		room = list(Facility.objects.filter(id=roomId))
		if room:
			userId = request.POST.get('userId')
			user = list(User.objects.filter(id=userId))
			if user:
				date = request.POST.get('date')
				print type(user)
				print user[0],room[0],date
				RoomUserLog.objects.create(user = user[0] , room = room[0], dateTime = date)
				dataum = {}
				dataum['userId'] = userId
				dataum['status'] = 'true'
				data['data'].append(dataum)
			else:
				data['error'] = "User Id not found"
				data['status'] = "false"
			dateTime = request.POST.get('date')
		else:
			data['error'] = "Room Id not found"
			data['status'] = "false"
	else :
		data['error'] = "URL not defined"
		data['status'] = "false"
	return HttpResponse(json.dumps(data),mimetype='application/json')


