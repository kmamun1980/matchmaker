from django.contrib import admin

from apps.rfidAPI.models import UserTag
# Register your models here.
admin.site.register(UserTag)
