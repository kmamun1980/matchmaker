from django.conf.urls import patterns, include, url

from django.conf import settings
from django.views.generic import TemplateView


urlpatterns = patterns('',
    url(r'^user/list/(?P<urlType>\w+)$', 'apps.rfidAPI.views.userList', name='list_user_status_tag'),
    url(r'^user/find/(?P<id>\d+)$', 'apps.rfidAPI.views.userFindList', name='find_list_user_details'),
    url(r'^user/(?P<userId>\d+)/status/$', 'apps.rfidAPI.views.changeStatus', name='change_user_status'),
    url(r'^rooms/list/$', 'apps.rfidAPI.views.getRoomList', name='list_rooms'),
    url(r'^rooms/list/$', 'apps.rfidAPI.views.getRoomList', name='list_rooms'),
    url(r'^rooms/log/$', 'apps.rfidAPI.views.roomUserLog', name='user_rooms_log'),
    url(r'^rooms/log/list$', 'apps.rfidAPI.views.getRoomUserList', name='user_rooms_log_list'),
)