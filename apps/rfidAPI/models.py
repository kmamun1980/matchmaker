from django.db import models
from django.core.exceptions import ValidationError
from apps.users.models import User
from apps.events.models import Facility
# Create your models here.


class UserTag(models.Model):
	user = models.ForeignKey(User, null=True, blank=True)
	status = models.CharField(max_length=400)

class RoomUserLog(models.Model):
	user = models.ForeignKey(User, null=True, blank=True)
	room = models.ForeignKey(Facility, null=True, blank=True)
	dateTime = models.CharField(max_length=400)