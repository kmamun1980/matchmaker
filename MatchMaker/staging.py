from .settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_*&gy8f2@(^ixw+tvxkyt$$1f87-qvi00r&s)l)*m)3ny-1**^'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ADMINS = (
    ('John', 'dextrazdxt@yahoo.com'),
)

MANAGERS = ADMINS

ALLOWED_HOSTS = ['.businessmatchmakingsoftware.com', 'localhost']

WSGI_APPLICATION = 'MatchMaker.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'matchmaker',       # Or path to database file if using sqlite3.
#         'USER': 'root',             # Not used with sqlite3.
#         'PASSWORD': 'password',     # Not used with sqlite3.
#         'HOST': 'localhost',        # Set to empty string for localhost
#         'PORT': '3306',             # Set to empty string for default
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'matchmakerdev',       # Or path to database file if using sqlite3.
        'USER': 'root',             # Not used with sqlite3.
        'PASSWORD': 'root',     # Not used with sqlite3.
        'HOST': '',        # Set to empty string for localhost
        'PORT': '3306',             # Set to empty string for default
    }
}


EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
MANDRILL_API_KEY = '_PS9E1bRpEJacH91VgJnXw'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'dextrazdxt@yahoo.com'
EMAIL_HOST_PASSWORD = '_PS9E1bRpEJacH91VgJnXw'
DEFAULT_FROM_EMAIL = 'noreply@businessmatchmakingsoftware.com'

DJRILL_WEBHOOK_SECRET = "FxVllnffnlYhWVFq"


LOCATION_MODEL = 'apps.events.models.Location'
LOGIN_URL = '/login/'


REDACTOR_OPTIONS = {'lang': 'en'}
REDACTOR_UPLOAD = 'redactor/'

TWILIO_SID = 'AC3d4615b9a2dfd9d7472f29f3737ca787'
TWILIO_TOKEN = 'c3a979e3c53928243760af93d3e44c17'
TWILIO_NUMBER = '+14806669090'

CITIES_LIGHT_TRANSLATION_LANGUAGES = ['en']


AUTO_RENDER_SELECT2_STATICS = False


# Django Suit configuration example
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'MatchMaker Admin',
    # 'HEADER_DATE_FORMAT': 'l, j. F Y',
    # 'HEADER_TIME_FORMAT': 'H:i',

    # forms
    # 'SHOW_REQUIRED_ASTERISK': True,  # Default True
    # 'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    # 'SEARCH_URL': '/admin/auth/user/',
    # 'MENU_ICONS': {
    #    'sites': 'icon-leaf',
    #    'auth': 'icon-lock',
    # },
    # 'MENU_OPEN_FIRST_CHILD': True, # Default True
    # 'MENU_EXCLUDE': ('auth.group',),
    # 'MENU': (
    #     'sites',
    #     {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
    #     {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
    #     {'label': 'Support', 'icon':'icon-question-sign', 'url': '/support/'},
    # ),

    # misc
    'LIST_PER_PAGE': 15
}

CRISPY_TEMPLATE_PACK = 'bootstrap3'



###
# local dev setting overrides
try:
    from settings_local import *
except ImportError:
    pass
