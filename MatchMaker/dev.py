from .settings import *

DEBUG = True
DEV_ENV = True
PROD_ENV = False


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'matchmakerdev',       # Or path to database file if using sqlite3.
        'USER': 'root',             # Not used with sqlite3.
        'PASSWORD': 'root',     # Not used with sqlite3.
        'HOST': '',        # Set to empty string for localhost
        'PORT': '3306',             # Set to empty string for default
    }
}


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_*&gy8f2@(^ixw+tvxkyt$$1f87-qvi00r&s)l)*m)3ny-1**^'

ALLOWED_HOSTS = ['localhost']

WSGI_APPLICATION = 'MatchMaker.wsgi.application'

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"


LOCATION_MODEL = 'apps.events.models.Location'
LOGIN_URL = '/login/'


REDACTOR_OPTIONS = {'lang': 'en'}
REDACTOR_UPLOAD = 'redactor/'

TWILIO_SID = 'AC3d4615b9a2dfd9d7472f29f3737ca787'
TWILIO_TOKEN = 'c3a979e3c53928243760af93d3e44c17'
TWILIO_NUMBER = '+14806669090'

CITIES_LIGHT_TRANSLATION_LANGUAGES = ['en']


AUTO_RENDER_SELECT2_STATICS = False


# Django Suit configuration example
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'MatchMaker Admin',
    # 'HEADER_DATE_FORMAT': 'l, j. F Y',
    # 'HEADER_TIME_FORMAT': 'H:i',

    # forms
    # 'SHOW_REQUIRED_ASTERISK': True,  # Default True
    # 'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    # 'SEARCH_URL': '/admin/auth/user/',
    # 'MENU_ICONS': {
    #    'sites': 'icon-leaf',
    #    'auth': 'icon-lock',
    # },
    # 'MENU_OPEN_FIRST_CHILD': True, # Default True
    # 'MENU_EXCLUDE': ('auth.group',),
    # 'MENU': (
    #     'sites',
    #     {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
    #     {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
    #     {'label': 'Support', 'icon':'icon-question-sign', 'url': '/support/'},
    # ),

    # misc
    'LIST_PER_PAGE': 15
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'apps.events': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'apps.company': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'apps.users': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

###
# local dev setting overrides
try:
    from settings_local import *
except ImportError:
    pass
