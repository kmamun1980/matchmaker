# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
#

SITE_ID = 1
SITE_URL = 'http://dev.businessmatchmakingsoftware.com/'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_i)gy8f2@(^ixe+tvxkyt$$1f87-qvi00r&s)l)*m)3ny-1**^'

# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

ALLOWED_HOSTS = ['dev.businessmatchmakingsoftware.com', 'localhost']

# Application definition
AUTH_USER_MODEL = 'users.User'


INSTALLED_APPS = (

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.admin',
    'apps.users',
    'chosen',
    'apps.flatpage',
    'south',
    'crispy_forms',
    'smart_selects',
    'geoposition',
    'cities_light',
    'redactor',
    'apps.communication',
    'qrcode',
    'select2',
    'captcha',
    'rest_framework',
    'sorl.thumbnail',
    'apps.base',
    'apps.company',
    'apps.relationships',
    'apps.survey',
    'apps.events',
    'apps.match',
    'apps.reports',
    'apps.rfidAPI',
    'apps.userrole',
    'apps.event_registration',
    'loginas',
    'apps.search',
    'inplaceeditform',
    'tastypie',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
)


ROOT_URLCONF = 'MatchMaker.urls'

WSGI_APPLICATION = 'MatchMaker.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'matchmaker.db'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

WEBSOCKET_PORT = 9000

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'static_media')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'

ADMIN_MEDIA_PREFIX = '/media/admin/'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

NOREPLY_EMAIL = 'noreply@businessmatchmakingsoftware.com'
CONTACT_EMAIL = 'hello@businessmatchmakingsoftware.com'

LOCATION_MODEL = 'apps.events.models.Location'
LOGIN_URL = '/login/'


REDACTOR_OPTIONS = {'lang': 'en'}
REDACTOR_UPLOAD = 'redactor/'

TWILIO_SID = 'AC3d4615b9a2dfd9d7472f29f3737ca787'
TWILIO_TOKEN = 'c3a979e3c53928243760af93d3e44c17'
TWILIO_NUMBER = '+14806669090'


CITIES_LIGHT_TRANSLATION_LANGUAGES = ['en']


AUTO_RENDER_SELECT2_STATICS = False

PROFILE_PICTURE_THUMBNAIL_SIZE = '150x150'
SMALL_PROFILE_PICTURE_THUMBNAIL_SIZE = '25x25'


# Django Suit configuration example
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'MatchMaker Admin',
    # 'HEADER_DATE_FORMAT': 'l, j. F Y',
    # 'HEADER_TIME_FORMAT': 'H:i',

    # forms
    # 'SHOW_REQUIRED_ASTERISK': True,  # Default True
    # 'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    # 'SEARCH_URL': '/admin/auth/user/',
    # 'MENU_ICONS': {
    #    'sites': 'icon-leaf',
    #    'auth': 'icon-lock',
    # },
    # 'MENU_OPEN_FIRST_CHILD': True, # Default True
    # 'MENU_EXCLUDE': ('auth.group',),
    # 'MENU': (
    #     'sites',
    #     {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
    #     {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
    #     {'label': 'Support', 'icon':'icon-question-sign', 'url': '/support/'},
    # ),

    # misc
    'LIST_PER_PAGE': 15
}

CRISPY_TEMPLATE_PACK = 'bootstrap3'
STRIPE_SECRET_KEY = 'sk_test_kC4gfsD0ajNMI9z33NexyUGD'
STRIPE_PUBLIC_KEY = 'pk_test_csYmy2bLMruu3twUZP6egWHh'

REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ],

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )
}
