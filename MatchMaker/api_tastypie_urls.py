from django.conf.urls import patterns, include, url
from django.conf import settings
from tastypie.api import Api

api = Api(api_name="v2")

from apps.users.api import *
api.register(UserResource())


from apps.company.api import *
api.register(NSCodeResource())

urlpatterns = patterns('',
    (r'', include(api.urls)),
)

