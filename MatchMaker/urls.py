from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#from apps.builder import urls as builder_urls
from apps.flatpage.views import HomeView
js_info_dict = {
    'packages': ('django.conf',),
}

admin.autodiscover()


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    # Examples:

    url(r'^$', HomeView.as_view(), name='home'),

    url(r'^events/', include('apps.events.urls')),

    url(r'^reports/', include('apps.reports.urls')),
    url(r'^api/v1/', include('apps.rfidAPI.urls')),


    url(r'^', include('apps.company.urls')),

    url(r'^', include('apps.survey.urls')),

    url(r'^', include('apps.users.urls')),
    url(r'^', include('apps.search.urls')),
    url(r'^', include('apps.match.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^select2/', include('select2.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^', include('apps.userrole.urls')),
    url(r'^', include('apps.communication.urls')),
    url(r'^', include('apps.event_registration.urls')),

    url(r'^api/rfid/', include('apps.rfid.urls')),
    url(r"^login/user/(?P<user_id>.+)/$", "loginas.views.user_login", name="loginas-user-login"),
    url(r'^inplaceeditform/', include('inplaceeditform.urls')),
    url(r'^jsi18n$', 'django.views.i18n.javascript_catalog', js_info_dict),


)

from MatchMaker.api_tastypie_urls import api as tastypie_api
urlpatterns += patterns('',
    (r'^apiv2/',include(tastypie_api.urls)),
)


if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(
            r'^static/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.STATIC_ROOT}
        ),
        #media
        url(
            r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}
        )
    )

