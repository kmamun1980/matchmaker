from django.http import HttpResponse
from django.shortcuts import render_to_response, render

def home(request):
	message = "You are welcome this our team."
	return render(request, "home/home.html", {'message': message})