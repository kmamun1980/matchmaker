__author__ = 'iromanov'

HOST_URI = 'root@107.170.252.36'

from fabric.api import run, env, cd, roles

env.roledefs['production'] = ['root@107.170.252.36:22']
env.CCC_root = '/root/projects/CCC'
env.MM_root = '/root/projects/MatchMaker'

@roles('production')
def update_CCC():
    with cd(env.CCC_root):
        run('supervisorctl stop ccc')
        run('supervisorctl stop celery')
        run('git pull')
        run('pip install -r requirements.txt')
        run('python manage.py syncdb')
        run('python manage.py migrate --noinput')
        run('python manage.py collectstatic --noinput')
        run('supervisorctl start ccc')
        run('supervisorctl start celery')


@roles('production')
def update_MM():
    with cd(env.MM_root):
        run('supervisorctl stop bbm')
        run('git pull')
        run('pip install -r requirements.txt')
        run('python manage.py syncdb')
        run('python manage.py migrate --noinput')
        run('python manage.py collectstatic --noinput')
        run('supervisorctl start bbm')


@roles('production')
def update_cities():
    with cd(env.MM_root):
        run('python manage.py cities_light --force-all')


@roles('production')
def restart_WS():
    with cd(env.MM_root):
        run('supervisorctl restart ws')
