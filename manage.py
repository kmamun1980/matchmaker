#!/usr/bin/env python
import os
import sys
import glob

if __name__ == "__main__":

    # Let's use some env variables
    # just put a file named after variable name you want to set into envdir directory
    # and fill it with variable value.
    # More:
    # http://bruno.im/2013/may/18/django-stop-writing-settings-files/
    # http://envdir.readthedocs.org/en/latest/usage.html
    env_dir = 'envdir'
    env_vars = glob.glob(os.path.join(env_dir, '*'))
    for env_var in env_vars:
        with open(env_var, 'r') as env_var_file:
            os.environ.setdefault(env_var.split(os.sep)[-1],
                                  env_var_file.read().strip())

    # fallback if there's no DJANGO_SETTINGS_MODULE in envdir
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MatchMaker.settings")

    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
